# README #

Effect fine-mapping (**EFM**) is  framework to identify statistically independent effects in genetic studies and perform fine-mapping.  

EFM is currently under development. Feel free to use this candidate release and let us know of any issues or suggestions.  

### Version ###
The current version of EFM is v0.1RC1.

### Installation ###

* Prerequsities 

* Instructions

### Documentation ###

#### Available commands ####
Running EFM with no options or "help" will list all available options:

~~~~~~~~~~~~~~~~~~~~~

> efm help
Usage: efm <verb> <options>

Verbs and their options ([*] - required, <*> - optional):
    help                                                                             - show usage
    version                                                                          - show version number
    logisticRegression [-f] <-o|-c|-t|-v|-g|-n|-m|-i|-b|-x|-k>                       - run logistic regression
    geneticRiskScore [-f] <-v|-i|-o|-g>                                              - calculate Genetic Risk Score
    transposeBedFile [-f] <-o>                                                       - transpose the BED-file
Options, their long versions and arguments:
    -f|--input-file-set-prefix <arg>       - PLINK file-set prefix (BED/BIM/FAM)
    -o|--output-file-set-prefix <arg>      - output file-set prefix (default: efm)
    -i|--input-file <arg>                  - input file
    -g|--genetic-inheritance <arg>         - inheritance model: 'd'=dominant, 'r'=recessive or 'a'=additive (default)
    -c|--covar-file <arg>                  - covariates file (PLINK-formatted)
    -t|--thread_num <arg>                  - number of threads
    -k|--blocksize <arg>                   - define the number of SNP's each thread reads before processing
    -v|--verbose                           - show info and time elapsed for steps
    -n|--new-covar <arg>                   - create and add new covariate as specified
    -m|--model-report                      - report information criterion info for all models run
    -b|--beagle-input <arg>                - input file contains genotype probability/likelihood in BEAGLE format
    -x|--double-x-male                     - double chromosome X dosage for males
~~~~~~~~~~~~~~~~~~~~~


#### Input files ####
The current version of EFM supports [binary PLINK files](https://www.cog-genomics.org/plink2/formats#bed) (bed/bim/fam). The next candidate release will add vcf  (v4.1; uncompressed and gzip compressed) and bgen files. 
We will gradually expand the list of supported files, prioritizing formats widely used by the community.

#### Logistic regression ####
The current version of EFM is supporting logistc regression using a gradient descent algorithm. 
To run logistic regression the "logisticRegression" needs to be specified before any option:
~~~~~~~~~~~~~~~~~~~~~
> efm  logisticRegression -f filename 
~~~~~~~~~~~~~~~~~~~~~
The above command will run logistic regression for all variants in the file filename.[bed/bim/fam]. 

You can specify a text file with covariates to use in the model. EFM supports covariate files in the same format as PLINK, i.e. FID and IID in first two columns, covariates in remaining columns. 
By default all covariates in the file are used. Selection of covariates and more complex models can be specified with the "-i" option.
~~~~~~~~~~~~~~~~~~~~~
> efm  logisticRegression -f filename -c covariate.txt
~~~~~~~~~~~~~~~~~~~~~

#### Options ####



### Who do I talk to? ###
For any questions please contact npatsopoulos AT rics.bwh.harvard.edu using the subject "EFM". 

### Credits ###
*EFM is been developed by Zhou (Ark) Fang, Kostas Stefanou and Vasilis Pierros.* 
*We are grateful to Onek Zarzycki for the first version of the code and his contribution.* 
*EFM was developed as part of an Intel Parallel Computing Center in the Patsopoulos lab.*