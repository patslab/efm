/*
 * Copyright (C) 2016-2017 The Brigham and Women’s Hospital, Inc.
 * Portions contributed and copyright held by others as indicated.
 * All rights reserved.
 *
 * The contents of this file are subject to the terms of the BSD 3-clause "New" or "Revised"
 * License (the "License"). You may not use this file except in compliance with the License.
 *
 * You should have received a copy of the License in a separate text file originally named
 * "LICENSE_BWH.md"; if not, refer to "BSD-3-Clause" license at https://spdx.org/licenses/.
 */

#include <getopt.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>

#include "interface.h"
#include "types.h"

// Commands
static struct command {
   char* name;
   char* description;
   char* required_options;
   char* optional_options;
   int config_flags;
} commands[] = { { CMD_HELP_NAME, CMD_HELP_DESC, CMD_HELP_OPTS_REQ, CMD_HELP_OPTS_OPT, CONF_HELP }, { CMD_VERSION_NAME, CMD_VERSION_DESC,
CMD_VERSION_OPTS_REQ, CMD_VERSION_OPTS_OPT, CONF_VERSION }, { CMD_LOGISTIC_REGRESSION_NAME, CMD_LOGISTIC_REGRESSION_DESC, CMD_LOGISTIC_OPTS_REQ,
CMD_LOGISTIC_OPTS_OPT, CONF_LOGISTIC }, { CMD_LINEAR_REGRESSION_NAME, CMD_LINEAR_REGRESSION_DESC, CMD_LOGISTIC_OPTS_REQ, CMD_LOGISTIC_OPTS_OPT,
      CONF_LINEAR }, //LINEAR:1
      { CMD_GRS_NAME, CMD_GRS_DESC, CMD_GRS_OPTS_REQ, CMD_GRS_OPTS_OPT, CONF_GRS }, { CMD_TRANSPOSE_NAME, CMD_TRANSPOSE_DESC, CMD_TRANSPOSE_OPTS_REQ,
      CMD_TRANSPOSE_OPTS_OPT, CONF_TRANSPOSE } };

// Options
static struct option options[] = {
#ifdef DEBUG
      {  OPT_DEBUG_NAME, no_argument, NULL, OPT_DEBUG_VAL},
#endif
      { OPT_INFILE_NAME, required_argument, NULL, OPT_INFILE_VAL }, { OPT_OUTFILE_NAME, required_argument, NULL, OPT_OUTFILE_VAL }, {
      OPT_AUX_INFILE_NAME, required_argument, NULL, OPT_AUX_INFILE_VAL }, { OPT_INHERIT_NAME, required_argument, NULL, OPT_INHERIT_VAL }, {
      OPT_COVAR_NAME, required_argument, NULL, OPT_COVAR_VAL }, { OPT_THREADS_NAME, required_argument, NULL, OPT_THREADS_VAL }, {
      OPT_BLOCKSIZE_NAME, required_argument, NULL, OPT_BLOCKSIZE_VAL }, { OPT_VERBOSE_NAME, no_argument, NULL, OPT_VERBOSE_VAL }, {
      OPT_NEW_COVAR_NAME, required_argument, NULL, OPT_NEW_COVAR_VAL }, { OPT_MODEL_REPORT_NAME, no_argument, NULL, OPT_MODEL_REPORT_VAL }, {
      OPT_INPUT_BEAGLE_NAME, required_argument, NULL, OPT_INPUT_BEAGLE_VAL }, { OPT_INPUT_BGEN_NAME, required_argument, NULL, OPT_INPUT_BGEN_VAL }, //BGEN_VCF:1
      { OPT_INPUT_VCF_NAME, required_argument, NULL, OPT_INPUT_VCF_VAL }, //BGEN_VCF:1
      { OPT_INPUT_VCF_GZIPPED_NAME, required_argument, NULL, OPT_INPUT_VCF_GZIPPED_VAL }, //BGEN_VCF:1
      { OPT_DOUBLE_X_MALE_NAME, no_argument, NULL, OPT_DOUBLE_X_MALE_VAL }, { 0, 0, 0, 0 } };

// Descriptions for options
static struct option_description {
   int val;
   char* description;
} opt_descriptions[] = {
#ifdef DEBUG
      {  OPT_DEBUG_VAL, OPT_DEBUG_DESC},
#endif
      { OPT_INFILE_VAL, OPT_INFILE_DESC }, { OPT_OUTFILE_VAL, OPT_OUTFILE_DESC }, { OPT_AUX_INFILE_VAL, OPT_AUX_INFILE_DESC }, { OPT_INHERIT_VAL,
      OPT_INHERIT_DESC }, { OPT_COVAR_VAL, OPT_COVAR_DESC }, { OPT_THREADS_VAL, OPT_THREADS_DESC }, { OPT_BLOCKSIZE_VAL, OPT_BLOCKSIZE_DESC }, {
      OPT_VERBOSE_VAL, OPT_VERBOSE_DESC }, { OPT_NEW_COVAR_VAL, OPT_NEW_COVAR_DESC }, { OPT_MODEL_REPORT_VAL, OPT_MODEL_REPORT_DESC }, {
      OPT_INPUT_BEAGLE_VAL, OPT_INPUT_BEAGLE_DESC }, { OPT_INPUT_BGEN_VAL, OPT_INPUT_BGEN_DESC }, //BGEN_VCF:1
      { OPT_INPUT_VCF_VAL, OPT_INPUT_VCF_DESC }, //BGEN_VCF:1
      { OPT_INPUT_VCF_GZIPPED_VAL, OPT_INPUT_VCF_GZIPPED_DESC }, //BGEN_VCF:1

      { OPT_DOUBLE_X_MALE_VAL, OPT_DOUBLE_X_MALE_DESC } };

/**
 Get optstring for specific command (verb)

 @param cmd_idx command index

 @return optstring representing supported options (to be passed to getopt_long()).
 The return value is an allocated string. Free this memory with free() when no longer needed.
 */
static char* get_optstring(int cmd_idx) {
   // optstring buffer
   char cbuf[100];
   // prefix common for all optstrings
   int cbuf_len = sprintf(cbuf, "-:");
   // current command's options in a string
   char* cur_cmd_opts = commands[cmd_idx].required_options;

   // Required options
   while (*cur_cmd_opts) {
      int idx = 0;
      cbuf[cbuf_len++] = *cur_cmd_opts;
      while (options[idx].val != *cur_cmd_opts) {
         idx++;
      }
      if (options[idx].val && options[idx].has_arg == required_argument) {
         cbuf[cbuf_len++] = ':';
      }
      cur_cmd_opts++;
   }

   cur_cmd_opts = commands[cmd_idx].optional_options;

   // Optional options
   while (*cur_cmd_opts) {
      int idx = 0;
      cbuf[cbuf_len++] = *cur_cmd_opts;
      while (options[idx].val != *cur_cmd_opts) {
         idx++;
      }
      if (options[idx].val && options[idx].has_arg == required_argument) {
         cbuf[cbuf_len++] = ':';
      }
      cur_cmd_opts++;
   }

   cbuf[cbuf_len] = '\0';
   return strdup(cbuf);
}

void process_command_line_opts(int argc, char **argv, int cmd_idx) {
   int opt;
   int opt_idx = 0;
   char* optstring = get_optstring(cmd_idx);
   char *tail_ptr;

   // Disabling getopt's error messages
   opterr = 0;

   while ((opt = getopt_long(argc, argv, optstring, options, &opt_idx)) != -1) {
      switch (opt) {
#ifdef DEBUG
      case OPT_DEBUG_VAL:
      g_conf |= CONF_DEBUG;
      Debug("option -%c selected", opt);
      break;
#endif
      case OPT_INFILE_VAL:
         Debug("option -%c selected with file-set prefix of '%s'", opt, optarg);
         if (optarg && strlen(optarg) && strlen(optarg) < MAX_FILENAME_LENGTH - 4) {
            g_plink_fileset_prefix = strdup(optarg);
            g_conf |= CONF_FILESET | CONF_INPUT_PLINK;
         } else {
            Error("Invalid PLINK file-set specified.");
            g_conf |= CONF_FATAL_ERROR;
         }
         break;
      case OPT_OUTFILE_VAL:
         Debug("option -%o selected with file-set prefix of '%s'", opt, optarg);
         if (optarg && strlen(optarg) && strlen(optarg) < MAX_FILENAME_LENGTH - 10) {
            g_output_fileset_prefix = strdup(optarg);
            g_conf |= CONF_OUTFILE;
         } else {
            Error("Invalid output file specified.");
            g_conf |= CONF_FATAL_ERROR;
         }
         break;
      case OPT_AUX_INFILE_VAL:
         Debug("option -%o selected with file-name of '%s'", opt, optarg);
         if (optarg && strlen(optarg) && strlen(optarg) < MAX_FILENAME_LENGTH) {
            g_aux_input_file_name = strdup(optarg);
            g_conf |= CONF_AUX_INFILE;
         } else {
            Error("Invalid output file specified.");
            g_conf |= CONF_FATAL_ERROR;
         }
         break;
      case OPT_INPUT_BEAGLE_VAL:
         Debug("option -%c selected", opt);
         if (optarg && strlen(optarg) && strlen(optarg) < MAX_FILENAME_LENGTH - 4) {
            g_beagle_fileset_prefix = strdup(optarg);
            g_conf |= CONF_FILESET | CONF_INPUT_BEAGLE;
         } else {
            Error("Invalid PLINK file-set specified."); //TODO: This should probably be Invalid BEAGLE
            g_conf |= CONF_FATAL_ERROR;
         }
         break;
      case OPT_INPUT_BGEN_VAL: //BGEN_VCF:2
         Debug("option -%c selected", opt);
         if (optarg && strlen(optarg) && strlen(optarg) < MAX_FILENAME_LENGTH - 5) { //BGEN ends with .bgen
            g_bgen_fileset_prefix = strdup(optarg); //TODO: Check if that is actual necessary
            g_conf |= CONF_FILESET | CONF_INPUT_BGEN;
         } else {
            Error("Invalid BGEN file-set specified.");
            g_conf |= CONF_FATAL_ERROR;
         }
         break;
      case OPT_INPUT_VCF_VAL: //BGEN_VCF:2
         Debug("option -%c selected", opt);
         if (optarg && strlen(optarg) && strlen(optarg) < MAX_FILENAME_LENGTH - 4) {
            g_vcf_fileset_prefix = strdup(optarg); //TODO: Check if that is actual necessary
            g_conf |= CONF_FILESET | CONF_INPUT_VCF;
         } else {
            Error("Invalid VCF file-set specified.");
            g_conf |= CONF_FATAL_ERROR;
         }
         break;
      case OPT_INPUT_VCF_GZIPPED_VAL: //BGEN_VCF:2
         Debug("option -%c selected", opt);
         if (optarg && strlen(optarg) && strlen(optarg) < MAX_FILENAME_LENGTH - 8) { //GZIPPED VCF ends with .vcf.gz
            g_gzipped_vcf_fileset_prefix = strdup(optarg); //TODO: Check if that is actual necessary
            g_conf |= CONF_FILESET | CONF_INPUT_VCF_GZIPPED;
         } else {
            Error("Invalid VCF GZipped file-set specified.");
            g_conf |= CONF_FATAL_ERROR;
         }
         break;
      case OPT_COVAR_VAL:
         Debug("option -%c selected with file-name of '%s'", opt, g_covar_file_name);
         if (optarg && strlen(optarg) && strlen(optarg) < MAX_FILENAME_LENGTH) {
            g_covar_file_name = strdup(optarg);
            g_conf |= CONF_COVARFILE;
         } else {
            Error("Invalid covariate-file specified.");
            g_conf |= CONF_FATAL_ERROR;
         }
         break;
      case OPT_THREADS_VAL:
         g_thread_number = strtol(optarg, &tail_ptr, 10);
         Debug("option -%c selected with argument of %s", opt, optarg);
         if (optarg == tail_ptr || g_thread_number < 1 || g_thread_number > MAX_NUMBER_OF_THREADS) {
            Error("Invalid number of threads specified (the [min,max] is [1,%d])", MAX_NUMBER_OF_THREADS);
            g_conf |= CONF_FATAL_ERROR;
         }
         break;
      case OPT_BLOCKSIZE_VAL:
         blocksize = strtol(optarg, &tail_ptr, 10);
         Debug("option -%c selected with argument of %s", opt, optarg);
         if (optarg == tail_ptr || blocksize < 1 || blocksize > 1024) {
            Error("Invalid number of Block Size specified (the [min,max] is [1,%d])", 1024);
            g_conf |= CONF_FATAL_ERROR;
         }
         break;
      case OPT_DOUBLE_X_MALE_VAL:
         g_conf |= CONF_DBL_X_MALE;
         Debug("option -%c selected", opt);
         break;
      case OPT_VERBOSE_VAL:
         g_conf |= CONF_VERBOSE;
         Debug("option -%c selected", opt);
         break;
      case OPT_MODEL_REPORT_VAL:
         g_conf |= CONF_MODEL_REPORT;
         Debug("option -%c selected", opt);
         break;
      case OPT_NEW_COVAR_VAL:
         if (g_new_covar_spec) {
            Error("New covariate option specified twice (unsupported)");
            g_conf |= CONF_FATAL_ERROR;
         }
         Debug("option -%c selected", opt);
         if (optarg && strlen(optarg)) {
            Debug("New covariate specification: %s", optarg);
            g_new_covar_spec = strdup(optarg);
            g_conf |= CONF_NEW_COVAR;
         } else {
            Error("Invalid new covariate specification.");
            g_conf |= CONF_FATAL_ERROR;
         }
         break;
      case OPT_INHERIT_VAL:
         if (optarg && strlen(optarg)) {
            Debug("option -%c selected with value of %c", opt, optarg[0]);
            switch (optarg[0]) {
            case 'a':
               Debug("Additive model of inheritance selected");
               g_conf |= CONF_INH_ADD;
               break;
            case 'd':
               Debug("Dominant model of inheritance selected");
               g_conf |= CONF_INH_DOM;
               break;
            case 'r':
               Debug("Recessive model of inheritance selected");
               g_conf |= CONF_INH_REC;
               break;
            default:
               Error("Invalid argument for option -%c selected", opt);
               g_conf |= CONF_FATAL_ERROR;
            }
         } else {
            Error("Invalid inheritance mode.");
            g_conf |= CONF_FATAL_ERROR;
         }
         break;
      case ':':
         Error("Missing required argument for option -%c", optopt);
         g_conf |= CONF_FATAL_ERROR;
         break;
      default:
         Error("Invalid option '%c'. Quitting now.", opt);
         g_conf |= CONF_FATAL_ERROR;
         break;
      };

      if (g_conf & CONF_FATAL_ERROR) {
         break;
      }
   };
   if (optstring)
      free(optstring);
}

void process_command_line(int argc, char **argv) {
   int cmd_idx;

   if (argc < 2) {
      g_conf |= CONF_HELP;
      return;
   }

   // Matching the command
   for (cmd_idx = 0; cmd_idx < sizeof(commands) / sizeof(struct command); cmd_idx++) {
      if (strcasecmp(commands[cmd_idx].name, argv[1])) {
         continue;
      };
      g_conf |= commands[cmd_idx].config_flags;
      break;
   }

   // No option matched
   if (cmd_idx == sizeof(commands) / sizeof(struct command)) {
      Info("Invalid command \"%s\". Run again with command \"help\" for synopsis.", argv[1]);
      g_conf |= CONF_FATAL_ERROR;
      return;
   }

   // Processing options
   if (argc > 2) {
      process_command_line_opts(argc - 1, &(argv[1]), cmd_idx);
   }
}

void print_version() {
#if defined(__LP64__)
   Info("%s version %d.%d %s(64-bit) (%s)", EXECUTABLE_NAME, VERSION_MAJOR, VERSION_MINOR, VERSION_IDENTIFIER, VERSION_DATE);
#else
   Info("%s version %d.%d %s(32-bit) (%s)", EXECUTABLE_NAME, VERSION_MAJOR, VERSION_MINOR, VERSION_IDENTIFIER, VERSION_DATE);
#endif

#if defined(__INTEL_COMPILER)
   Info(__VERSION__);
#endif

#if defined(__INTEL_MKL__)
   Info("Intel MKL v. %d", INTEL_MKL_VERSION);
#endif

#if defined(_OPENMP)
   Info("Intel OpenMP v. %d", _OPENMP);
#endif
}

void print_usage() {
   char cbuf[TERMINAL_LINE_LENGTH + 1];
   Info("Usage: %s <verb> <options>\n", EXECUTABLE_NAME);
   // Print available commands
   for (int cmd_idx = 0; cmd_idx < sizeof(commands) / sizeof(struct command); cmd_idx++) {
      int cbuf_len = 0;
      if (cmd_idx == 0) {
         Info("Verbs and their options ([*] - required, <*> - optional):");
      }
      if (commands[cmd_idx].name) {
         cbuf_len += sprintf(cbuf + cbuf_len, "    %s", commands[cmd_idx].name);

         // Required options
         if (commands[cmd_idx].required_options && strlen(commands[cmd_idx].required_options)) {
            cbuf_len += sprintf(cbuf + cbuf_len, " [");
            for (int opt_idx = 0; opt_idx < strlen(commands[cmd_idx].required_options); opt_idx++) {
               if (cbuf[cbuf_len - 1] != '[') {
                  cbuf[cbuf_len++] = '|';
               }
               cbuf_len += sprintf(cbuf + cbuf_len, "-%c", commands[cmd_idx].required_options[opt_idx]);
            }
            cbuf_len += sprintf(cbuf + cbuf_len, "]");
         }

         // Optional options
         if (commands[cmd_idx].optional_options && strlen(commands[cmd_idx].optional_options)) {
            cbuf_len += sprintf(cbuf + cbuf_len, " <");
            for (int opt_idx = 0; opt_idx < strlen(commands[cmd_idx].optional_options); opt_idx++) {
               if (cbuf[cbuf_len - 1] != '<') {
                  cbuf[cbuf_len++] = '|';
               }
               cbuf_len += sprintf(cbuf + cbuf_len, "-%c", commands[cmd_idx].optional_options[opt_idx]);
            }
            cbuf_len += sprintf(cbuf + cbuf_len, ">");
         }

         do {
            cbuf[cbuf_len++] = ' ';
         } while (cbuf_len < TERMINAL_LINE_LENGTH * VERBS_FIRST_COLUMN_FRACT);

         if (commands[cmd_idx].description && strlen(commands[cmd_idx].description) <= TERMINAL_LINE_LENGTH - cbuf_len) {
            cbuf_len += sprintf(cbuf + cbuf_len, " - %s", commands[cmd_idx].description);
         }
      }
      cbuf[cbuf_len > TERMINAL_LINE_LENGTH ? TERMINAL_LINE_LENGTH : cbuf_len] = '\0';
      Info("%s", cbuf);
   }
   // Show options
   for (int opt_idx = 0; opt_idx < sizeof(options) / sizeof(struct option); opt_idx++) {
      int cbuf_len = 0;

      if (options[opt_idx].val == 0) {
         break;
      }

      if (opt_idx == 0) {
         Info("Options, their long versions and arguments:");
      }

      cbuf_len += sprintf(cbuf + cbuf_len, "    -%c", options[opt_idx].val);

      if (options[opt_idx].name) {
         cbuf_len += sprintf(cbuf + cbuf_len, "|--%s", options[opt_idx].name);
      }

      if (options[opt_idx].has_arg) {
         cbuf_len += sprintf(cbuf + cbuf_len, options[opt_idx].has_arg == required_argument ? " <arg>" : " [arg]");
      }

      do {
         cbuf[cbuf_len++] = ' ';
      } while (cbuf_len < TERMINAL_LINE_LENGTH * OPTS_FIRST_COLUMN_FRACT);

      for (int opt_desc_idx = 0; opt_desc_idx < sizeof(opt_descriptions) / sizeof(struct option_description); opt_desc_idx++) {
         if (options[opt_idx].val != opt_descriptions[opt_desc_idx].val) {
            continue;
         };
         if (opt_descriptions[opt_desc_idx].description) {
            cbuf_len += snprintf(cbuf + cbuf_len, TERMINAL_LINE_LENGTH - cbuf_len, " - %-s", opt_descriptions[opt_desc_idx].description);
         }
         break;
      }

      cbuf[cbuf_len > TERMINAL_LINE_LENGTH ? TERMINAL_LINE_LENGTH : cbuf_len] = '\0';
      Info("%s", cbuf);
   }
}
