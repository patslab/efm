/*
 * Copyright (C) 2016-2017 The Brigham and Women’s Hospital, Inc.
 * Portions contributed and copyright held by others as indicated.
 * All rights reserved.
 *
 * The contents of this file are subject to the terms of the BSD 3-clause "New" or "Revised"
 * License (the "License"). You may not use this file except in compliance with the License.
 *
 * You should have received a copy of the License in a separate text file originally named
 * "LICENSE_BWH.md"; if not, refer to "BSD-3-Clause" license at https://spdx.org/licenses/.
 */

#include "bgen_functions.h"

struct bgenpheno {
   char *fid, *iid;
   int phenotype, sex;
};

void appendme(char* s, char c) {
   int len = strlen(s);
   s[len] = c;
   s[len + 1] = '\0';
}

int bgen_getsnps(const byte fileName[]) {

   int output;

   BGenFile *bgen = bgen_open(fileName);

   inti nsamples = bgen_nvariants(bgen);
   output = nsamples;

   return output;
}

int bgen_getsamples(const byte fileName[]) {

   int output;

   BGenFile *bgen = bgen_open(fileName);

   inti nsamples = bgen_nsamples(bgen);

   output = nsamples;

   return output;
}

char* bgen_getallele1(const byte fileName[], int snpid, Variant *variants) {

   char* output;

   output = variants[snpid].allele_ids[0].str;

   int len = strlen(output);

   int outind = 0;

   for (int i = 0; i < len; i++) {
      char ch = output[i];
      if ((ch == 'A') || (ch == 'C') || (ch == 'G') || (ch == 'T')) {
         outind++;
      }
   }

   char* output2 = (char *) malloc(sizeof(char) * outind);    //[len+1];
   outind = 0;

   for (int i = 0; i < len; i++) {
      char ch = output[i];
      if ((ch == 'A') || (ch == 'C') || (ch == 'G') || (ch == 'T')) {
         output2[outind] = ch;
         outind++;
      }
   }
   output2[outind] = '\0';

   return output2;
}

char* bgen_getallele2(const byte fileName[], int snpid, Variant *variants) {
   char* output;
   output = variants[snpid].allele_ids[1].str;

   return output;
}

char* bgen_getallele3(const byte fileName[], int snpid, Variant *variants) {

   char* output;
   output = variants[snpid].allele_ids[0].str;

   return output;
}

char* bgen_getid(const byte fileName[], int snpid, Variant *variants) {

   char* output;

   output = variants[snpid].id.str;

   return output;

}

int bgen_getpos(const byte fileName[], int snpid, Variant *variants) {

   int output;

   output = variants[snpid].position;

   return output;

}

char* bgen_getchromosome(const byte fileName[], int snpid, Variant *variants) {

   char* output;

   output = variants[snpid].chrom.str;

   return output;

}

char* bgen_getchromosome2(const byte fileName[], int snpid, Variant *variants) {

   char* output;

   output = variants[snpid].rsid.str;

   int len = strlen(output);

   int outind = 0;

   for (int i = 0; i < len; i++) {
      char ch = output[i];
      int chint = ch;
      if ((chint > 31) && (chint < 128)) {
         outind++;
      }
   }

   char* output2 = (char *) malloc(sizeof(char) * outind);
   outind = 0;

   for (int i = 0; i < len; i++) {
      char ch = output[i];
      int chint = ch;
      if ((chint > 31) && (chint < 128)) {
         output2[outind] = ch;
         outind++;
      }
   }
   output2[outind] = '\0';

   return output2;
}

real* bgen_getgenotypicprobabilites(const byte fileName[], int number, int sample, VariantGenotype *vg, real *probabilities) {

   inti i, j;

   real* output = (real *) malloc(sizeof(real) * bgen_ncombs(vg));

   for (i = 0; i < bgen_ncombs(vg); ++i) {
      output[i] = probabilities[sample * bgen_ncombs(vg) + i];
   }

   return output;
}

void readsamplefile(char *fileName, int line) {

   FILE* file = fopen(fileName, "r"); /* should check the result */

   if (file == NULL) {
      fprintf(stderr, "Can't open input file!\n");
      exit(1);
   }

   char id1[100];
   int int1;
   fscanf(file, "%s", &id1);
   fscanf(file, "%s", &id1);
   fscanf(file, "%s", &id1);
   fscanf(file, "%s", &id1);
   fscanf(file, "%s", &id1);
   fscanf(file, "%s", &id1);
   fscanf(file, "%s", &id1);
   fscanf(file, "%s", &id1);
   fscanf(file, "%s", &id1);
   fscanf(file, "%s", &id1);

   while (!feof(file)) {
      fscanf(file, "%s", &id1);
      printf("%s ", id1);
      fscanf(file, "%s", &id1);
      printf("%s ", id1);
      fscanf(file, "%d", &int1);
      printf("%d ", int1);
      fscanf(file, "%d", &int1);
      printf("%d ", int1);
      fscanf(file, "%d", &int1);
      printf("%d\n", int1);
   }
}

char *readsamplefilefamilyid(char *fileName, int line) {

   FILE* file = fopen(fileName, "r"); /* should check the result */

   if (file == NULL) {
      fprintf(stderr, "Can't open input file!\n");
      exit(1);
   }

   char *output;

   char ignore[100];

   char id1[100];
   int int1;

   fgets(ignore, sizeof(ignore), file);
   fgets(ignore, sizeof(ignore), file);

   int cnt = 0;
   while (!feof(file)) {

      if (line != cnt) {
         fgets(ignore, sizeof(ignore), file);
         cnt++;
      } else {

         fscanf(file, "%s", &id1);
         if (line == cnt) {
            output = id1;
            break;
         }
         fscanf(file, "%s", &id1);
         fscanf(file, "%d", &int1);
         fscanf(file, "%d", &int1);
         fscanf(file, "%d", &int1);
      }
   }

   return output;
}

char *readsamplefilewithinfamilyid(char *fileName, int line) {

   FILE* file = fopen(fileName, "r"); /* should check the result */

   if (file == NULL) {
      fprintf(stderr, "Can't open input file!\n");
      exit(1);
   }

   char *output;

   char ignore[100];

   char id1[100];
   int int1;

   fgets(ignore, sizeof(ignore), file);
   fgets(ignore, sizeof(ignore), file);

   int cnt = 0;
   while (!feof(file)) {
      if (line != cnt) {
         fgets(ignore, sizeof(ignore), file);
         cnt++;
      } else {

         fscanf(file, "%s", &id1);
         fscanf(file, "%s", &id1);
         if (line == cnt) {
            output = id1;
            break;
         }
         fscanf(file, "%d", &int1);
         fscanf(file, "%d", &int1);
         fscanf(file, "%d", &int1);
      }
   }

   return output;
}

int readsamplefilemissing(char *fileName, int line) {

   FILE* file = fopen(fileName, "r"); /* should check the result */

   if (file == NULL) {
      fprintf(stderr, "Can't open input file!\n");
      exit(1);
   }

   int output;

   char id1[100];
   int int1;
   fscanf(file, "%s", &id1);
   fscanf(file, "%s", &id1);
   fscanf(file, "%s", &id1);
   fscanf(file, "%s", &id1);
   fscanf(file, "%s", &id1);
   fscanf(file, "%s", &id1);
   fscanf(file, "%s", &id1);
   fscanf(file, "%s", &id1);
   fscanf(file, "%s", &id1);
   fscanf(file, "%s", &id1);

   int cnt = 0;
   while (!feof(file)) {
      fscanf(file, "%s", &id1);
      fscanf(file, "%s", &id1);
      fscanf(file, "%d", &int1);
      if (line == cnt) {
         output = int1;
         break;
      }
      fscanf(file, "%d", &int1);
      fscanf(file, "%d", &int1);
      cnt++;
   }

   return output;
}

int readsamplefilesex(char *fileName, int line) {

   FILE* file = fopen(fileName, "r"); /* should check the result */

   if (file == NULL) {
      fprintf(stderr, "Can't open input file!\n");
      exit(1);
   }

   int output;

   char ignore[100];

   char id1[100];
   int int1;

   fgets(ignore, sizeof(ignore), file);
   fgets(ignore, sizeof(ignore), file);

   int cnt = 0;
   while (!feof(file)) {
      if (line != cnt) {
         fgets(ignore, sizeof(ignore), file);
         cnt++;
      } else {
         fscanf(file, "%s", &id1);
         fscanf(file, "%s", &id1);
         fscanf(file, "%d", &int1);
         fscanf(file, "%d", &int1);
         if (line == cnt) {
            output = int1;
            break;
         }
         fscanf(file, "%d", &int1);
      }
   }

   return output;
}

int readsamplefilephenotype(char *fileName, int line) {

   FILE* file = fopen(fileName, "r"); /* should check the result */

   if (file == NULL) {
      fprintf(stderr, "Can't open input file!\n");
      exit(1);
   }

   int output;

   char ignore[100];

   char id1[100];
   int int1;

   fgets(ignore, sizeof(ignore), file);
   fgets(ignore, sizeof(ignore), file);

   int cnt = 0;
   while (!feof(file)) {

      if (line != cnt) {
         fgets(ignore, sizeof(ignore), file);
         cnt++;
      } else {

         fscanf(file, "%s", &id1);
         fscanf(file, "%s", &id1);
         fscanf(file, "%d", &int1);
         fscanf(file, "%d", &int1);
         fscanf(file, "%d", &int1);
         if (line == cnt) {
            output = int1;
            break;
         }
      }
   }

   return output;
}

struct bgenpheno readsamplefileall(char *fileName, int line) {

   FILE* file = fopen(fileName, "r"); /* should check the result */

   if (file == NULL) {
      fprintf(stderr, "Can't open input file!\n");
      exit(1);
   }

   struct bgenpheno output;

   char ignore[100];

   char id1[100];
   int int1;
   float fl1;

   fgets(ignore, sizeof(ignore), file);
   fgets(ignore, sizeof(ignore), file);

   int cnt = 0;
   while (!feof(file)) {
      if (line != cnt) {
         fgets(ignore, sizeof(ignore), file);
         cnt++;
      } else {

         fscanf(file, "%s", &id1);
         if (line == cnt) {
            output.fid = id1;
         }

         fscanf(file, "%s", &id1);
         if (line == cnt) {
            output.iid = id1;
         }

         fscanf(file, "%f", &fl1);
         fscanf(file, "%f", &fl1);

         fscanf(file, "%d", &int1);
         if (line == cnt) {
            output.sex = int1;
         }

         fscanf(file, "%d", &int1);
         if (line == cnt) {
            output.phenotype = int1;
            break;
         }
      }
   }
   return output;
}
