/*
 * Copyright (C) 2016-2017 The Brigham and Women’s Hospital, Inc.
 * Portions contributed and copyright held by others as indicated.
 * All rights reserved.
 *
 * The contents of this file are subject to the terms of the BSD 3-clause "New" or "Revised"
 * License (the "License"). You may not use this file except in compliance with the License.
 *
 * You should have received a copy of the License in a separate text file originally named
 * "LICENSE_BWH.md"; if not, refer to "BSD-3-Clause" license at https://spdx.org/licenses/.
 */

#ifndef EFM__GENETIC_H
#define EFM__GENETIC_H

#include <stdlib.h>
#include <math.h>
#include <stdbool.h>

#include <plinkio/types.h>

/**
 * The following enum is a modification of its original version distributed under the Modified BSD License in the
 * libplinkio library (Copyright (c) 2012-2013, Mattias Frånberg). See the COPYING_libplinkio file for details.
 *
 * Inheritance modes:
 *
 * 0 - Additive with 0/1/2 values for GENOTYPE_HOMOZYGOUS_MAJOR / GENOTYPE_HETEROZYGOUS / GENOTYPE_HOMOZYGOUS_MINOR
 * 1 - Dominant with 0/1 values for GENOTYPE_HOMOZYGOUS_MAJOR / GENOTYPE_HETEROZYGOUS or GENOTYPE_HOMOZYGOUS_MINOR
 * 2 - Recessive with 0/1 values for GENOTYPE_HOMOZYGOUS_MAJOR or GENOTYPE_HETEROZYGOUS / GENOTYPE_HOMOZYGOUS_MINOR
 */
typedef enum {
    INH_MODE_ADDITIVE = 0,
    INH_MODE_DOMINANT = 1,
    INH_MODE_RECESSIVE = 2
} inheritance_mode_t;

/**
 * The following enum is a modification of its original version distributed under the Modified BSD License in the
 * libplinkio library (Copyright (c) 2012-2013, Mattias Frånberg). See the COPYING_libplinkio file for details.
 *
 * Genotypes:
 *
 * 0 - Homozygous minor (aa)
 * 1 - Hetrozygous (Aa)
 * 2 - Homozygous major (AA)
 * 3 - missing genotype
 */
typedef enum {
    GENOTYPE_HOMOZYGOUS_MINOR = 0,
    GENOTYPE_HETEROZYGOUS = 1,
    GENOTYPE_HOMOZYGOUS_MAJOR = 2,
    GENOTYPE_MISSING = 3
} genotype_t;

/**
 * Nucleotides
 */
typedef enum {
    ADENINE = 'A',
    GUANINE = 'G',
    CYTOSINE = 'C',
    THYMINE = 'T',
    URACYL = 'U'
} nucleobase_t;

/**
 Test if string contains a valid DNA nucleobase symbol (A/G/C/T)

 @param s pointer to string to test

 @return true if s is a 1-character long string containing valid DNA nucleobase symbol or false otherwise
 */
inline bool is_str_dna_base(char *s){
    return (s[1] == '\0' && (s[0] == ADENINE || s[0] == GUANINE || s[0] == CYTOSINE || s[0] == THYMINE)) ? true : false;
}

/**
 Convert genotype data into floating point data imposing mode of inheritance

 @param genotype    genotype (see genotype_t)
 @param inheritance inheritance mode (see inheritance_mode_t)

 @return Floating point representation of genmotype shall be returned
 */
inline float genotype_to_float(genotype_t genotype, inheritance_mode_t inheritance){
    if (inheritance == INH_MODE_RECESSIVE && genotype == GENOTYPE_HOMOZYGOUS_MINOR){
        return 1.0f;
    };

    if (inheritance == INH_MODE_DOMINANT && (genotype == GENOTYPE_HETEROZYGOUS || genotype == GENOTYPE_HOMOZYGOUS_MINOR)){
        return 1.0f;
    };

    if (inheritance == INH_MODE_ADDITIVE){
        if (genotype == GENOTYPE_HETEROZYGOUS){
            return 1.0f;
        };
        if (genotype == GENOTYPE_HOMOZYGOUS_MINOR){
            return 2.0f;
        }
    };

    return 0.0f;
}

inline float genotype_bgl_to_float(float* genotype, size_t sample_idx, inheritance_mode_t inheritance, bgl_format_t format){

    genotype = & genotype[sample_idx * format];

    if(inheritance == INH_MODE_ADDITIVE){
        //todo clarify this case
        return (genotype[0] * 2.0 + genotype[1]) / 2.0;
    }else if(inheritance == INH_MODE_RECESSIVE){
        return genotype[0];
    }else if(inheritance == INH_MODE_DOMINANT){
        return genotype[0] + genotype[1];
    }else{
        return nanf("");
    }
}

#endif /* EFM__GENETIC_H */
