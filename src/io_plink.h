/*
 * Copyright (C) 2016-2017 The Brigham and Women’s Hospital, Inc.
 * Portions contributed and copyright held by others as indicated.
 * All rights reserved.
 *
 * The contents of this file are subject to the terms of the BSD 3-clause "New" or "Revised"
 * License (the "License"). You may not use this file except in compliance with the License.
 *
 * You should have received a copy of the License in a separate text file originally named
 * "LICENSE_BWH.md"; if not, refer to "BSD-3-Clause" license at https://spdx.org/licenses/.
 */

// PLINK binary file-set format is documented at http://zzz.bwh.harvard.edu/plink/data.shtml and http://zzz.bwh.harvard.edu/plink/binary.shtml
#ifndef EFM__IO_PLINK_H
#define EFM__IO_PLINK_H

#include <stddef.h>

#define PLINK_BED_MAGIC1        0x6C
#define PLINK_BED_MAGIC2        0x1B

#define PLINK_BIM_FILE_EXT      ".bim"
#define PLINK_FAM_FILE_EXT      ".fam"
#define PLINK_BED_FILE_EXT      ".bed"

#define PLINK_BIM_NO_OF_COLS    4

typedef unsigned char snp_t;

typedef enum {
   PLINK_IO_STATUS_OK = 0, PLINK_IO_UNKNOWN_ERROR, PLINK_IO_PARAM_ERROR, PLINK_IO_FS_ERROR
} io_plink_status_t;

typedef enum {
   PLINK_BED_IND_MAJOR = 0x00, PLINK_BED_SNP_MAJOR = 0x01
} plink_bed_mode_t;

/**
 PLINK BED file handle
 
 BED file format is the binary PED file format
 
 The BED-file contains packed genotype data (up to four SNPs per byte)
 
 */
typedef struct {
   char* file_name;
   size_t file_size;
   plink_bed_mode_t mode;
   snp_t *snp_buffer;
} plink_bed_file_t;

/**
 SNP Locus Info
 */
typedef struct {
   char* chromosome;
   char* rs;
   float distance;
   size_t position;
   char allele_1; // by default minor allele
   char allele_2; // by default major allele
} plink_snp_locus_t;

/**
 PLINK BIM file
 
 BIM file format is an extended MAP file format (two extra columns for allele names)
 
 */
typedef struct {
   plink_snp_locus_t *snps;
   size_t locus_ct;
   size_t locus_sz;
} plink_bim_file_t;

/**
 PLINK FAM file
 */
typedef struct {

} plink_fam_file_t;

typedef struct {
   plink_bim_file_t bim;
   plink_fam_file_t fam;
   plink_bed_file_t bed;
   size_t snp_ct;
   size_t sample_ct;
} plink_fileset_t;

// SNP Genotype Data
typedef struct {

} plink_snp_genotype_t;

// Individual Info
typedef struct {

} plink_individual_t;

#endif /* EFM__IO_PLINK_H */
