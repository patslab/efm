/*
 * Copyright (C) 2016-2017 The Brigham and Women’s Hospital, Inc.
 * Portions contributed and copyright held by others as indicated.
 * All rights reserved.
 *
 * The contents of this file are subject to the terms of the BSD 3-clause "New" or "Revised"
 * License (the "License"). You may not use this file except in compliance with the License.
 *
 * You should have received a copy of the License in a separate text file originally named
 * "LICENSE_BWH.md"; if not, refer to "BSD-3-Clause" license at https://spdx.org/licenses/.
 */

#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/mman.h>
#include <sys/stat.h>

#include "common.h"
#include "io_report.h"

io_report_file_t* open_report_file(char* file_name, size_t num_of_sections, size_t lines_per_section, size_t line_length, bool header){
    io_report_file_t* file = NULL;
    int fd = -1;
    void* mmap_ptr = NULL;
    size_t report_file_size = line_length * (num_of_sections * lines_per_section + (header ? 1 : 0));
    
    if (!file_name) return NULL;
    if (report_file_size < 1 || report_file_size > MAX_REPORT_FILE_SIZE) return NULL;
    
    if ((fd = open(file_name, O_RDWR | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)) == -1 ){
        Error("Could not create file (errno=%d)", errno);
        return NULL;
    }
    
    // adjusting the report filesize
#ifdef __MACH__ // macOS doesn't support posix_fallocate(); ftruncate() is used instead
    if (ftruncate(fd, report_file_size))
#else
        if (posix_fallocate(fd, 0, report_file_size))
#endif
        {
            Error("Could not resize file; errno=%d", errno);
            close(fd);
            return NULL;
        }
    
    // memory-mapped file I/O
    mmap_ptr = (char*) mmap(NULL, report_file_size, PROT_WRITE, MAP_SHARED, fd, 0);
    if (mmap_ptr == MAP_FAILED){
        Error("Filesystem error: unsuccessful mmap(); errno=%d", errno);
        close(fd);
        return NULL;
    }
    
    file = (io_report_file_t*) calloc(1, sizeof(io_report_file_t));
    file->fd = fd;
    file->file_name = strdup(file_name);
    file->header = header;
    file->sect_ct = num_of_sections;
    file->lines_per_sect = lines_per_section;
    file->line_len = line_length;
    file->file_sz = report_file_size;
    file->mmap_ptr = mmap_ptr;
    
    return file;
}

io_report_file_t* open_report_file_2(char* file_name, size_t num_of_sections, size_t rows_per_section, size_t num_of_columns, size_t *column_widht, bool header){
    io_report_file_t* file = NULL;
    int fd = -1;
    void* mmap_ptr = NULL;
    
    if (!file_name) return NULL;
    
    size_t line_length = sizeof(char);
    
    for (int i = 0; i < num_of_columns; i++){
        if (column_widht[i]){
            line_length += column_widht[i];
        }else{
            Error("Invalid column width specification");
            return NULL;
        }
    }
    
    size_t report_file_size = line_length * (num_of_sections * rows_per_section + (header ? 1 : 0));
    
    if (report_file_size < 1 || report_file_size > MAX_REPORT_FILE_SIZE) return NULL;
    
    if ((fd = open(file_name, O_RDWR | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)) == -1 ){
        Error("Could not create file (errno=%d)", errno);
        return NULL;
    }
    
    // adjusting the report filesize
#ifdef __MACH__ // macOS doesn't support posix_fallocate(); ftruncate() is used instead
    if (ftruncate(fd, report_file_size))
#else
        if (posix_fallocate(fd, 0, report_file_size))
#endif
        {
            Error("Could not resize file; errno=%d", errno);
            close(fd);
            return NULL;
        }
    
    // memory-mapped file I/O
    mmap_ptr = (char*) mmap(NULL, report_file_size, PROT_WRITE, MAP_SHARED, fd, 0);
    if (mmap_ptr == MAP_FAILED){
        Error("Filesystem error: unsuccessful mmap(); errno=%d", errno);
        close(fd);
        return NULL;
    }
    
    file = (io_report_file_t*) calloc(1, sizeof(io_report_file_t));
    file->fd = fd;
    file->file_name = strdup(file_name);
    file->header = header;
    file->sect_ct = num_of_sections;
    file->lines_per_sect = rows_per_section;
    file->column_ct = num_of_columns;
    file->column_width = (size_t*) malloc(sizeof(size_t) * num_of_columns);
    for (int i = 0; i < file->column_ct; i++){
        file->column_width[i] = column_widht[i];
    }
    file->line_len = line_length;
    file->file_sz = report_file_size;
    file->mmap_ptr = mmap_ptr;
    
    return file;
}

int close_report_file(io_report_file_t* file){
    int retval = 1;
    
    if (!file) return retval;
    
    if (munmap(file->mmap_ptr, file->file_sz) != 0){
        Error("Filesystem error: unsuccessful munmap(); errno=%d", errno);
        return retval;
    }
    
    if (close(file->fd)){
        Error("Could not close file (errno=%d)", errno);
        return retval;
    }
    
    if (file->file_name) free(file->file_name);
    if (file->column_width) free(file->column_width);
    free(file);
    
    retval = 0;
    
    return retval;
}

// these extern "definitions" are required for proper handling of inline functions in C99
extern char* report_file_sect_ptr(io_report_file_t* file, size_t sect_idx);
extern char* report_file_hdr_ptr(io_report_file_t* file);
