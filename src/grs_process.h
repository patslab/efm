/*
 * Copyright (C) 2016-2017 The Brigham and Women’s Hospital, Inc.
 * Portions contributed and copyright held by others as indicated.
 * All rights reserved.
 *
 * The contents of this file are subject to the terms of the BSD 3-clause "New" or "Revised"
 * License (the "License"). You may not use this file except in compliance with the License.
 *
 * You should have received a copy of the License in a separate text file originally named
 * "LICENSE_BWH.md"; if not, refer to "BSD-3-Clause" license at https://spdx.org/licenses/.
 */

#ifndef EFM__GRS_PROCESS_H
#define EFM__GRS_PROCESS_H

#include "common.h"

#define GRS_DEFAULT_BLOCK_SIZE      256
#define GRS_OR_FILE_FIELD_CT        6
#define GRS_OR_FILE_MAX_LINE_LEN    1024
#define GRS_OR_FILE_DELIMITER       " \n\r"
#define GRS_REPORT_NEW_LINE_CHR     '\n'
#define GRS_REPORT_NEW_LINE_CHR_LEN 1

#define GRS_REPORT_GRS_FILE_EXT     ".grs"
#define GRS_REPORT_WGRS_FILE_EXT    ".wgrs"
#define GRS_REPORT_LWGRS_FILE_EXT   ".lwgrs"
#define GRS_REPORT_INFO_FILE_EXT   ".info"

#define GRS_REPORT_FID_STR          "FID"
#define GRS_REPORT_IID_STR          "IID"
#define GRS_REPORT_FID_FMT          " %21s"
#define GRS_REPORT_IID_FMT          " %21s"
#define GRS_REPORT_HDR_FMT          " %19s_%1s"
#define GRS_REPORT_VAL_FMT          " %21.9g"

#define GRS_REPORT_INFO_HDR_CHR_FMT "%4s"
#define GRS_REPORT_INFO_HDR_CHR_STR "CHR"
#define GRS_REPORT_INFO_HDR_SNP_ID  "SNP"
#define GRS_REPORT_INFO_HDR_POS_FMT "%12s"
#define GRS_REPORT_INFO_HDR_POS_STR "BP"
#define GRS_REPORT_INFO_HDR_AL1_STR "A1"
#define GRS_REPORT_INFO_HDR_AL2_STR "A2"
#define GRS_REPORT_INFO_HDR_N_FMT   "%12s"
#define GRS_REPORT_INFO_HDR_N_STR   "N"
#define GRS_REPORT_INFO_HDR_ST_STR  "STATUS"
#define GRS_REPORT_INFO_CHR_NO_FMT  " %3d"
#define GRS_REPORT_INFO_SNP_ID_FMT  " %21s"
#define GRS_REPORT_INFO_POS_BP_FMT  " %11llu"
#define GRS_REPORT_INFO_SMPL_CT_FMT " %11lu"
#define GRS_REPORT_INFO_ALL_NM_FMT  " %3s"
#define GRS_REPORT_INFO_STATUS_FMT  " %9s"
#define GRS_REPORT_INFO_SKIPPED_STR "A/T,C/G"
#define GRS_REPORT_INFO_FLIPPED_STR "flipped"
#define GRS_REPORT_INFO_OK_STR      "ok"

#define GRS_REPORT_LINES_PER_SECT   1
#define GRS_REPORT_FIELD_LEN        22
#define GRS_REPORT_INFO_LINE_LEN    69

typedef struct {
    unsigned char chromosome;
    long long bp_position;
    char *snp_name;
    char *allele1;
    char *allele2;
    double or_val;
} snp_or_t;

typedef struct {
    unsigned char chromosome;
    long long bp_position;
    char *snp_name;
    char *allele1;
    char *allele2;
    double or_val;
    float *score;
    size_t sample_ct;
    bool alleles_swapped;
    STATUS status;
} grs_process_result_t;

int grs_process(const char *plink_file_prefix, const char *aux_input_file, const char *output_fileset_prefix, size_t num_of_threads, unsigned long int config_flags);

#endif /* EFM__GRS_PROCESS_H */
