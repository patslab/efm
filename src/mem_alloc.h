/*
 * Copyright (C) 2016-2017 The Brigham and Women’s Hospital, Inc.
 * Portions contributed and copyright held by others as indicated.
 * All rights reserved.
 *
 * The contents of this file are subject to the terms of the BSD 3-clause "New" or "Revised"
 * License (the "License"). You may not use this file except in compliance with the License.
 *
 * You should have received a copy of the License in a separate text file originally named
 * "LICENSE_BWH.md"; if not, refer to "BSD-3-Clause" license at https://spdx.org/licenses/.
 */

#ifndef EFM__MEM_ALLOC_H
#define EFM__MEM_ALLOC_H

#include "config.h"

#ifdef USE_TBB_SCALABLE_ALLOCATOR
	#include <tbb/scalable_allocator.h>
	#define my_mm_malloc(size)		scalable_aligned_malloc((size), ALIGNMENT)
	#define my_mm_free(ptr)			scalable_aligned_free(ptr)
#else // POSIX
	#include <stdlib.h>
	#define my_mm_malloc(size)	({ void *ptr; posix_memalign(&ptr, ALIGNMENT,(size)) ? NULL : ptr; })
	#define my_mm_free(ptr)		free(ptr)
#endif

// Calculate number of floats needed to have a padded buffer of floats
// This is to avoid the next element in the array to start on a non-padded address
// If PADDING is set to ALIGNEMNT, the next element in the array will also be aligned
inline size_t padded_float_buffer_size(size_t n){
    return ((n*sizeof(float) + PADDING - 1) & ( ~ ( PADDING - 1 ) )) / sizeof(float);
}

#endif /* EFM__MEM_ALLOC_H */
