/*
 * Copyright (C) 2016-2017 The Brigham and Women�s Hospital, Inc.
 * Portions contributed and copyright held by others as indicated.
 * All rights reserved.
 *
 * The contents of this file are subject to the terms of the BSD 3-clause "New" or "Revised"
 * License (the "License"). You may not use this file except in compliance with the License.
 *
 * You should have received a copy of the License in a separate text file originally named
 * "LICENSE_BWH.md"; if not, refer to "BSD-3-Clause" license at https://spdx.org/licenses/.
 */

#ifndef EFM__VERSION_H
#define EFM__VERSION_H

#define EXECUTABLE_NAME "efm"
#define VERSION_MAJOR 0
#define VERSION_MINOR 1
#define VERSION_IDENTIFIER "RC1 " //Leave a space at the end for better readability
#define VERSION_DATE "3 Sep 2018"

#endif /* EFM__VERSION_H */
