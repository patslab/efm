/*
 * Copyright (C) 2016-2017 The Brigham and Women’s Hospital, Inc.
 * Portions contributed and copyright held by others as indicated.
 * All rights reserved.
 *
 * The contents of this file are subject to the terms of the BSD 3-clause "New" or "Revised"
 * License (the "License"). You may not use this file except in compliance with the License.
 *
 * You should have received a copy of the License in a separate text file originally named
 * "LICENSE_BWH.md"; if not, refer to "BSD-3-Clause" license at https://spdx.org/licenses/.
 */

#ifndef EFM__STAT_H
#define EFM__STAT_H

#include <math.h>
#include <unistd.h>

/**
 Deviance
 
 D = -2 * Log-Lik

 @param loglik log-likelihood

 @return Deviance in double floating point precision
 */
inline double deviance(double loglik) {
   return (-2.0 * loglik);
}

/**
 Akaike Information Criterion with correction for finite sample size

 AICc = -2 * Log-Lik + 2 * k + [(2 * k * (k + 1)) / (n - k - 1)]
 
 @param loglik log-likelihood
 @param k      number of model parameters
 @param n      sample size

 @return AICc value in double floating point precision
 */
inline double aicc(double loglik, size_t k, size_t n) {
   if (n - k < 2)
      return NAN;
   return (-2.0 * loglik + 2 * k + ((2 * k * (k + 1)) / (n - k - 1)));
}

/**
 Bayesian Information Criterion
 
 BIC = -2 * Log-Lik + ln(n) * k

 @param loglik log-likelihood
 @param k      number of model parameters
 @param n      sample size

 @return BIC value in double floating point precision
 */
inline double bic(double loglik, size_t k, size_t n) {
   return (-2.0 * loglik + log(n) * k);
}

/**
 Z-value for Maximum Likelihood Estimation: z = Coef / SE
 Standard Error (SE) is a sqrt of the estimated error variance: SE = sqrt (R)

 @param regress_coef regression coefficient
 @param std_error    standard error

 @return Z-value in extended floating point precision
 */
inline long double lr_z_val(double regress_coef, double std_error) {
   return (((long double) regress_coef) / std_error);
}

/**
 Two-sided P-value:
 p-value = 2 * ( 1 - CdfNorm(abs(zval))) = 2 * ( 1 - 1 + 0.5 * erfc(abs(zval) / sqrtl(2)) )
 p-value = erfc(abs(zval) / sqrtl(2))
 
 Cummulative Normal Distribution Function is calculated using the Error Function:
 CdfNorm(x) = 0.5 * [ 1 + erf(x / sqrtl(2)) ] , but since erf(x) = 1 - erfc(x), we get:
 CdfNorm(x) = 1 - 0.5 * erfc(x / sqrtl(2))

 @param z_value z-value in extended floating point precision

 @return p-value in extended floating point precision
 */
inline long double p_val(long double z_value) {
   return erfcl(fabsl(z_value) / sqrtl(2.0l));
}

/**
 Adjusted Odds Ratio

 @param regress_coef regression coefficient

 @return Adjusted OR in long double floating point precision
 */
inline long double lr_adj_or(double regress_coef) {
   return expl(regress_coef);
}

#endif /* EFM__STAT_H */
