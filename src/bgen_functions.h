/*
 * Copyright (C) 2016-2017 The Brigham and Women’s Hospital, Inc.
 * Portions contributed and copyright held by others as indicated.
 * All rights reserved.
 *
 * The contents of this file are subject to the terms of the BSD 3-clause "New" or "Revised"
 * License (the "License"). You may not use this file except in compliance with the License.
 *
 * You should have received a copy of the License in a separate text file originally named
 * "LICENSE_BWH.md"; if not, refer to "BSD-3-Clause" license at https://spdx.org/licenses/.
 */

#ifndef EFM__BGEN_FUNCTIONS_H
#define EFM__BGEN_FUNCTIONS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>		// For math routines (such as sqrt & trig).
#include <zlib.h>
#include <errno.h>

#include "bgen/bgen.h"

#define PLINK_TRANSPOSED_BED_EXTENSION ".t"
#define DEFAULT_OUT_FILESET_PREFIX      EXECUTABLE_NAME

// Global configuration shared across the project
int bgen_getsnps(const byte fileName[]);
int bgen_getsamples(const byte fileName[]);
char* bgen_getallele1(const byte fileName[], int snpid, Variant *variants);
char* bgen_getallele2(const byte fileName[], int snpid, Variant *variants);
char* bgen_getallele3(const byte fileName[], int snpid, Variant *variants);
char* bgen_getid(const byte fileName[], int snpid, Variant *variants);
int bgen_getpos(const byte fileName[], int snpid, Variant *variants);
char* bgen_getchromosome(const byte fileName[], int snpid, Variant *variants);
char* bgen_getchromosome2(const byte fileName[], int snpid, Variant *variants);
real* bgen_getgenotypicprobabilites(const byte fileName[], int number, int sample, VariantGenotype *vg, real *probabilities);
void readsamplefile(char *fileName, int line);
char *readsamplefilefamilyid(char *fileName, int line);
char *readsamplefilewithinfamilyid(char *fileName, int line);
int readsamplefilemissing(char *fileName, int line);
int readsamplefilesex(char *fileName, int line);
int readsamplefilephenotype(char *fileName, int line);
struct bgenpheno readsamplefileall(char *fileName, int line);

// Enabling assert() for debug builds only
#ifdef DEBUG
#undef NDEBUG
#include <assert.h>
#else
#define NDEBUG
#include <assert.h>
#endif

#ifdef DEBUG
// IN MAGENTA
#define Debug(format, ...)		if(g_conf & CONF_DEBUG) fprintf(stderr, "\033[35mDEBUG: " format " (%s:%d)\033[0m\n",  ##__VA_ARGS__, basename(__FILE__), __LINE__)
#else
#define Debug(format, ...)
#endif
// IN YELLOW
#define Warning(format, ...)    fprintf(stderr, "\033[33mWARNING: " format "\033[0m\n",  ##__VA_ARGS__)
// IN RED
#define Error(format, ...)		fprintf(stderr, "\033[31mERROR: " format "\033[0m\n",  ##__VA_ARGS__)
// IN NAVY
//#define Verbose(format, ...)	fprintf(stderr, "\033[34mVERBOSE: " format "\033[0m\n",  ##__VA_ARGS__)
#define Info(format, ...)		fprintf(stdout, format "\n", ##__VA_ARGS__)

#ifdef USE_OPEN_MP
#include <omp.h>
#define ClearTimeElapsed(config, time_stamp)		if(config & CONF_VERBOSE) do { *time_stamp=omp_get_wtime(); } while(0)
#define ShowTimeElapsed(config, time_stamp, msg)	if(config & CONF_VERBOSE) do { Verbose("%s in %.2f sec.", msg, omp_get_wtime() - *time_stamp); *time_stamp=omp_get_wtime(); } while(0)
#else
#define ClearTimeElapsed(config, time_stamp)
#define ShowTimeElapsed(config, time_stamp, msg)
#endif

#endif /* EFM__VCF_GZIPPED_H */
