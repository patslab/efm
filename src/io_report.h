/*
 * Copyright (C) 2016-2017 The Brigham and Women’s Hospital, Inc.
 * Portions contributed and copyright held by others as indicated.
 * All rights reserved.
 *
 * The contents of this file are subject to the terms of the BSD 3-clause "New" or "Revised"
 * License (the "License"). You may not use this file except in compliance with the License.
 *
 * You should have received a copy of the License in a separate text file originally named
 * "LICENSE_BWH.md"; if not, refer to "BSD-3-Clause" license at https://spdx.org/licenses/.
 */

#ifndef EFM__IO_REPORT_H
#define EFM__IO_REPORT_H

#include <stdbool.h>
#include "types.h"

/**
 Report file descriptor
 */
typedef struct {
    int fd;
    char* file_name;
    size_t sect_ct;
    size_t lines_per_sect;
    size_t column_ct;
    size_t *column_width;
    size_t line_len;
    size_t file_sz;
    char* mmap_ptr;
    bool header;
} io_report_file_t;

/**
 Open a report file using memory-mapped I/O
 
 @param file_name           file name
 @param num_of_sections     number of sections in the report file
 @param lines_per_section   number of lines per section of the report file
 @param line_length         length of each line (all lines will have the same length)
 @param header              bool switch defining whether there is a header
 
 @return A pointer to report_file_t shall be returned on success; a NULL pointer shall be returned otherwise.
 */
io_report_file_t* open_report_file(char* file_name, size_t num_of_sections, size_t lines_per_section, size_t line_length, bool header);

//todo document
io_report_file_t* open_report_file_2(char* file_name, size_t num_of_sections, size_t rows_per_section, size_t num_of_columns, size_t *column_widht, bool header);

/**
 Close the report file
 
 @param file pointer to report_file_t (obtained by calling open_report_file())
 
 @return Zero shall be retunred on success; a non-zero value shall be returned otherwise.
 */
int close_report_file(io_report_file_t* file);

/**
 Return a pointer to section of memory-mapped region of the report file
 
 @param file     pointer to report_file_t (obtained by calling open_report_file())
 @param sect_idx index of the section in the report file
 
 @return A pointer to character buffer (char*) of the section in the report file shall be retutned on success; a NULL pointer shall be returned otherwise.
 */
inline char* report_file_sect_ptr(io_report_file_t* file, size_t sect_idx){
    size_t offset = (sect_idx * file->lines_per_sect + (file->header ? 1 : 0)) * file->line_len;
    if (!file) return NULL;
    if (offset >= file->file_sz) return NULL;
    return &(file->mmap_ptr[offset]);
}

/**
 Return a pointer to header of memory-mapped region of the report file

 @param file pointer to report_file_t (obtained by calling open_report_file())

 @return A pointer to character buffer (char*) of the header in the report file shall be retutned on success; a NULL pointer shall be returned otherwise.
 */
inline char* report_file_hdr_ptr(io_report_file_t* file){
    if (!file) return NULL;
    return (file->mmap_ptr);
}

inline char* io_report_hdr_ptr(io_report_file_t* file){
    if (!file) return NULL;
    return (file->mmap_ptr);
}

inline char* io_report_sect_ptr(io_report_file_t* file, size_t sect_idx){
    size_t offset = (sect_idx * file->lines_per_sect + (file->header ? 1 : 0)) * file->line_len;
    if (!file) return NULL;
    if (offset >= file->file_sz) return NULL;
    return &(file->mmap_ptr[offset]);
}

/*
inline char* io_report_line_ptr(io_report_file_t* file, char* sect_ptr, size_t line_num){
    size_t offset = sect_idx * file->lines_per_sect + (file->header ? 1 : 0)) * file->line_len;
    size_t offset = (sect_idx * file->lines_per_sect + (file->header ? 1 : 0)) * file->line_len;
    if (!file) return NULL;
    if (offset >= file->file_sz) return NULL;
    return &(file->mmap_ptr[offset]);
}
*/

//todo
inline STATUS write_report_file_sect(io_report_file_t* file, size_t sect_idx){
    char* section = report_file_sect_ptr(file, sect_idx);
    if (!section) return STATUS_UNKNOWN_ERROR;
    
    return STATUS_OK;
}

#endif /* EFM__IO_REPORT_H */
