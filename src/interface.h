/*
 * Copyright (C) 2016-2017 The Brigham and Women’s Hospital, Inc.
 * Portions contributed and copyright held by others as indicated.
 * All rights reserved.
 *
 * The contents of this file are subject to the terms of the BSD 3-clause "New" or "Revised"
 * License (the "License"). You may not use this file except in compliance with the License.
 *
 * You should have received a copy of the License in a separate text file originally named
 * "LICENSE_BWH.md"; if not, refer to "BSD-3-Clause" license at https://spdx.org/licenses/.
 */

#ifndef EFM__INTERFACE_H
#define EFM__INTERFACE_H

#include "common.h"

#define TERMINAL_LINE_LENGTH 120
#define VERBS_FIRST_COLUMN_FRACT 0.70
#define OPTS_FIRST_COLUMN_FRACT 0.35

// Names of commands
#define CMD_HELP_NAME		"help"
#define CMD_VERSION_NAME	"version"
#define CMD_LOGISTIC_REGRESSION_NAME	"logisticRegression"
#define CMD_LINEAR_REGRESSION_NAME  "linearRegression" //LINEAR:1
#define CMD_GRS_NAME        "geneticRiskScore"
#define CMD_TRANSPOSE_NAME	"transposeBedFile"

// Description of commands
#define CMD_HELP_DESC		"show usage"
#define CMD_VERSION_DESC	"show version number"
#define CMD_LOGISTIC_REGRESSION_DESC	"run logistic regression"
#define CMD_LINEAR_REGRESSION_DESC   "run linear regression"
#define CMD_GRS_DESC        "calculate Genetic Risk Score"
#define CMD_TRANSPOSE_DESC	"transpose the BED-file"

// Debug option available in debug build only
#ifdef DEBUG
#define CMD_DEBUG_OPT_LETTER "d"
#else
#define CMD_DEBUG_OPT_LETTER ""
#endif

// List of required options for commands
#define CMD_HELP_OPTS_REQ		""
#define CMD_VERSION_OPTS_REQ	""
#define CMD_LOGISTIC_OPTS_REQ	"f"
#define CMD_GRS_OPTS_REQ        "f"
#define CMD_TRANSPOSE_OPTS_REQ	"f"

// List of options available for commands
#define CMD_HELP_OPTS_OPT		CMD_DEBUG_OPT_LETTER ""
#define CMD_VERSION_OPTS_OPT	CMD_DEBUG_OPT_LETTER ""
#define CMD_LOGISTIC_OPTS_OPT	CMD_DEBUG_OPT_LETTER "octvgnmibxk"
#define CMD_GRS_OPTS_OPT        CMD_DEBUG_OPT_LETTER "viog"
#define CMD_TRANSPOSE_OPTS_OPT	CMD_DEBUG_OPT_LETTER "o"

// Short option names
//TODO: Maybe redefine letters.
#define OPT_DEBUG_VAL			'd'
#define OPT_INFILE_VAL			'f'
#define OPT_VERBOSE_VAL			'v'
#define OPT_INHERIT_VAL			'g'
#define OPT_COVAR_VAL			'c'
#define OPT_THREADS_VAL			't'
#define OPT_OUTFILE_VAL			'o'
#define OPT_AUX_INFILE_VAL		'i'
#define OPT_NEW_COVAR_VAL		'n'
#define OPT_MODEL_REPORT_VAL    'm'
#define OPT_INPUT_BEAGLE_VAL    'b'
#define OPT_INPUT_BGEN_VAL    'e' //BGEN_VCF:1
#define OPT_INPUT_VCF_VAL    'a'//BGEN_VCF:1
#define OPT_INPUT_VCF_GZIPPED_VAL    'z'//BGEN_VCF:1
#define OPT_DOUBLE_X_MALE_VAL   'x'
#define OPT_BLOCKSIZE_VAL       'k'

// Long option names
#define OPT_DEBUG_NAME			"debug"
#define OPT_INFILE_NAME			"input-file-set-prefix"
#define OPT_VERBOSE_NAME		"verbose"
#define OPT_INHERIT_NAME		"genetic-inheritance"
#define OPT_COVAR_NAME			"covar-file"
#define OPT_AUX_INFILE_NAME		"input-file"
#define OPT_COVAR_LIST_NAME		"covar-list"
#define OPT_THREADS_NAME		"thread_num"
#define OPT_OUTFILE_NAME		"output-file-set-prefix"
#define OPT_NEW_COVAR_NAME		"new-covar"
#define OPT_INTERACTION_NAME	"interaction"
#define OPT_SNP_COVAR_NAME      "snp-cov"
#define OPT_MODEL_REPORT_NAME   "model-report"
#define OPT_INPUT_BEAGLE_NAME   "beagle-input"
#define OPT_INPUT_BGEN_NAME   "bgen-input" //BGEN_VCF:1
#define OPT_INPUT_VCF_NAME   "vcf-input" //BGEN_VCF:1
#define OPT_INPUT_VCF_GZIPPED_NAME   "vcf-gz-input" //BGEN_VCF:1
#define OPT_DOUBLE_X_MALE_NAME  "double-x-male"
#define OPT_BLOCKSIZE_NAME      "blocksize"

// Option descriptions
#define OPT_DEBUG_DESC			"print debug info to standard error"
#define OPT_INFILE_DESC			"PLINK file-set prefix (BED/BIM/FAM)"
#define OPT_VERBOSE_DESC		"show info and time elapsed for steps"
#define OPT_INHERIT_DESC		"inheritance model: 'd'=dominant, 'r'=recessive or 'a'=additive (default)"
#define OPT_COVAR_DESC			"covariates file (PLINK-formatted)"
#define OPT_COVAR_LIST_DESC		"list of covariates to include in the model"
#define OPT_THREADS_DESC		"number of threads"
#define OPT_OUTFILE_DESC		"output file-set prefix (default: " DEFAULT_OUT_FILESET_PREFIX ")"
#define OPT_AUX_INFILE_DESC     "input file"
#define OPT_NEW_COVAR_DESC		"create and add new covariate as specified"
#define OPT_INTERACTION_DESC	"include interactions in the model"
#define OPT_SNP_COVAR_DESC      "expand the model covariates with SNP(s)"
#define OPT_MODEL_REPORT_DESC   "report information criterion info for all models run"
#define OPT_INPUT_BEAGLE_DESC   "input file contains genotype probability/likelihood in BEAGLE format"
#define OPT_INPUT_BGEN_DESC   "input file contains genotype probability/likelihood in BGEN format" //BGEN_VCF:1
#define OPT_INPUT_VCF_DESC   "input file contains genotype probability/likelihood in VCF format" //BGEN_VCF:1
#define OPT_INPUT_VCF_GZIPPED_DESC   "input file contains genotype probability/likelihood in VCF format (GZipped)" //BGEN_VCF:1
#define OPT_DOUBLE_X_MALE_DESC  "double chromosome X dosage for males"
#define OPT_BLOCKSIZE_DESC      "define the number of SNP's each thread reads before processing"
//TODO: Add BGEN / VCF / VCFGZIP Options

/**
 Process command line options

 @param argc    cmd line argument count
 @param argv    cmd line arguments
 @param cmd_idx command index
 */
void process_command_line_opts(int argc, char **argv, int cmd_idx);

/**
 Process cmd line

 @param argc argument count
 @param argv argument array
 */
void process_command_line(int argc, char **argv);

/**
 Print version info
 */
void print_version();

/**
 Print usage info
 */
void print_usage();

#endif /* EFM__INTERFACE_H */
