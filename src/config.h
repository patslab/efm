/*
 * Copyright (C) 2016-2017 The Brigham and Women’s Hospital, Inc.
 * Portions contributed and copyright held by others as indicated.
 * All rights reserved.
 *
 * The contents of this file are subject to the terms of the BSD 3-clause "New" or "Revised"
 * License (the "License"). You may not use this file except in compliance with the License.
 *
 * You should have received a copy of the License in a separate text file originally named
 * "LICENSE_BWH.md"; if not, refer to "BSD-3-Clause" license at https://spdx.org/licenses/.
 */

#ifndef EFM__CONFIG_H
#define EFM__CONFIG_H

#include <stdint.h>

/**
 * Global configuration
 */

// MKL
#include <mkl_version.h>
#if defined(__INTEL_MKL__)
	#define USE_INTEL_MKL
	#define USE_INTEL_MKL_VMF
#endif

// OMP
#if defined(_OPENMP)
	#define USE_OPEN_MP
#endif

// ALPHA OPTIONS

#define DEFAULT_NUMBER_OF_THREADS 2
#define MAX_NUMBER_OF_THREADS 512

// Maximum output file size (64GB)
#define MAX_REPORT_FILE_SIZE UINT64_C(64*1024*1024*1024)

//#define USE_TBB_SCALABLE_ALLOCATOR

#define USE_PREALLOCATED_BUFFERS

#define USE_COLUMN_MAJOR_MATRIX // Row-Major is used otherwise (need fix)

#define MAX_FILENAME_LENGTH 96

#define ALIGNMENT 32

#define PADDING ALIGNMENT

#if ALIGNMENT < 16
#error "Too small alignment factor: it is assumed here that alignment of at least 16 bytes might be efficient"
#endif

#if ((ALIGNMENT - 1) & ALIGNMENT)
#error "Invalid alignment (must be power of 2)"
#endif

#if PADDING < 16
#error "Too small padding factor: it is assumed here that padding of at least 16 bytes might be efficient"
#endif

#if ((PADDING - 1) & PADDING)
#error "Invalid padding (must be power of 2)"
#endif

#if (PADDING > ALIGNMENT)
#error "Too large padding factor: it is assumed here that padding should be no larger than alignment"
#endif

#endif /* EFM__CONFIG_H */
