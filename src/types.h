/*
 * Copyright (C) 2016-2017 The Brigham and Women’s Hospital, Inc.
 * Portions contributed and copyright held by others as indicated.
 * All rights reserved.
 *
 * The contents of this file are subject to the terms of the BSD 3-clause "New" or "Revised"
 * License (the "License"). You may not use this file except in compliance with the License.
 *
 * You should have received a copy of the License in a separate text file originally named
 * "LICENSE_BWH.md"; if not, refer to "BSD-3-Clause" license at https://spdx.org/licenses/.
 */

#ifndef EFM__TYPES_H
#define EFM__TYPES_H

#include "config.h"

#define ALG_ATTR __attribute__((aligned(ALIGNMENT)))

#ifdef __INTEL_COMPILER
#define ALG_VAL_ATTR __attribute__((align_value(ALIGNMENT)))
#else
#define ALG_VAL_ATTR
#endif

typedef size_t ALG_ATTR alg_size;
typedef size_t* ALG_VAL_ATTR alg_size_ptr;

typedef int ALG_ATTR alg_int;
typedef int* ALG_VAL_ATTR alg_int_ptr;

typedef float ALG_ATTR alg_float;
typedef float* ALG_VAL_ATTR alg_float_ptr;

typedef double ALG_ATTR alg_double;
typedef double* ALG_VAL_ATTR alg_double_ptr;

typedef unsigned char snp_t;

typedef enum status_t {
   STATUS_OK = 0, STATUS_MALLOC_ERROR, STATUS_PARAM_ERROR, STATUS_MKL_ERROR, STATUS_UNKNOWN_ERROR, STATUS_MKL_LAPACK_ERROR, STATUS_IO_ERROR
} STATUS;

typedef enum config_flags_t {
   CONF_NULL = 0x000000,
   CONF_TRANSPOSE = 0x000001,
   CONF_LOGISTIC = 0x000002, //TODO: Maybe change name to LOGISTIC_REGRESSION
   CONF_LINEAR = 0x000003, //TODO: Maybe change name to LINEAR_REGRESSION
   CONF_FILESET = 0x000004,
   CONF_COVARFILE = 0x000008,
   CONF_OUTFILE = 0x000010,
   CONF_INPUT_PLINK = 0x000020,
   CONF_INPUT_BEAGLE = 0x000040,
   CONF_INPUT_BGEN = 0x000050,//BGEN_VCF:3
   CONF_INPUT_VCF = 0x000060,//BGEN_VCF:3
   CONF_INPUT_VCF_GZIPPED = 0x000061,//BGEN_VCF:3
   CONF_NEW_COVAR = 0x000080,
   CONF_INH_ADD = 0x000100,
   CONF_INH_REC = 0x000200,
   CONF_INH_DOM = 0x000400,
   CONF_DBL_X_MALE = 0x000800,
   CONF_DEBUG = 0x001000,
   CONF_HELP = 0x002000,
   CONF_VERSION = 0x004000,
   CONF_FATAL_ERROR = 0x008000,
   CONF_MODEL_REPORT = 0x020000,
   CONF_GRS = 0x080000,
   CONF_AUX_INFILE = 0x100000,
   CONF_VERBOSE = 0x200000,
} CONFIG_FLAGS;

#endif /* EFM__TYPES_H */
