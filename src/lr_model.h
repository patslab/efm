/*
 * Copyright (C) 2016-2017 The Brigham and Women’s Hospital, Inc.
 * Portions contributed and copyright held by others as indicated.
 * All rights reserved.
 *
 * The contents of this file are subject to the terms of the BSD 3-clause "New" or "Revised"
 * License (the "License"). You may not use this file except in compliance with the License.
 *
 * You should have received a copy of the License in a separate text file originally named
 * "LICENSE_BWH.md"; if not, refer to "BSD-3-Clause" license at https://spdx.org/licenses/.
 */

#ifndef EFM_LR_MODEL_H
#define EFM_LR_MODEL_H

#define LR_MODEL_EXPOSURE_VARS          1
#if (LR_MODEL_EXPOSURE_VARS != 1)
#error "Unsupported number of exposure variables"
#endif

#include "common.h"
#include "genetic.h"
#include "term.h"

#ifdef USE_INTEL_MKL
#include <mkl.h>
#else
#include <cblas.h>
#include <lapacke.h>
#endif

typedef struct {
   // Genotype terms
   char **snp_name;
   struct pio_locus_t** snp_locus;
   void** snp_data;
   size_t snp_ct;
   size_t snp_sz;

   // Non-genotype terms
   char **cov_name;
   float **cov_data;
   term_spec_t **term_spec;
   size_t cov_ct;
   size_t cov_sz;
} lr_terms_t;

/*
 * Logistic Regression model
 *
 * Number of model parameters k = a + q + o + p + r + s, where:
 * a = 0 or 1   - intercept status (0 for absent or 1 for present),
 * q = 1        - number of exposure variables (in this version, q is fixed to LR_MODEL_EXPOSURE_VARS)
 * o = 0..      - number of genotype covariates
 * p = 0..      - number of non-genotype covariates
 * r = 0..o     - number of genotype interacting terms (exposure vars x genotype covars)
 * s = 0..p     - number of non-genotype interacting terms (exposure vars x non-genotype covars)
 *
 */
typedef struct {
   // Model coefficients
   alg_float_ptr coef;

   // Standard Error for model coefficients
   alg_float_ptr se;

   // Number of model parameters (coefficients)
   size_t k;

   // Sample size used to fit the model to data
   size_t n;

   // Sizes of parameter vector (must sum up to k)
   size_t a;
   size_t q;
   size_t o;
   size_t p;
   size_t r;
   size_t s;

   // Log-likelihood for fitted model
   double loglik;

   // Mode of inheritance
   inheritance_mode_t inheritance;

} lr_model_t;

// Initialize LR_MODEL structure
STATUS init_lr_model(lr_model_t **model, size_t a, size_t q, lr_terms_t *covariates, lr_terms_t *interactions, size_t n,
      inheritance_mode_t inheritance);

// Free up LR_MODEL structure
STATUS free_lr_model(lr_model_t **model);

// Fit model to data
STATUS fit_lr_model(lr_model_t* models, alg_float_ptr xx, alg_float_ptr yy, alg_float_ptr buf1, alg_float_ptr buf2, alg_float_ptr buf3,
      alg_float_ptr buf4);

#endif /* EFM_LR_MODEL_H */
