/*
 * Copyright (C) 2016-2017 The Brigham and Women�s Hospital, Inc.
 * Portions contributed and copyright held by others as indicated.
 * All rights reserved.
 *
 * The contents of this file are subject to the terms of the BSD 3-clause "New" or "Revised"
 * License (the "License"). You may not use this file except in compliance with the License.
 *
 * You should have received a copy of the License in a separate text file originally named
 * "LICENSE_BWH.md"; if not, refer to "BSD-3-Clause" license at https://spdx.org/licenses/.
 */

#include <string.h>
#include <math.h>
#include <stdio.h>
#include <limits.h>
#include "lr_model.h"
#include "mem_alloc.h"

/**
 * Free up logistic regression model structure
 */
STATUS free_lr_model(lr_model_t **model) {
   if (model && *model) {
      if (((*model)->coef))
         my_mm_free((*model)->coef);
      if (((*model)->se))
         my_mm_free((*model)->se);
      my_mm_free(*model);
      *model = NULL;
   } else {
      return STATUS_PARAM_ERROR;
   }
   return STATUS_OK;
}

/**
 * Allocate and initialize logistic regression model structure
 */
STATUS init_lr_model(lr_model_t **model, size_t a, size_t q, lr_terms_t *covariates, lr_terms_t *interactions, size_t n,
      inheritance_mode_t inheritance) {

   if (!model || *model) {
      return STATUS_PARAM_ERROR;
   }

   if (a > 1 || q != LR_MODEL_EXPOSURE_VARS || n < 1) {
      return STATUS_PARAM_ERROR;
   }

   *model = (lr_model_t*) my_mm_malloc(sizeof(lr_model_t));
   if (*model) {
      memset(*model, 0, sizeof(lr_model_t));
   } else {
      return STATUS_MALLOC_ERROR;
   }

   // temporary pointer to the model (for convenience)
   lr_model_t *m = *model;

   // Model specification
   m->a = a;
   m->q = q;
   m->o = covariates->snp_ct;
   m->p = covariates->cov_ct;
   m->r = interactions->snp_ct;
   m->s = interactions->cov_ct;
   m->k = m->a + m->q + m->o + m->p + m->r + m->s;
   m->n = n;

//    Debug("-----------------------");
//    Debug("Model specification:");
//    Debug("\ta=%zd (intercept)", m->a);
//    Debug("\tq=%zd (exposure)", m->q);
//    Debug("\to=%zd (genotype)", m->o);
//    Debug("\tp=%zd (non-genotype)", m->p);
//    Debug("\tr=%zd (genotype interacting terms)", m->r);
//    Debug("\ts=%zd (non-genotype interacting terms)", m->s);
//    Debug("\tk=%zd (number of params)", m->k);
//    Debug("\tn=%zd (number of samples)", m->n);
//    Debug("-----------------------");

// Model coefficients
   m->coef = (alg_float_ptr) my_mm_malloc(sizeof(float) * m->k);
   if (m->coef) {
      memset(m->coef, 0, sizeof(float) * m->k);
   } else {
      free_lr_model(model);
      return STATUS_MALLOC_ERROR;
   }

   // Standard Error for model coefficients
   m->se = (alg_float_ptr) my_mm_malloc(sizeof(float) * m->k);
   if (m->se) {
      memset(m->se, 0, sizeof(float) * m->k);
   } else {
      free_lr_model(model);
      return STATUS_MALLOC_ERROR;
   }

   m->loglik = 0.0;
   m->inheritance = inheritance;

   return STATUS_OK;
}

/**
 * Fit logistic regression model to data
 *
 * Note: This implementation is based on logistic_regression algorithm from PLINK
 * For more details see https://www.cog-genomics.org/plink2/dev
 *
 * Inputs:
 *  model - logistic regression model structure with pre-initialized parameter vector
 *  sample_ct - size of the xx vector, which is the number of samples
 * 	xx - predictor data matrix; size: param_ct by sample_ct; PADDING-bytes padded with zeros
 * 	yy - response data vector; size: sample_ct
 *
 * Input/output:
 * 	model->coef - initial coefficient matrix, overwritten with logistic regression result (must be aligned)
 *
 * Output:
 * 	pp - final likelihoods minus Y[]
 * 	model->se - diagonal vector of S matrix; size: param_ct (last estimate for coeaf is temporarily stored in model->coef)
 *
 * 	[Optional] pre-allocated float buffers. NULL must be passed for each unused parameter.
 * 	Each buffer must be allocated with respect to the ALIGNMENT macro using special variant of malloc function.
 * 	Each buffer must also be padded (see padded_float_buffer_size()) and big enough to keep maximum _possible_ number of floats:
 * 	buf1 - size: sample_cta
 * 	buf2 - size: param_cta
 * 	buf3 - size: param_ct by param_cta
 * 	buf4 - size: param_ct by sample_cta
 *
 * 	where "ct" stands for count and represents dimension, while "cta" stands for PADDING-bytes padded ct (leading dimension in matrix)
 *
 * Return value: STATUS_OK on success or error code otherwise
 *
 */
STATUS fit_lr_model(lr_model_t* model, alg_float_ptr xx, alg_float_ptr yy, alg_float_ptr buf1, alg_float_ptr buf2, alg_float_ptr buf3,
      alg_float_ptr buf4) {

   // Bufer sizes (CBLAS uses int for size parameters)
   int param_ct = (int) model->k;
   int sample_ct = (int) model->n;

   // Aligned buffer sizes (CBLAS uses int for size parameters)
   int param_cta = (int) padded_float_buffer_size(param_ct);
#ifndef USE_COLUMN_MAJOR_MATRIX
   int sample_cta = (int) padded_float_buffer_size(sample_ct);
#endif

   uint_fast32_t iteration = 0;
   int info = 0;
   alg_float min_delta_coef = __FLT_MAX__;
   alg_float delta_coef;

   uint_fast32_t i;
   bool revert_coef_and_last_fit = false;
   STATUS retval = STATUS_OK;

   // Variance vector (length sample_ct, aligned)
   alg_float_ptr pp = buf1 ? buf1 : (alg_float_ptr) my_mm_malloc(sizeof(float) * sample_ct);

   // Gradient vector / coefficient change buffer (length param_ct, aligned)
   alg_float_ptr dcoef = buf2 ? buf2 : (alg_float_ptr) my_mm_malloc(sizeof(float) * param_ct);

   // Linear system matrix (param_ct x param_ct, rows aligned)
   alg_float_ptr ll = buf3 ? buf3 : (alg_float_ptr) my_mm_malloc(sizeof(float) * param_ct * param_cta);

   // A temporary matrix (param_ct x sample_ct, rows aligned)
   // This matrix is used to store the xx matrix element-by-element multiplied by the vv vector
#ifdef USE_COLUMN_MAJOR_MATRIX
   alg_float_ptr xxvv = buf4 ? buf4 : (alg_float_ptr) my_mm_malloc(sizeof(float) * param_cta * sample_ct);
#else
   alg_float_ptr xxvv = buf4 ? buf4 : (alg_float_ptr) my_mm_malloc(sizeof(float) * param_ct * sample_cta);
#endif

   if (!(pp && dcoef && ll && xxvv)) {
      if (!buf1 && pp)
         my_mm_free(pp);
      if (!buf2 && dcoef)
         my_mm_free(dcoef);
      if (!buf3 && ll)
         my_mm_free(ll);
      if (!buf4 && xxvv)
         my_mm_free(xxvv);
      return STATUS_MALLOC_ERROR;
   }

//    Debug("Matrix:");
//    for(int i=0; i<param_ct; i++){
//        for(int j=0; j<sample_ct; j++){
//            fprintf(stderr," %7.5f", xx[i + param_cta * j]);
//        }
//        fprintf(stderr,"\n");
//    }
//    fprintf(stderr,"\n");

   while (1) {
      iteration++;

      // reverting model->coeff to previous values, which were stored temporarily in model->se
      if (revert_coef_and_last_fit) {
         for (i = 0; i < param_ct; i++) {
            model->coef[i] = model->se[i];
         }
      }

      /*
       * Logistic response for samples from matrix XX' using current coefficients in vector COEF
       * Note: vector XXVV is used as a temporary variable here
       *
       * PP = 1 / ( 1 + exp( -(COEF * XX') ) )
       *
       */
      // pp[i] = - ( COEF . XX'[i] ); "." means dot product, here of vector COEF and row XX'[i]
#ifdef USE_COLUMN_MAJOR_MATRIX
      cblas_sgemv(CblasColMajor, CblasTrans, param_ct, sample_ct, -1.0f, xx, param_cta, model->coef, 1, 0.0f, pp, 1);
#else
      cblas_sgemv(CblasRowMajor, CblasTrans, param_ct, sample_ct, -1.0f, xx, sample_cta, model->coef, 1, 0.0f , pp, 1);
#endif

#ifdef USE_INTEL_MKL_VMF
      // xxvv[i] = exp(pp[i]); xxvv used as temporary variable
      vsExp(sample_ct, pp, xxvv);

      // pp[i] = 1.0
#ifdef __INTEL_COMPILER
      __assume_aligned(pp, ALIGNMENT);
#endif
      for (i = 0; i < sample_ct; i++) {
         pp[i] = 1.0f;
      }

      // xxvv [i] = pp[i] + xxvv[i]
      cblas_saxpby(sample_ct, 1.0f, pp, 1, 1.0f, xxvv, 1);

      // pp[i] = 1 / xxvv[i] (inversion)
      vsInv(sample_ct, xxvv, pp);
#else
#if defined(__INTEL_COMPILER)
      __assume_aligned(pp, ALIGNMENT);
#endif
      for (i = 0; i < sample_ct; i++) {
         pp[i] = 1.0f / (1.0f + expf(pp[i]));
      }
#endif

      /*
       * Hessian matrix (according to computations in PLINK)
       */
      // element-by-element multiplication of exposure/covariate values across all samples by variance across samples' response (calculated above); result stored in xxvv
      // Sample variance for responses (var = p*q for simple proportion)
      // xxvv[i] = pp[i] * (1 - pp[i]);
      // Initializing xxvv with xx
#ifdef USE_COLUMN_MAJOR_MATRIX
      memcpy(xxvv, xx, sizeof(float) * param_cta * sample_ct);
      for (i = 0; i < sample_ct; i++) {
         cblas_sscal(param_ct, pp[i] * (1 - pp[i]), &(xxvv[i * param_cta]), 1);
      }
#else
      memcpy(xxvv, xx, sizeof(float) * param_ct * sample_cta);
      for(i = 0; i < sample_ct; i++) {
         cblas_sscal(param_ct, pp[i] * (1-pp[i]), &(xxvv[i]), sample_cta);
      }
#endif

#ifdef USE_INTEL_MKL
      // matrix-matrix multiplication xx * xxvv'; only lower triangle is updated in the result matrix ll
#ifdef USE_COLUMN_MAJOR_MATRIX
      cblas_sgemmt(CblasColMajor, CblasLower, CblasNoTrans, CblasTrans, param_ct, sample_ct, 1.0f, xx, param_cta, xxvv, param_cta, 0.0f, ll,
            param_cta);
#else
      cblas_sgemmt(CblasRowMajor, CblasLower, CblasNoTrans, CblasTrans, param_ct, sample_ct, 1.0f, xx, sample_cta, xxvv, sample_cta, 0.0f, ll, param_cta);
#endif
#else
      // matrix-matrix mmultiplication xx * xxvv'; only lower triangle is updated in the result matrix ll
#ifdef USE_COLUMN_MAJOR_MATRIX
      cblas_sgemm(CblasColMajor, CblasNoTrans, CblasTrans, param_ct, param_ct, sample_ct, 1.0f, xx, param_cta, xxvv, param_cta, 0.0f, ll, param_cta);
#else
      cblas_sgemm(CblasRowMajor, CblasNoTrans, CblasTrans, param_ct, param_ct, sample_ct, 1.0f, xx, sample_cta, xxvv, sample_cta, 0.0f, ll, param_cta);
#endif
#endif

      /*
       * Cholesky decomposition
       */
#ifdef USE_COLUMN_MAJOR_MATRIX
      if ((info = LAPACKE_spotrf(LAPACK_COL_MAJOR, 'L', param_ct, ll, param_cta))) {
#else
         if((info = LAPACKE_spotrf(LAPACK_ROW_MAJOR, 'L', param_ct, ll, param_cta))) {
#endif
         if (info < 0 || revert_coef_and_last_fit) {
            retval = STATUS_MKL_LAPACK_ERROR;
            break;
         } else {
// Debug("LAPACKE_spotrf INFO %d at iteration %d", info, iteration);
            revert_coef_and_last_fit = true;
            continue;
         }
      }

      /* todo clarify
       * Gradient vector of log-likelihood (in logistic regression); this seems like negative gradient, though ???
       * GRAD = XX * (PP - YY)
       */
#ifdef USE_INTEL_MKL_VMF
      // PP = PP - YY;
      cblas_saxpby(sample_ct, -1.0f, yy, 1, 1.0f, pp, 1);
#else
#ifdef __INTEL_COMPILER
      __assume_aligned(pp,ALIGNMENT);
      __assume_aligned(yy,ALIGNMENT);
#endif
      for (i = 0; i < sample_ct; i++) {
         pp[i] -= yy[i];
      }
#endif
      // GRAD = XX * PP (stored in dcoef for the next step)
#ifdef USE_COLUMN_MAJOR_MATRIX
      cblas_sgemv(CblasColMajor, CblasNoTrans, param_ct, sample_ct, 1.0f, xx, param_cta, pp, 1, 0.0f, dcoef, 1);
#else
      cblas_sgemv(CblasRowMajor, CblasNoTrans, param_ct, sample_ct, 1.0f, xx, sample_cta, pp, 1, 0.0f , dcoef, 1);
#endif

      /*
       * Solve system of linear equations with Cholesky-factored coefficient matrix
       */
#ifdef USE_COLUMN_MAJOR_MATRIX
      if (LAPACKE_spotrs(LAPACK_COL_MAJOR, 'L', param_ct, 1, ll, param_cta, dcoef, param_cta)) {
#else
         if(LAPACKE_spotrs(LAPACK_ROW_MAJOR, 'L', param_ct, 1, ll, param_cta, dcoef, 1)) {
#endif
         retval = STATUS_MKL_LAPACK_ERROR;
         break;
      }

      /*
       * based on PLINK's source code
       * todo copyrights
       */

      delta_coef = 0.0f;

      for (i = 0; i < param_ct; i++) {
         delta_coef += fabsf(dcoef[i]);
      }

      if (delta_coef < min_delta_coef) {
         min_delta_coef = delta_coef;
      }

      if (delta_coef != delta_coef) {
         retval = STATUS_UNKNOWN_ERROR;
         break;
      }

      // delta_coef became too large; revert to previous estimates and fit model last time
      if ((iteration > 4) && (delta_coef > 20.0f) && (delta_coef > 10.0f * min_delta_coef)) {
         revert_coef_and_last_fit = true;
         continue;
      }

      /*
       * Updating model coefficients
       */
      for (i = 0; i < param_ct; i++) {
         // previous model->coeff values are stored in model->se
         model->se[i] = model->coef[i];
         model->coef[i] -= dcoef[i];
      }

      /*
       * Log-likelihood for the fitted model. Using pp as a temporary general-purpose vector
       * Unconditional log-likelihood of observing Y for covariates, given model parameters
       * pp[i] = ( COEF . XX'[i] ); "." means dot product, here of vector COEF and row XX'[i]
       */
#ifdef USE_COLUMN_MAJOR_MATRIX
      cblas_sgemv(CblasColMajor, CblasTrans, param_ct, sample_ct, 1.0f, xx, param_cta, model->coef, 1, 0.0f, pp, 1);
#else
      cblas_sgemv(CblasRowMajor, CblasTrans, param_ct, sample_ct, 1.0f, xx, sample_cta, model->coef, 1, 0.0f , pp, 1);
#endif
      model->loglik = 0.0;

      for (i = 0; i < sample_ct; i++) {
         if (yy[i]) {
            model->loglik += pp[i];
         }
         model->loglik -= log(1.0 + exp(pp[i]));
      }

      if (revert_coef_and_last_fit) {
         break;
      }

// todo review what is the purpose of this
//    if ((iteration >= 8) && (fabsf(1.0f - delta_coef) < 1e-3f)){
//        retval = STATUS_UNKNOWN_ERROR;
//        break;
//    }

      // Original comment from PLINK: "Pons reported that 1.1e-3 was dangerous, so I agree with the decision to tighten this threshold from 1e-3 to 1e-4."
      if ((iteration >= 15) || (delta_coef < 1e-4f)) {
         break;
      }
   }

   // Computing S matrix (inverting Cholesky-factorized coefficient matrix)
#ifdef USE_COLUMN_MAJOR_MATRIX
   if (LAPACKE_spotri(LAPACK_COL_MAJOR, 'L', param_ct, ll, param_cta)) {
#else
      if(LAPACKE_spotri(LAPACK_ROW_MAJOR, 'L', param_ct, ll, param_cta)) {
#endif
      retval = STATUS_MKL_LAPACK_ERROR;
   }

   /*
    * validParameters() check - based on PLINK's source code
    * todo review & copyrights
    */
#ifdef __INTEL_COMPILER
   __assume_aligned(ll, ALIGNMENT);
#endif
   for (i = 1; i < param_ct; i++) {
      // diagonal element
      float f = ll[i * (param_cta + 1)];
      if (f == NAN || f == HUGE_VALF || f == -HUGE_VALF || f < 1e-20f) {
         retval = STATUS_UNKNOWN_ERROR;
         break;
      }

      /* todo re-define this test if needed
       f = 0.99999 * sqrtf(f);
       fptr = &(ll[i * param_cta]);
       for (j = 0; j < i; j++) {
       if (  (*fptr++) > f * sqrtf(ll[i*(param_cta)+j]) ) {
       retval = STATUS_UNKNOWN_ERROR;
       }
       } */
   }

   /*
    * Log-likelihood for the fitted model. Using pp as a temporary general-purpose vector
    * Unconditional log-likelihood of observing Y for covariates, given model parameters
    * pp[i] = ( COEF . XX'[i] ); "." means dot product, here of vector COEF and row XX'[i]
    */
#ifdef USE_COLUMN_MAJOR_MATRIX
   cblas_sgemv(CblasColMajor, CblasTrans, param_ct, sample_ct, 1.0f, xx, param_cta, model->coef, 1, 0.0f, pp, 1);
#else
   cblas_sgemv(CblasRowMajor, CblasTrans, param_ct, sample_ct, 1.0f, xx, sample_cta, model->coef, 1, 0.0f , pp, 1);
#endif
   model->loglik = 0.0;

   for (i = 0; i < sample_ct; i++) {
      if (yy[i]) {
         model->loglik += pp[i];
      }
      model->loglik -= log(1.0 + exp(pp[i]));
   }

   // Copying SE values (sqrt of the diagonal elements of S matrix)
   // NOTE: Conversion from float values to double values takes place here
   //cblas_scopy(param_ct, ll, param_cta+1, results, 1);
   for (i = 0; i < param_ct; i++) {
      model->se[i] = sqrtf(ll[i * (param_cta + 1)]);
   }

//#include "stat.h"
//      Debug("Deviance = %g after %d iterations", deviance(model->loglik), iteration);
//      Debug("Coef & SE");
//      for(int j=0; j<param_ct; j++){
//          Debug("\t%+15.10g\t\t%+15.10g", model->coef[j], model->se[j]);
//      }

   if (!buf1 && pp)
      my_mm_free(pp);
   if (!buf2 && dcoef)
      my_mm_free(dcoef);
   if (!buf3 && ll)
      my_mm_free(ll);
   if (!buf4 && xxvv)
      my_mm_free(xxvv);

   return retval;
}
