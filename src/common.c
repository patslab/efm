/*
 * Copyright (C) 2016-2017 The Brigham and Women’s Hospital, Inc.
 * Portions contributed and copyright held by others as indicated.
 * All rights reserved.
 *
 * The contents of this file are subject to the terms of the BSD 3-clause "New" or "Revised"
 * License (the "License"). You may not use this file except in compliance with the License.
 *
 * You should have received a copy of the License in a separate text file originally named
 * "LICENSE_BWH.md"; if not, refer to "BSD-3-Clause" license at https://spdx.org/licenses/.
 */

#include "common.h"

// Global variables
unsigned long int g_conf = 0;

char *g_plink_fileset_prefix = NULL;
char *g_beagle_fileset_prefix = NULL;
char *g_bgen_fileset_prefix = NULL;
char *g_vcf_fileset_prefix = NULL;
char *g_gzipped_vcf_fileset_prefix = NULL;
char *g_aux_input_file_name = NULL;
char *g_plink_fileset_extensions[] = { ".fam", ".bed", ".bim", NULL };
char *g_beagle_fileset_extensions[] = { ".fam", ".bgl", NULL };
char *g_bgen_fileset_extensions[] = { ".bgen", ".sample", NULL };
char *g_vcf_fileset_extensions[] = { ".vcf", ".fam", NULL };
char *g_gzipped_vcf_fileset_extensions[] = { ".vcf.gz", ".fam", NULL };
char *g_output_fileset_prefix = DEFAULT_OUT_FILESET_PREFIX;
char *g_covar_file_name = NULL;
char *g_interaction_spec = NULL;
char *g_new_covar_spec = NULL;
char *g_extra_snps_as_covars = NULL;
char *g_covar_list = NULL;
size_t g_thread_number = DEFAULT_NUMBER_OF_THREADS;
float g_aic_threshold;
unsigned int blocksize = 0;
