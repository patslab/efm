/*
 * Copyright (C) 2016-2017 The Brigham and Women’s Hospital, Inc.
 * Portions contributed and copyright held by others as indicated.
 * All rights reserved.
 *
 * The contents of this file are subject to the terms of the BSD 3-clause "New" or "Revised"
 * License (the "License"). You may not use this file except in compliance with the License.
 *
 * You should have received a copy of the License in a separate text file originally named
 * "LICENSE_BWH.md"; if not, refer to "BSD-3-Clause" license at https://spdx.org/licenses/.
 */

#ifndef ALPHA__VCF_GZIPPED_H
#define ALPHA__VCF_GZIPPED_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>		// For math routines (such as sqrt & trig).
#include <zlib.h>
#include <errno.h>

#define PLINK_TRANSPOSED_BED_EXTENSION ".t"
#define DEFAULT_OUT_FILESET_PREFIX      EXECUTABLE_NAME

// Global configuration shared across the project
int vcf_gz_getsnps(char const* fileName);
int vcf_gz_getsamples(char const* fileName);
char* vcf_gz_getallele1(char const* fileName, int index);
char* vcf_gz_getallele2(char const* fileName, int index);
char* vcf_gz_getid(char const* fileName, int index);
char* vcf_gz_getpos(char const* fileName, int index);
char* vcf_gz_getchromosome(char const* fileName, int index);
int vcf_gz_getgenotypiccount(char const* fileName, int index, int sample);
double vcf_gz_getdosage(char const* fileName, int index, int sample);
double *vcf_gz_getgenotypicprobabilites(char const* fileName, int index, int sample);

// Enabling assert() for debug builds only
#ifdef DEBUG
#undef NDEBUG
#include <assert.h>
#else
#define NDEBUG
#include <assert.h>
#endif

#ifdef DEBUG
// IN MAGENTA
#define Debug(format, ...)		if(g_conf & CONF_DEBUG) fprintf(stderr, "\033[35mDEBUG: " format " (%s:%d)\033[0m\n",  ##__VA_ARGS__, basename(__FILE__), __LINE__)
#else
#define Debug(format, ...)
#endif
// IN YELLOW
#define Warning(format, ...)    fprintf(stderr, "\033[33mWARNING: " format "\033[0m\n",  ##__VA_ARGS__)
// IN RED
#define Error(format, ...)		fprintf(stderr, "\033[31mERROR: " format "\033[0m\n",  ##__VA_ARGS__)
// IN NAVY
//#define Verbose(format, ...)	fprintf(stderr, "\033[34mVERBOSE: " format "\033[0m\n",  ##__VA_ARGS__)
#define Info(format, ...)		fprintf(stdout, format "\n", ##__VA_ARGS__)

#ifdef USE_OPEN_MP
#include <omp.h>
#define ClearTimeElapsed(config, time_stamp)		if(config & CONF_VERBOSE) do { *time_stamp=omp_get_wtime(); } while(0)
#define ShowTimeElapsed(config, time_stamp, msg)	if(config & CONF_VERBOSE) do { Verbose("%s in %.2f sec.", msg, omp_get_wtime() - *time_stamp); *time_stamp=omp_get_wtime(); } while(0)
#else
#define ClearTimeElapsed(config, time_stamp)
#define ShowTimeElapsed(config, time_stamp, msg)
#endif

#endif /* ALPHA__VCF_GZIPPED_H */
