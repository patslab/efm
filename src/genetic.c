/*
 * Copyright (C) 2016-2017 The Brigham and Women’s Hospital, Inc.
 * Portions contributed and copyright held by others as indicated.
 * All rights reserved.
 *
 * The contents of this file are subject to the terms of the BSD 3-clause "New" or "Revised"
 * License (the "License"). You may not use this file except in compliance with the License.
 *
 * You should have received a copy of the License in a separate text file originally named
 * "LICENSE_BWH.md"; if not, refer to "BSD-3-Clause" license at https://spdx.org/licenses/.
 */

#include "genetic.h"

// these extern "definitions" are required for proper handling of inline functions in C99
extern float genotype_to_float(genotype_t genotype, inheritance_mode_t inheritance);
extern float genotype_bgl_to_float(float* genotype, size_t sample_idx, inheritance_mode_t inheritance, bgl_format_t format);
extern bool is_str_dna_base(char *c);
