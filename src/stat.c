/*
 * Copyright (C) 2016-2017 The Brigham and Women’s Hospital, Inc.
 * Portions contributed and copyright held by others as indicated.
 * All rights reserved.
 *
 * The contents of this file are subject to the terms of the BSD 3-clause "New" or "Revised"
 * License (the "License"). You may not use this file except in compliance with the License.
 *
 * You should have received a copy of the License in a separate text file originally named
 * "LICENSE_BWH.md"; if not, refer to "BSD-3-Clause" license at https://spdx.org/licenses/.
 */

#include "stat.h"

// these extern "definitions" are required for proper handling of inline functions in C99
extern inline double deviance(double loglik);
extern inline double aicc(double loglik, size_t k, size_t n);
extern inline double bic(double loglik, size_t k, size_t n);
extern long double lr_z_val(double regress_coef, double std_error);
extern long double p_val(long double z_value);
extern long double lr_adj_or(double regress_coef);
