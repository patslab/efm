/*
 * Copyright (C) 2016-2017 The Brigham and Women’s Hospital, Inc.
 * Portions contributed and copyright held by others as indicated.
 * All rights reserved.
 *
 * The contents of this file are subject to the terms of the BSD 3-clause "New" or "Revised"
 * License (the "License"). You may not use this file except in compliance with the License.
 *
 * You should have received a copy of the License in a separate text file originally named
 * "LICENSE_BWH.md"; if not, refer to "BSD-3-Clause" license at https://spdx.org/licenses/.
 */

#include <plinkio/plinkio.h>
#include <math.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/mman.h>

#include "lr_process.h"
#include "mem_alloc.h"
#include "stat.h"
#include "genetic.h"
#include "io_report.h"

#include "vcf_unzipped.h"
#include "vcf_gzipped.h"
#include "bgen_functions.h"

#ifdef USE_OPEN_MP
#include <omp.h>
#endif

#define MULTIPLIER (37)

//BGEN_VCF:9
struct bgenpheno {
   char *fid, *iid;
   int phenotype, sex;
};

int numberofcases = 0;
int numberofcontrols = 0;

//BGEN_VCF:13
int compare(const void * a, const void * b) {
   return (*(int*) a - *(int*) b);
}

double *afarray;
double *afarraycases;
double *afarraycontrols;

/*
 * Simple Hashing Function to Optimize Mapping of Covariates to Samples
 */
unsigned int hash(const char *s, int max) {
   unsigned int h;
   unsigned const char *ucc;

   ucc = (unsigned const char *) s;

   h = 0;
   while (*ucc != '\0') {
      h = h * MULTIPLIER + *ucc;
      ucc++;
   }

   return h % max;
}

/**
 * Report logistic regression results to pre-allocated memory buffer.
 */

static inline void write_lr_report(char* dest_buffer, const lr_process_result_t* result, const lr_terms_t *covariates, const lr_terms_t *interactions) {

   int bytes_written;

   // Returning with no action in case of parameter issue
   if (!dest_buffer || !result) {
      return;
   }

   // Convenient pointer to the model
   lr_model_t *model = result->model;

   // Printing an empty line in case results are incorrect
   if (!model || !interactions || !covariates) {
      for (size_t j = model->a; j < model->k + LR_REPORT_SUMMARY_LINES; j++) {
         bytes_written = sprintf(dest_buffer, LR_REPORT_EMPTY_LINE_FMT,
         LR_REPORT_EMPTY_LINE_STR);
         dest_buffer[bytes_written++] = LR_REPORT_NEW_LINE_CHR;
         assert(bytes_written == LR_REPORT_LINE_LENGTH);
         dest_buffer += bytes_written;
      }
      return;
   }

   // Summary (line #1)
   bytes_written = sprintf(dest_buffer,
   LR_REPORT_CHR_NO_FMT LR_REPORT_SNP_ID_FMT LR_REPORT_POS_BP_FMT LR_REPORT_AL1_NM_FMT LR_REPORT_COV_NM_FMT LR_REPORT_SML_CT_FMT, result->chromosome,
         result->snp_name, result->bp_position, result->allele1,
         LR_REPORT_SUMMARY_LINE_TEXT, model->n);

   if (result->status == STATUS_OK) {
      bytes_written += sprintf(dest_buffer + bytes_written,
      LR_REPORT_DEV_FMT LR_REPORT_CAIC_FMT LR_REPORT_BIC_FMT,
      // Deviance
            deviance(model->loglik),
            // AICc
            aicc(model->loglik, model->k, model->n),
            // BIC
            bic(model->loglik, model->k, model->n));
   } else {
      bytes_written += sprintf(dest_buffer + bytes_written,
      LR_REPORT_NA_STR LR_REPORT_NA_STR LR_REPORT_NA_STR);
   };
   dest_buffer[bytes_written++] = LR_REPORT_NEW_LINE_CHR;
   assert(bytes_written == LR_REPORT_LINE_LENGTH);
   dest_buffer += bytes_written;

   // Results (one line per covariate or interaction)
   for (size_t j = model->a; j < model->k; j++) {

      char* cov_name = NULL;
      char* cov_name2 = NULL;
      long double z_val = lr_z_val(model->coef[j], model->se[j]);

      if (j < model->a + model->q) {
         cov_name = (model->inheritance == INH_MODE_DOMINANT) ?
         LR_REPORT_DOM_STR :
                                                                ((model->inheritance == INH_MODE_RECESSIVE) ?
                                                                LR_REPORT_REC_STR :
                                                                                                              LR_REPORT_ADD_STR);
      } else if (j < model->a + model->q + model->o) {
         cov_name = covariates->snp_locus[j - model->a - model->q]->name;
      } else if (j < model->a + model->q + model->o + model->p) {
         cov_name = covariates->cov_name[j - model->a - model->q - model->o];
      } else if (j < model->a + model->q + model->o + model->p + model->r) {
         //todo prevent memory leak
         cov_name2 = (char*) malloc(sizeof(char) * (strlen(interactions->snp_locus[j - model->a - model->q - model->o - model->p]->name) + strlen(
         LR_TERM_SPEC_OP_MULT_ADD_MARG_SYMB) + 1));
         sprintf(cov_name2, "%s%s", LR_TERM_SPEC_OP_MULT_ADD_MARG_SYMB, interactions->snp_locus[j - model->a - model->q - model->o - model->p]->name);
         // ... free(cov_name) at some point
      } else if (j < model->a + model->q + model->o + model->p + model->r + model->s) {
         //todo prevent memory leak
         cov_name2 = (char*) malloc(
               sizeof(char)
                     * (strlen(interactions->cov_name[j - model->a - model->q - model->o - model->p - model->r])
                           + strlen(LR_TERM_SPEC_OP_MULT_ADD_MARG_SYMB) + 1));
         sprintf(cov_name2, "%s%s", LR_TERM_SPEC_OP_MULT_ADD_MARG_SYMB,
               interactions->cov_name[j - model->a - model->q - model->o - model->p - model->r]);
         // ... free(cov_name) at some point
      }

      bytes_written = sprintf(dest_buffer,
      LR_REPORT_CHR_NO_FMT LR_REPORT_SNP_ID_FMT LR_REPORT_POS_BP_FMT LR_REPORT_AL1_NM_FMT LR_REPORT_COV_NM_FMT LR_REPORT_SML_CT_FMT,
            result->chromosome, result->snp_name, result->bp_position, result->allele1,
            //covariate name: could be a covariate from a file, extra SNP covariate or covariate added due to interaction term
            cov_name2 ? cov_name2 : cov_name, model->n);

      if (result->status == STATUS_OK) {
         bytes_written += sprintf(dest_buffer + bytes_written,
         LR_REPORT_OR_FMT LR_REPORT_ZV_FMT LR_REPORT_PV_FMT, lr_adj_or(model->coef[j]), z_val, p_val(z_val));
      } else {
         bytes_written += sprintf(dest_buffer + bytes_written,
         LR_REPORT_NA_STR LR_REPORT_NA_STR LR_REPORT_NA_STR);
      };
      dest_buffer[bytes_written++] = LR_REPORT_NEW_LINE_CHR;
      assert(bytes_written == LR_REPORT_LINE_LENGTH);
      dest_buffer += bytes_written;
      if (cov_name2) {
         free(cov_name2);
         cov_name2 = NULL;
      }
   }
}

/**
 * Comparator for the logistic regression structures based on the AICc value
 */
static int compare_lr_results_aicc(const void *a, const void *b) {

   const lr_process_result_t *aa = *((const lr_process_result_t **) a);
   const lr_process_result_t *bb = *((const lr_process_result_t **) b);
   double a_aicc = aicc(aa->model->loglik, aa->model->k, aa->model->n);
   double b_aicc = aicc(bb->model->loglik, bb->model->k, bb->model->n);
   if (fabs(a_aicc - b_aicc) <= __DBL_EPSILON__) {
      return 0;
   };
   return (isless(a_aicc, b_aicc) ? -1 : 1);
}

/**
 * Logistic regression for a block of SNPs
 */
int logistic_regression_block(size_t block_size, struct pio_locus_t** snp_locus_block, float** snp_buffer_block, struct pio_file_t* pio_file,
      alg_float_ptr snp_phenotype, alg_int_ptr sex_array, size_t sample_ct, char* outfile_ptr, lr_terms_t *interactions, lr_terms_t *covariates,
      inheritance_mode_t inheritance, lr_process_result_t** block_results, unsigned long int config_flags) {

   if (!block_size || !sample_ct || !snp_locus_block || !snp_buffer_block || !snp_phenotype || !sex_array || !interactions || !covariates) {
      return STATUS_PARAM_ERROR;
   };

#ifdef USE_PREALLOCATED_BUFFERS
   /*
    * Pre-allocating buffers for computations in the model fitting function.
    * Buffers must be big enough to hold maximum possible amount of data.
    */

   size_t max_param_ct = LR_INTERCEPT_NUM_PARAMS + LR_EXPOSURE_NUM_PARAMS + covariates->snp_ct + covariates->cov_ct;
   max_param_ct += LR_EXPOSURE_NUM_PARAMS * (interactions->snp_ct + interactions->cov_ct);
   size_t max_param_cta = padded_float_buffer_size(max_param_ct);
   size_t max_sample_ct = sample_ct;
#ifndef USE_COLUMN_MAJOR_MATRIX
   size_t max_sample_cta = padded_float_buffer_size(max_sample_ct);
#endif

   // Exposure variable
#ifdef USE_COLUMN_MAJOR_MATRIX
   alg_float_ptr E = (alg_float_ptr) my_mm_malloc(sizeof(float) * max_param_cta * max_sample_ct);
#else
   alg_float_ptr E = (alg_float_ptr) my_mm_malloc(sizeof(float) * max_param_ct * max_sample_cta);
#endif

   // Response variable
   alg_float_ptr Y = (alg_float_ptr) my_mm_malloc(sizeof(float) * max_sample_ct);

   // Buffer for variance vector (length sample_ct, aligned)
   alg_float_ptr buf1 = (alg_float_ptr) my_mm_malloc(sizeof(float) * max_sample_ct);

   // Buffer for gradient vector (length param_ct, aligned)
   alg_float_ptr buf2 = (alg_float_ptr) my_mm_malloc(sizeof(float) * max_param_ct);

   // Buffer for linear system matrix (param_ct x param_ct)
   alg_float_ptr buf3 = (alg_float_ptr) my_mm_malloc(sizeof(float) * max_param_ct * max_param_cta);

   // Buffer for a temporary matrix (param_ct x sample_ct)
#ifdef USE_COLUMN_MAJOR_MATRIX
   alg_float_ptr buf4 = (alg_float_ptr) my_mm_malloc(sizeof(float) * max_param_cta * max_sample_ct);
#else
   alg_float_ptr buf4 = (alg_float_ptr) my_mm_malloc(sizeof(float) * max_param_ct * max_sample_cta);
#endif

   if (buf1 == NULL || buf2 == NULL || buf3 == NULL || buf4 == NULL || E == NULL || Y == NULL) {
      if (buf1)
         my_mm_free(buf1);
      if (buf2)
         my_mm_free(buf2);
      if (buf3)
         my_mm_free(buf3);
      if (buf4)
         my_mm_free(buf4);
      if (E)
         my_mm_free(E);
      if (Y)
         my_mm_free(Y);
      return STATUS_MALLOC_ERROR;
   }
#endif

   for (uint_fast32_t snp_id = 0; snp_id < block_size; snp_id++) {

      // Model pointer (to be assigned below)
      lr_model_t *model = NULL;

      // Valid sample count (samples with valid genotype data; to be determined below)
      size_t model_sample_ct;

      // SNP buffer
      void *snp_data_buf = snp_buffer_block[snp_id];

      // Starting with total sample count and adjusting sample_ct to exclude all samples with invalid or missing genotype data
      model_sample_ct = sample_ct;
      if (pio_file->format == PIO_FORMAT_PLINK_BINARY) {
         for (int sample_id = 0; sample_id < sample_ct; sample_id++) {

            float val;

            // Marking the current sample as "missing genotype data" if data for any covariate is missing

            // Validating sample for the SNP covariates
            for (int j = 0; j < covariates->snp_ct; j++) {
               if (((snp_t**) covariates->snp_data)[j][sample_id] == GT_MISSING) {
                  ((snp_t*) snp_data_buf)[sample_id] = GT_MISSING;
                  break;
               }
            }

            // Validating sample for the non-genotype covariates
            for (int j = 0; j < covariates->cov_ct; j++) {
               val = covariates->cov_data[j][sample_id];
               if (isnan(val) || val == HUGE_VALF || val == -HUGE_VALF) {
                  ((snp_t*) snp_data_buf)[sample_id] = GT_MISSING;
                  break;
               }
            }

            // Validating sample for the interacting SNPs
            for (int j = 0; j < interactions->snp_ct; j++) {
               if (((snp_t**) interactions->snp_data)[j][sample_id] == GT_MISSING) {
                  ((snp_t*) snp_data_buf)[sample_id] = GT_MISSING;
                  break;
               }
            }

            // Validating sample for the interacting non-genotype covariates
            for (int j = 0; j < interactions->cov_ct; j++) {
               val = interactions->cov_data[j][sample_id];
               if (isnan(val) || val == HUGE_VALF || val == -HUGE_VALF) {
                  ((snp_t*) snp_data_buf)[sample_id] = GT_MISSING;
                  break;
               }
            }

         }
      }

      // Parameter count
      size_t param_ct = max_param_ct;
      size_t param_cta = padded_float_buffer_size(param_ct);
#ifndef USE_COLUMN_MAJOR_MATRIX
      size_t model_sample_cta = padded_float_buffer_size(model_sample_ct);
#endif

#ifndef USE_PREALLOCATED_BUFFERS

      // Exposure variable
#ifdef USE_COLUMN_MAJOR_MATRIX
      alg_float_ptr E = (alg_float_ptr) my_mm_malloc(sizeof(float) * param_cta * sample_ct);
#else
      alg_float_ptr E = (alg_float_ptr) my_mm_malloc(sizeof(float) * param_ct * sample_cta);
#endif
      // Response variable
      alg_float_ptr Y = (alg_float_ptr) my_mm_malloc(sizeof(float) * sample_ct);

      // Note: double type used for results! Initial values are irrelevant
      alg_double_ptr S = (alg_double_ptr) my_mm_malloc(sizeof(double) * param_ct);

      struct lr_result *results = (struct lr_result*) my_mm_malloc(sizeof(struct lr_result) * param_ct);

      if(E == NULL || Y == NULL || S == NULL || results == NULL) {
         if (E) my_mm_free(E);
         if (Y) my_mm_free(Y);
         if (S) my_mm_free(S);
         if (results) my_mm_free(results);
         return STATUS_MALLOC_ERROR;
      }
#endif

#ifdef USE_COLUMN_MAJOR_MATRIX
      memset(E, 0, sizeof(float) * param_cta * model_sample_ct);
#else
      memset(E, 0, sizeof(float) * param_ct * model_sample_cta);
#endif
      memset(Y, 0, sizeof(float) * model_sample_ct);

      size_t samples_skipped = 0;
      size_t param_idx;

      // for each sample with non-missing genotype
      for (int sample_idx = 0; sample_idx < model_sample_ct; sample_idx++) {

//			// Skipping samples with missing genotype
//            if(snp_data_format == PIO_FORMAT_PLINK_BINARY){
//                while (((snp_t*)snp_data_buf)[ sample_idx + samples_skipped ] == GT_MISSING){
//                    samples_skipped++;
//                }
//            }

// Sample index in the original data buffers
         size_t i = sample_idx + samples_skipped;

         if (((snp_t*) snp_data_buf)[i] == GT_MISSING) {
            continue;
         }

         bool double_x_males = ((config_flags & CONF_DBL_X_MALE) && (sex_array[i] == PIO_MALE)) ? true : false;

#ifdef USE_COLUMN_MAJOR_MATRIX
         param_idx = sample_idx * param_cta;
#else
         param_idx = sample_idx;
#endif

         // 0-th covariate (beta0, a.k.a. alpha, has E parameter set fixed to 1.0f)
         // Only LR_INTERCEPT_NUM_PARAMS of 1 is supported
         E[param_idx] = 1.0f;

         // 1..LR_EXPOSURE_NUM_PARAMS exposure variables (values according to the inheritance model selected)
         // Only LR_EXPOSURE_NUM_PARAMS of 1 is supported
#ifdef USE_COLUMN_MAJOR_MATRIX
         param_idx++;
#else
         param_idx += model_sample_cta;
#endif
         switch (pio_file->format) {
         case PIO_FORMAT_PLINK_BINARY:
            E[param_idx] = genotype_to_float(((snp_t*) snp_data_buf)[i], inheritance);
            break;
         case PIO_FORMAT_BEAGLE_LIKE:
            switch (pio_file->bgl_file.format) {
            case BGL2_VALUES_PER_SAMPLE:
            case BGL3_VALUES_PER_SAMPLE:
               E[param_idx] = genotype_bgl_to_float((float*) snp_data_buf, i, inheritance, pio_file->bgl_file.format);
               break;
            default:
               break;
            }
            break;
         default:
            break;
         }

         if (double_x_males && (snp_locus_block[snp_id]->chromosome == 23)) {
            E[param_idx] *= 2;
         }

         // SNP covariates, if any
         for (int j = 0; j < covariates->snp_ct; j++) {
#ifdef USE_COLUMN_MAJOR_MATRIX
            param_idx++;
#else
            param_idx += model_sample_cta;
#endif
            switch (pio_file->format) {
            case PIO_FORMAT_PLINK_BINARY:
               E[param_idx] = genotype_to_float(((snp_t**) covariates->snp_data)[j][i], inheritance);
               break;
            case PIO_FORMAT_BEAGLE_LIKE:
               switch (pio_file->bgl_file.format) {
               case BGL2_VALUES_PER_SAMPLE:
               case BGL3_VALUES_PER_SAMPLE:
                  E[param_idx] = genotype_bgl_to_float((float*) covariates->snp_data[j], i, inheritance, pio_file->bgl_file.format);
                  break;
               default:
                  break;
               }
               break;
            default:
               break;
            }

            if (double_x_males && (covariates->snp_locus[j]->chromosome == 23)) {
               E[param_idx] *= 2;
            }
         }

         // Non-SNP covariates (read from the cov-file), if any
         for (int j = 0; j < covariates->cov_ct; j++) {
#ifdef USE_COLUMN_MAJOR_MATRIX
            param_idx++;
#else
            param_idx += model_sample_cta;
#endif
            E[param_idx] = covariates->cov_data[j][i];
         }

         // Interacting SNP covariate terms
         for (int j = 0; j < interactions->snp_ct; j++) {
#ifdef USE_COLUMN_MAJOR_MATRIX
            param_idx++;
#else
            param_idx += model_sample_cta;
#endif
            switch (pio_file->format) {
            case PIO_FORMAT_PLINK_BINARY:
               E[param_idx] = genotype_to_float(((snp_t**) interactions->snp_data)[j][i], inheritance);
               break;
            case PIO_FORMAT_BEAGLE_LIKE:
               switch (pio_file->bgl_file.format) {
               case BGL2_VALUES_PER_SAMPLE:
               case BGL3_VALUES_PER_SAMPLE:
                  E[param_idx] = genotype_bgl_to_float((float*) interactions->snp_data[j], i, inheritance, pio_file->bgl_file.format);
                  break;
               default:
                  break;
               }
               break;
            default:
               break;
            }

            if (double_x_males && (interactions->snp_locus[j]->chromosome == 23)) {
               E[param_idx] *= 2;
            }

            switch (pio_file->format) {
            case PIO_FORMAT_PLINK_BINARY:
               E[param_idx] *= genotype_to_float(((snp_t*) snp_data_buf)[i], inheritance);
               break;
            case PIO_FORMAT_BEAGLE_LIKE:
               switch (pio_file->bgl_file.format) {
               case BGL2_VALUES_PER_SAMPLE:
               case BGL3_VALUES_PER_SAMPLE:
                  E[param_idx] *= genotype_bgl_to_float((float*) snp_data_buf, i, inheritance, pio_file->bgl_file.format);
                  break;
               default:
                  break;
               }
               break;
            default:
               break;
            }

            if (double_x_males && (snp_locus_block[snp_id]->chromosome == 23)) {
               E[param_idx] *= 2;
            }
         }

         // Interacting non-SNP covariate terms
         for (int j = 0; j < interactions->cov_ct; j++) {
#ifdef USE_COLUMN_MAJOR_MATRIX
            param_idx++;
#else
            param_idx += model_sample_cta;
#endif
            E[param_idx] = interactions->cov_data[j][i];
            switch (pio_file->format) {
            case PIO_FORMAT_PLINK_BINARY:
               E[param_idx] *= genotype_to_float(((snp_t*) snp_data_buf)[i], inheritance);
               break;
            case PIO_FORMAT_BEAGLE_LIKE:
               switch (pio_file->bgl_file.format) {
               case BGL2_VALUES_PER_SAMPLE:
               case BGL3_VALUES_PER_SAMPLE:
                  E[param_idx] *= genotype_bgl_to_float((float*) snp_data_buf, i, inheritance, pio_file->bgl_file.format);
                  break;
               default:
                  break;
               }
               break;
            default:
               break;
            }

            if (double_x_males && (snp_locus_block[snp_id]->chromosome == 23)) {
               E[param_idx] *= 2;
            }
         }

         // Sanity check
#ifdef USE_COLUMN_MAJOR_MATRIX
         if (param_idx + 1 - sample_idx * param_cta != param_ct) {
#else
            if(param_idx + model_sample_cta - sample_idx * param_cta != param_ct) {
#endif
            Error("Number of parameters mismatch during exposure data initialization");
            return STATUS_PARAM_ERROR;
         }

         // Affection status
         Y[sample_idx] = snp_phenotype[i];
      }

      // Allocate results structure
      block_results[snp_id] = (lr_process_result_t*) my_mm_malloc(sizeof(lr_process_result_t));
      if (block_results[snp_id]) {
         memset(block_results[snp_id], 0, sizeof(lr_process_result_t));
      } else {
         Error("Could not init results structure");
         return STATUS_MALLOC_ERROR;
      }

      // Initialize model structure
      STATUS status = init_lr_model(&model, LR_INTERCEPT_NUM_PARAMS,
      LR_EXPOSURE_NUM_PARAMS, covariates, interactions, model_sample_ct, inheritance);
      if (status != STATUS_OK) {
         Error("Could not init model structure (status %d)", status);
         return status;
      }

      block_results[snp_id]->model = model;
      //todo cleaning of the duplicated string
      block_results[snp_id]->chromosome = snp_locus_block[snp_id]->chromosome;
      block_results[snp_id]->snp_name = strdup(snp_locus_block[snp_id]->name);
      block_results[snp_id]->bp_position = snp_locus_block[snp_id]->bp_position;
      block_results[snp_id]->allele1 = strdup(snp_locus_block[snp_id]->allele1);
      block_results[snp_id]->allele2 = strdup(snp_locus_block[snp_id]->allele2);

      // Fitting the model to data
#ifdef USE_PREALLOCATED_BUFFERS
      block_results[snp_id]->status = fit_lr_model(model, E, Y, buf1, buf2, buf3, buf4);
#else
      block_results[snp_id]->status = fit_lr_model(model, E, Y, NULL, NULL, NULL, NULL);
#endif

      // Writing the results to output file
      size_t buf_size = (model->k - model->a + LR_REPORT_SUMMARY_LINES) * LR_REPORT_LINE_LENGTH;

      if (outfile_ptr) { // only when outfile_ptr is not NULL
         write_lr_report(&(outfile_ptr[snp_id * buf_size]), block_results[snp_id], covariates, interactions);
      }

      // Storing the results for the model
      /*
       if(block_results[snp_id]){
       // Structure already allocated (re-run of the same SNP data)
       if((block_results[snp_id]->snp_name == NULL) || strcmp(block_results[snp_id]->snp_name, snp_locus_block[snp_id]->name)){
       Error("Results structure was allocated for invalid or different SNP data. Undefined behaviour");
       return STATUS_MALLOC_ERROR;
       }
       // Saving the reference log likelihood
       block_results[snp_id]->ref_aic = lr_model_aicc(block_results[snp_id]);
       }else{
       */

      //}
      //VP: Disabled Flushing
//		if (outfile_ptr)
//			msync(outfile_ptr, buf_size, MS_ASYNC);
#ifndef USE_PREALLOCATED_BUFFERS
      if (E) {
         my_mm_free(E);
         E = NULL;
      }
      if (Y) {
         my_mm_free(Y);
         Y = NULL;
      }
      if (results) {
         my_mm_free(results);
         results = NULL;
      }
#endif

   }

#ifdef USE_PREALLOCATED_BUFFERS
   /*
    * Freeing up the pre-allocated buffers
    */
   if (buf1)
      my_mm_free(buf1);
   if (buf2)
      my_mm_free(buf2);
   if (buf3)
      my_mm_free(buf3);
   if (buf4)
      my_mm_free(buf4);
   if (E)
      my_mm_free(E);
   if (Y)
      my_mm_free(Y);
#endif

   return STATUS_OK;
}

/**
 * Progress Bar variables and subroutines
 * NOT THREAD-SAFE: using static variables, which needs to be reset before (re)using this routine
 */
static int progress_percent;
static size_t progress_done;
static size_t progress_total;
static char progress_bar[4] = { '|', '/', '-', '\\' };

static inline void reset_progress(size_t total) {
   if (total < 1) {
      progress_percent = -1;
      return;
   }
   progress_percent = 0;
   progress_done = 0;
   progress_total = total;
}

static void show_progress(size_t done) {
   if (progress_percent < 0 || progress_done + done > progress_total) {
      return;
   }

   progress_done += done;

   uint32_t percent = ((double) progress_done / (double) progress_total) * 100.0;

   if (percent > progress_percent) {
      progress_percent = percent;
      fprintf(stderr, "\b\b\b\b\b\b\b %c %3d%%", progress_bar[progress_percent % 4], progress_percent);
   }

   if (progress_percent == 100) {
      fprintf(stderr, "\b\b\b\b\b\b\b   %3d%%\n", progress_percent);
      progress_percent = -1;
   }
}

// Add a covariate term. If cov_data is NULL, an empty structure will be allocated.
static STATUS add_cov_term(lr_terms_t *terms, const char* cov_name, const float* cov_data, const size_t cov_data_ct, term_spec_t *term_spec) {

   if (!terms || !cov_name || !(*cov_name) || !cov_data_ct) {
      return STATUS_PARAM_ERROR;
   }

   for (int i = 0; i < terms->cov_ct; i++) {
      if (!strcmp(terms->cov_name[i], cov_name)) {
         Debug("Covariate %s is already in the model (skipped)", cov_name);
         return STATUS_OK;
      }
   }

   // Buffer for covariate data
   float *data_buf = (float*) my_mm_malloc(sizeof(float) * cov_data_ct);
   if (!data_buf) {
      return STATUS_MALLOC_ERROR;
   }

   if (cov_data) {
      for (size_t i = 0; i < cov_data_ct; i++) {
         data_buf[i] = cov_data[i];
      }
   } else {
      memset(data_buf, 0, sizeof(float) * cov_data_ct);
   }

   char *name_buf = (char*) my_mm_malloc(sizeof(char) * (strlen(cov_name) + 1));
   if (!name_buf) {
      my_mm_free(data_buf);
      return STATUS_MALLOC_ERROR;
   }
   strcpy(name_buf, cov_name);

   // Allocating/extending structure for cov terms, if needed
   if (terms->cov_ct == terms->cov_sz) {

      char **cov_name_tmp = terms->cov_name;
      float **cov_data_tmp = terms->cov_data;
      term_spec_t** cov_term_spec_tmp = terms->term_spec;

      terms->cov_sz += 10;
      terms->cov_name = (char**) my_mm_malloc(sizeof(char*) * terms->cov_sz);
      memset(terms->cov_name, 0, sizeof(char*) * terms->cov_sz);
      terms->cov_data = (float**) my_mm_malloc(sizeof(float*) * terms->cov_sz);
      memset(terms->cov_data, 0, sizeof(float*) * terms->cov_sz);
      terms->term_spec = (term_spec_t**) my_mm_malloc(sizeof(term_spec_t*) * terms->cov_sz);
      memset(terms->term_spec, 0, sizeof(term_spec_t*) * terms->cov_sz);

      for (int i = 0; i < terms->cov_ct; i++) {
         terms->cov_name[i] = cov_name_tmp[i];
         terms->cov_data[i] = cov_data_tmp[i];
         terms->term_spec[i] = cov_term_spec_tmp[i];
      }

      if (cov_name_tmp)
         my_mm_free(cov_name_tmp);
      if (cov_data_tmp)
         my_mm_free(cov_data_tmp);
      if (cov_term_spec_tmp)
         my_mm_free(cov_term_spec_tmp);Debug("Covariate terms structure resized from %zu to %zu", terms->cov_ct, terms->cov_sz);
   }

   // Adding the new cov term
   terms->cov_name[terms->cov_ct] = name_buf;
   terms->cov_data[terms->cov_ct] = data_buf;
   terms->term_spec[terms->cov_ct] = term_spec;
   terms->cov_ct++;

   return STATUS_OK;
}

// Add a SNP term. NOT THREAD-SAFE (iterating through data-file rows)
static STATUS add_snp_term(lr_terms_t *terms, const char* snp_name, struct pio_file_t *pio_file, struct pio_locus_t** snp_locus_array, size_t snp_ct,
      size_t sample_ct) {

   int i = 0;

   if (!terms || !snp_name || !(*snp_name) || !pio_file || !snp_locus_array || !snp_ct || !sample_ct) {
      return STATUS_PARAM_ERROR;
   }

   for (int i = 0; i < terms->snp_ct; i++) {
      if (!strcmp(terms->snp_name[i], snp_name)) {
         Debug("Adding SNP %s skipped (already in the model)", snp_name);
         return STATUS_OK;
      }
   }

   // Resetting position to the beginning of the BED-file
   pio_reset_row(pio_file);

   // Checking if the SNP exists and seeking the genotype data in the genotype file
   while (i < snp_ct && strcmp(snp_locus_array[i]->name, snp_name)) {
      if (pio_next_row(pio_file, NULL) != PIO_OK) {
         Error("Term cannot be added to the model because the specified SNP couldn't be accessed.");
         return STATUS_UNKNOWN_ERROR;
      }
      i++;
   }

   if (i >= snp_ct) {
      Error("Term cannot be added to the model because the specified SNP wasn't found.");
      return STATUS_UNKNOWN_ERROR;
   }

   // Allocating buffer to keep SNP data
   void *data_buf = NULL;

   switch (pio_file->format) {
   case PIO_FORMAT_PLINK_BINARY:
      data_buf = (void*) my_mm_malloc(sizeof(snp_t) * sample_ct);
      break;
   case PIO_FORMAT_BEAGLE_LIKE:
      switch (pio_file->bgl_file.format) {
      case BGL2_VALUES_PER_SAMPLE:
         data_buf = (void*) my_mm_malloc(sizeof(float) * BGL2_VALUES_PER_SAMPLE * sample_ct);
         break;
      case BGL3_VALUES_PER_SAMPLE:
         data_buf = (void*) my_mm_malloc(sizeof(float) * BGL3_VALUES_PER_SAMPLE * sample_ct);
         break;
      default:
         break;
      }
      break;
   default:
      return STATUS_PARAM_ERROR;
      break;
   }

   if (!data_buf) {
      return STATUS_MALLOC_ERROR;
   }

   // Reading SNP genotype data
   if (pio_next_row(pio_file, data_buf) != PIO_OK) {
      if (data_buf)
         my_mm_free(data_buf);
      return STATUS_UNKNOWN_ERROR;
   }

   struct pio_locus_t *locus_buf = (struct pio_locus_t *) my_mm_malloc(sizeof(struct pio_locus_t));
   if (!locus_buf) {
      my_mm_free(data_buf);
      return STATUS_MALLOC_ERROR;
   }
   memset(locus_buf, 0, sizeof(struct pio_locus_t));

   locus_buf->chromosome = snp_locus_array[i]->chromosome;
   locus_buf->bp_position = snp_locus_array[i]->bp_position;
   locus_buf->pio_id = snp_locus_array[i]->pio_id;
   locus_buf->allele1 = strdup(snp_locus_array[i]->allele1);
   locus_buf->allele2 = strdup(snp_locus_array[i]->allele2);
   locus_buf->name = strdup(snp_locus_array[i]->name);
   locus_buf->position = snp_locus_array[i]->position;

   char *name_buf = (char*) my_mm_malloc(sizeof(char) * (strlen(snp_name) + 1));
   if (!name_buf) {
      my_mm_free(data_buf);
      my_mm_free(locus_buf);
      return STATUS_MALLOC_ERROR;
   }
   strcpy(name_buf, snp_name);

   // Allocating/extending structure for SNP terms, if needed
   if (terms->snp_ct == terms->snp_sz) {

      char **snp_name_tmp = terms->snp_name;
      struct pio_locus_t **snp_locus_tmp = terms->snp_locus;
      void **snp_data_tmp = terms->snp_data;

      terms->snp_sz += 10;
      terms->snp_name = (char**) my_mm_malloc(sizeof(char*) * terms->snp_sz);
      memset(terms->snp_name, 0, sizeof(char*) * terms->snp_sz);
      terms->snp_locus = (struct pio_locus_t**) my_mm_malloc(sizeof(struct pio_locus_t*) * terms->snp_sz);
      memset(terms->snp_locus, 0, sizeof(struct pio_locus_t*) * terms->snp_sz);
      terms->snp_data = (void**) my_mm_malloc(sizeof(void*) * terms->snp_sz);
      memset(terms->snp_data, 0, sizeof(snp_t*) * terms->snp_sz);

      for (int i = 0; i < terms->snp_ct; i++) {
         terms->snp_name[i] = snp_name_tmp[i];
         terms->snp_locus[i] = snp_locus_tmp[i];
         terms->snp_data[i] = snp_data_tmp[i];
      }

      if (snp_name_tmp)
         my_mm_free(snp_name_tmp);
      if (snp_locus_tmp)
         my_mm_free(snp_locus_tmp);
      if (snp_data_tmp)
         my_mm_free(snp_data_tmp);Debug("SNP terms structure resized from %zu to %zu", terms->snp_ct, terms->snp_sz);
   }

   // Adding the new SNP term
   terms->snp_name[terms->snp_ct] = name_buf;
   terms->snp_locus[terms->snp_ct] = locus_buf;
   terms->snp_data[terms->snp_ct] = data_buf;
   terms->snp_ct++;

   return STATUS_OK;
}

static STATUS read_term_spec_file(const char *input_file, lr_term_spec_list_t* spec, struct pio_file_t *pio_file, struct pio_cov_file_t *cov_file) {
   STATUS status = STATUS_UNKNOWN_ERROR;
   FILE *fp = NULL;
   char buf[LR_TERM_FILE_MAX_LINE_LEN];
   char *buf_state = NULL;
   term_spec_t* term_spec = NULL;

   if (!input_file || !spec || !pio_file || !cov_file) {
      return status;
   }

   if (!(fp = fopen(input_file, "r"))) {
      status = STATUS_IO_ERROR;
      return status;
   }

   status = STATUS_OK;

   // WORKAROUND TO SUPPORT WILKINSON NOTATION FOR ITERATING VARIABLE REPRESENTED WITH SYMBOL '$'
   // TODO: RESTRUCTURE THIS
   char *converted_term1 = NULL;
   char *converted_term2 = NULL;
//    while(fgets(buf, LR_TERM_FILE_MAX_LINE_LEN, fp)){
   while (converted_term1 || converted_term2 || fgets(buf, LR_TERM_FILE_MAX_LINE_LEN, fp)) {
      // END OF WORKAROUND
      char *inst_str = NULL;
      char *op_str = NULL;
      char *term1_str = NULL;
      char *term2_str = NULL;

      // WORKAROUND TO SUPPORT WILKINSON NOTATION FOR ITERATING VARIABLE REPRESENTED WITH SYMBOL '$'
      // TODO: RESTRUCTURE THIS
      if (converted_term1) {
         strcpy(buf, converted_term1);
         free(converted_term1);
         converted_term1 = NULL;
      } else if (converted_term2) {
         strcpy(buf, converted_term2);
         free(converted_term2);
         converted_term2 = NULL;
      }
      // END OF WORKAROUND

      status = STATUS_UNKNOWN_ERROR;
      buf[strcspn(buf, LR_TERM_FILE_NEW_LINE_DELIM)] = '\0';

      Debug("Reading term specification '%s'", buf);

      // Skipping empty lines
      if (!(strtok_r(strdup(buf), LR_TERM_FILE_FIELD_DELIM, &buf_state))) {
         status = STATUS_OK;
         continue;
      }
      buf_state = NULL;

      term_spec = (term_spec_t*) my_mm_malloc(sizeof(term_spec_t));
      if (term_spec) {
         memset(term_spec, 0, sizeof(term_spec_t));
      } else {
         status = STATUS_MALLOC_ERROR;
         break;
      }

      // Allocating or extending structure for term specs, if needed
      if (spec->term_spec_ct == spec->term_spec_sz) {
         term_spec_t** term_spec_tmp = spec->term_spec;
         spec->term_spec_sz += 10;
         spec->term_spec = (term_spec_t**) my_mm_malloc(sizeof(term_spec_t*) * spec->term_spec_sz);
         if (spec->term_spec) {
            memset(spec->term_spec, 0, sizeof(term_spec_t*) * spec->term_spec_sz);
         } else {
            status = STATUS_MALLOC_ERROR;
            break;
         }
         for (int i = 0; i < spec->term_spec_ct; i++) {
            spec->term_spec[i] = term_spec_tmp[i];
         }
         if (term_spec_tmp)
            my_mm_free(term_spec_tmp);
      }

      // Reading instruction field
      if (!(inst_str = strtok_r(strdup(buf), LR_TERM_FILE_FIELD_DELIM, &buf_state))) {
         Error("Could not read instruction symbol");
         break;
      } else {
         if (!strcmp(inst_str, LR_TERM_SPEC_INST_ADD_VAR_SYMB)) {
            // instruction +
            term_spec->inst = TERM_INST_ADD_VAR;
         } else if (!strcmp(inst_str, LR_TERM_SPEC_INST_REM_VAR_SYMB)) {
            // instruction -
            //term_spec->inst = TERM_INST_REM_VAR;
            Error("Removing terms from the model is currently unsupported");
            break;
         } else if (!strcmp(inst_str, LR_TERM_SPEC_INST_ADD_INT_SYMB)) {
            // instruction %
            term_spec->inst = TERM_INST_ADD_INT;
         } else {
            // invalid instruction
            Error("Invalid instruction '%s' in term spec", inst_str);
            break;
         }
      }

      // Reading term1 field
      if (!(term1_str = strtok_r(NULL, LR_TERM_FILE_FIELD_DELIM, &buf_state))) {
         Error("Could not read the name of the first term");
         Error("%s!", buf_state);
         break;
      } else {
         term_spec->term1_name = strdup(term1_str);
      }

      // Reading operator field and term2, if specified
      if ((op_str = strtok_r(NULL, LR_TERM_FILE_FIELD_DELIM, &buf_state))) {
         Debug("Operator string: '%s'", op_str);
         if (!strcmp(op_str, LR_TERM_SPEC_OP_MULT_NO_MARG_SYMB)) {
            // operator :
            term_spec->op = TERM_OP_MULT_NO_MARGINAL;
         } else if (!strcmp(op_str, LR_TERM_SPEC_OP_MULT_ADD_MARG_SYMB)) {
            // operator *
            term_spec->op = TERM_OP_MULT_ADD_MARGINAL;
         } else {
            // invalid operator
            Error("Invalid operator '%s' in term spec", op_str);
            break;
         }

         // Reading term2 field
         if (!(term2_str = strtok_r(NULL, LR_TERM_FILE_FIELD_DELIM, &buf_state))) {
            // missing second operand
            Error("Missing the second operand in term spec");
            break;
         } else {
            term_spec->term2_name = strdup(term2_str);
         }

         // Ensuring we reached end of line
         if (strtok_r(NULL, LR_TERM_FILE_FIELD_DELIM, &buf_state)) {
            Error("Unexpected values after the second operand in term spec");
            break;
         }
      }

      // WORKAROUND TO SUPPORT WILKINSON NOTATION FOR ITERATING VARIABLE REPRESENTED WITH SYMBOL '$'
      // TODO: RESTRUCTURE THIS

      /*
       Two cases of convertion:
       1) "+ $ : VAR" into "% VAR"
       2) "+ $ * VAR" into "+ VAR" and "% VAR"
       */
      if (term_spec->op && (!strcmp("$", term_spec->term1_name) || !strcmp("$", term_spec->term2_name))) {
         if (!strcmp("$", term_spec->term1_name) && !strcmp("$", term_spec->term2_name)) {
            Error("Self-interaction is unsupported (term '%s')", buf);
            break;
         }

         if (!strcmp("$", term_spec->term1_name)) {
            term_spec->term1_name = term_spec->term2_name;
         }

         if (term_spec->op == TERM_OP_MULT_ADD_MARGINAL) {
            converted_term1 = (char*) malloc(strlen("+ ") + strlen(term_spec->term1_name) + 1);
            sprintf(converted_term1, "+ %s", term_spec->term1_name);
         } else if (term_spec->op != TERM_OP_MULT_NO_MARGINAL) {
            Error("Invalid operator in term '%s'", buf);
            break;
         }

         converted_term2 = (char*) malloc(strlen("% ") + strlen(term_spec->term1_name) + 1);
         sprintf(converted_term2, "%% %s", term_spec->term1_name);

         continue;
      }
      // END OF WORKAROUND

      // Checking if term1 is a covariate
      for (int i = 0; i < cov_file->field_count; i++) {
         if (!strcmp(cov_file->field_names[i], term_spec->term1_name)) {
            term_spec->term1_type = TERM_TYPE_COVAR;
            break;
         }
      }

      // Checking if term1 is genotype
      if (!term_spec->term1_type) {
         for (int i = 0; i < pio_num_loci(pio_file); i++) {
            if (!strcmp(pio_get_locus(pio_file, i)->name, term_spec->term1_name)) {
               term_spec->term1_type = TERM_TYPE_SNP;
               break;
            }
         }
      }

      // Checking if term2 is a covariate
      if (term_spec->op) {
         for (int i = 0; i < cov_file->field_count; i++) {
            if (!strcmp(cov_file->field_names[i], term_spec->term2_name)) {
               term_spec->term2_type = TERM_TYPE_COVAR;
               break;
            }
         }
      }

      // Checking if term2 is genotype
      if (term_spec->op && !term_spec->term2_type) {
         for (int i = 0; i < pio_num_loci(pio_file); i++) {
            if (!strcmp(pio_get_locus(pio_file, i)->name, term_spec->term2_name)) {
               term_spec->term2_type = TERM_TYPE_SNP;
               break;
            }
         }
      }

      if (!term_spec->term1_type) {
         Error("Could not map the first operand in term '%s' to either covariate or genotype", buf);
         break;
      }

      if (term_spec->op && !term_spec->term2_type) {
         Error("Could not map the second operand in term '%s' to either covariate or genotype", buf);
         break;
      }

      // Validating terms specification
      if (term_spec->inst == TERM_INST_REM_VAR) {
         if (term_spec->op || (term_spec->term1_type != TERM_TYPE_COVAR)) {
            Error("Term specification \'%s\' is invalid", buf);
            break;
         }
      }

      // Checking for duplicated or comflicting specifications
      for (int i = 0; i < spec->term_spec_ct; i++) {

         // Different number of operands
         if ((!spec->term_spec[i]->op && term_spec->op) || (spec->term_spec[i]->op && !term_spec->op)) {
            continue;
         }

         // No operator in the term specification
         if (!spec->term_spec[i]->op && !term_spec->op) {
            // The same operand
            if (!strcmp(spec->term_spec[i]->term1_name, term_spec->term1_name)) {
               // Duplicated specification
               if (spec->term_spec[i]->inst == term_spec->inst) {
                  Warning("Duplicated term specification \'%s\'", buf);
                  free(term_spec->term1_name);
                  my_mm_free(term_spec);
                  term_spec = NULL;
                  status = STATUS_OK;
                  break;
               }
               // Canceling specification (+/-)
               if (((spec->term_spec[i]->inst == TERM_INST_ADD_VAR) && (term_spec->inst == TERM_INST_REM_VAR))
                     || ((spec->term_spec[i]->inst == TERM_INST_REM_VAR) && (term_spec->inst == TERM_INST_ADD_VAR))) {
                  Error("Term specification \'%s\' cancels previously defined term specification", buf);
                  free(term_spec->term1_name);
                  my_mm_free(term_spec);
                  term_spec = NULL;
                  break;
               }
            } else {
               continue;
            }
         } else if (spec->term_spec[i]->inst == term_spec->inst) {
            // Operator and two operands
            if ((!strcmp(spec->term_spec[i]->term1_name, term_spec->term1_name) && !strcmp(spec->term_spec[i]->term2_name, term_spec->term2_name))
                  || (!strcmp(spec->term_spec[i]->term1_name, term_spec->term2_name) && !strcmp(spec->term_spec[i]->term2_name, term_spec->term1_name))) {
               if (spec->term_spec[i]->op == term_spec->op) {
                  Warning("Duplicated term specification \'%s\'", buf);
                  status = STATUS_OK;
               } else {
                  Error("Term \'%s\' incompatible with previously specified one", buf);
               }
               if (term_spec->term1_name)
                  free(term_spec->term1_name);
               if (term_spec->term2_name)
                  free(term_spec->term2_name);
               my_mm_free(term_spec);
               term_spec = NULL;
               break;
            }
         }
      }

      if (!term_spec) {
         if (status) {
            break;
         } else {
            continue;
         }
      }

      // Adding the term specification to the list
      if (term_spec) {
         spec->term_spec[spec->term_spec_ct++] = term_spec;
         term_spec = NULL;

         Debug("Added inst %s; operator %s; for %s %s [ct: %zd]",
               inst_str,
               op_str,
               spec->term_spec[spec->term_spec_ct-1]->term1_name,
               spec->term_spec[spec->term_spec_ct-1]->term2_name,
               spec->term_spec_ct
         );
      }

      status = STATUS_OK;
   }

   if (status) {
      Error("Error in term specification file");
   }

   if (term_spec) {
      my_mm_free(term_spec);
   }

   if (fclose(fp)) {
      status = STATUS_IO_ERROR;
   }

   return status;
}

static STATUS write_term_spec_file(const char *output_file, lr_term_spec_list_t* spec) {
   STATUS status = STATUS_OK;
   FILE *fp = NULL;

   if (!spec || !output_file) {
      status = STATUS_PARAM_ERROR;
      return status;
   }

   if (!(fp = fopen(output_file, "w"))) {
      status = STATUS_IO_ERROR;
      return status;
   }

   for (int i = 0; i < spec->term_spec_ct; i++) {

      status = STATUS_OK;

      term_spec_t *term_spec = spec->term_spec[i];

      if (!term_spec || !term_spec->term1_name) {
         status = STATUS_UNKNOWN_ERROR;
         break;
      }

      switch (term_spec->inst) {
      case TERM_INST_ADD_VAR:
         fprintf(fp, "%s %s", LR_TERM_SPEC_INST_ADD_VAR_SYMB, term_spec->term1_name);
         break;
      case TERM_INST_REM_VAR:
         fprintf(fp, "%s %s", LR_TERM_SPEC_INST_REM_VAR_SYMB, term_spec->term1_name);
         break;
      case TERM_INST_ADD_INT:
         fprintf(fp, "%s %s", LR_TERM_SPEC_INST_ADD_INT_SYMB, term_spec->term1_name);
         break;
      default:
         status = STATUS_UNKNOWN_ERROR;
         break;
      }

      if (status) {
         break;
      }

      switch (term_spec->op) {
      case TERM_OP_NONE:
         break;
      case TERM_OP_MULT_NO_MARGINAL:
         if (term_spec->term2_name) {
            fprintf(fp, " %s %s", LR_TERM_SPEC_OP_MULT_NO_MARG_SYMB, term_spec->term2_name);
         } else {
            status = STATUS_UNKNOWN_ERROR;
         }
         break;
      case TERM_OP_MULT_ADD_MARGINAL:
         if (term_spec->term2_name) {
            fprintf(fp, " %s %s", LR_TERM_SPEC_OP_MULT_ADD_MARG_SYMB, term_spec->term2_name);
         } else {
            status = STATUS_UNKNOWN_ERROR;
         }
         break;
      default:
         status = STATUS_UNKNOWN_ERROR;
         break;
      }

      if (status) {
         break;
      }

      fprintf(fp, "%s", LR_TERM_FILE_NEW_LINE_DELIM);
   }

   if (fclose(fp)) {
      status = STATUS_IO_ERROR;
   }

   return status;
}

/**
 * Logistic regression process
 */
int logistic_regression_process(const char* plink_file_prefix,const char* bgen_vcf_filename,const char* bgen_vcf_sample_filename, const char* covar_file_name, const char* input_file_name,
      const char* output_file_prefix, unsigned long int config_flags, size_t num_of_threads, const char* interaction_str, const char* snp_covars_str,
      const char* covar_list, const char* new_covar_str) {


   size_t snp_buffer_idx = 0;
   int errors = 0;
   struct pio_file_t pio_file;
   struct pio_cov_file_t cov_file;
   lr_terms_t interactions;
   lr_terms_t covariates;
   inheritance_mode_t inheritance;
   lr_term_spec_list_t term_spec_list;

   // Determining mode of inheritance
   if (config_flags & CONF_INH_REC) {
      inheritance = INH_MODE_RECESSIVE;
   } else if (config_flags & CONF_INH_DOM) {
      inheritance = INH_MODE_DOMINANT;
   } else if (config_flags & CONF_INH_ADD) {
      inheritance = INH_MODE_ADDITIVE;
   } else {
      Error("Invalid mode of inheritance configured");
      return STATUS_UNKNOWN_ERROR;
   }

   // Checking options compatibility
   if (!covar_file_name && covar_list) {
      Error("Covariate list option specified without covariate file option. Unsupported configuration.");
      return STATUS_UNKNOWN_ERROR;
   }

   // Checking options compatibility
   if (input_file_name && (interaction_str || covar_list || snp_covars_str || new_covar_str)) {
      Error("Model specification input file and command line options mixed. Unsupported configuration.");
      return STATUS_UNKNOWN_ERROR;
   }

   // Execution time profiling when in verbose mode
#if defined(_OPENMP)
   double time_zero = 0;
   double time_diff = 0;
   ClearTimeElapsed(config_flags, &time_zero);
   ClearTimeElapsed(config_flags, &time_diff);
#endif

   // Initializing structures
   memset(&pio_file, 0, sizeof(struct pio_file_t));
   memset(&cov_file, 0, sizeof(struct pio_cov_file_t));
   memset(&interactions, 0, sizeof(lr_terms_t));
   memset(&covariates, 0, sizeof(lr_terms_t));
   memset(&term_spec_list, 0, sizeof(lr_term_spec_list_t));

   // Opening file-set
   if (config_flags & CONF_INPUT_BEAGLE) {
      // BEAGLE
      if (pio_open(&pio_file, plink_file_prefix, PIO_FORMAT_BEAGLE_LIKE) != PIO_OK) {
         Error("Could not open BEAGLE file-set with prefix %s", plink_file_prefix);
         return STATUS_UNKNOWN_ERROR;
      }ShowTimeElapsed(config_flags, &time_diff, "BEAGLE file-set opened");
   } else if (config_flags & CONF_INPUT_PLINK) {
      // PLINK
      if (pio_open(&pio_file, plink_file_prefix, PIO_FORMAT_PLINK_BINARY) != PIO_OK) {
         Error("Could not open PLINK file-set with prefix %s", plink_file_prefix);
         return STATUS_UNKNOWN_ERROR;
      }ShowTimeElapsed(config_flags, &time_diff, "PLINK file-set opened");
      // Checking if PLINK file-set contains single SNP for all individuals per row, as opposed to the opposite storage fashion
      if (!pio_one_locus_per_row(&pio_file)) {
         Warning("PLINK BED-file must contain a single SNP (for all individuals) in each row.");
         //todo review this command with new I/O
         Warning("Use command \"transpose\" to convert this file-set and re-run program with the resulting file-set.");
         pio_close(&pio_file);
         return STATUS_UNKNOWN_ERROR;
      }
   } else if (config_flags & CONF_INPUT_BGEN) {
      //TODO: Add Proper Checks for BGEN
   } else if (config_flags & CONF_INPUT_VCF) {
      //TODO: Add Proper Checks for VCF
   } else if (config_flags & CONF_INPUT_VCF_GZIPPED) {
      //TODO: Add Proper Checks for VCF_GZIPPED
   } else {
      return STATUS_UNKNOWN_ERROR;
   }

   // Reading covariates from file
   if (config_flags & CONF_COVARFILE) {
      if (cov_open(&cov_file, covar_file_name) != PIO_OK) {
         Error("Could not read covariate-file at %s", covar_file_name);
         return STATUS_UNKNOWN_ERROR;
      }ShowTimeElapsed(config_flags, &time_diff, "Covariates read");
   }

   // Reading terms specification from input file
   if (input_file_name && read_term_spec_file(input_file_name, &term_spec_list, &pio_file, &cov_file)) {
      Error("Could not read the terms specification from %s", input_file_name);
      return STATUS_UNKNOWN_ERROR;
   }

   // Processing block size
   if (blocksize == 0)
      blocksize = LR_DEFAULT_BLOCK_SIZE;

   size_t blk_size = blocksize;

   Verbose("Using Blocksize %d ", blk_size);

   // Parameter count
   size_t param_ct = LR_INTERCEPT_NUM_PARAMS + LR_EXPOSURE_NUM_PARAMS;

   // SNP count
   size_t snp_ct = 0;

   // Sample count
   size_t sample_ct = 0;

   //BGEN_VCF:10
   if (config_flags & CONF_INPUT_BEAGLE) {
      snp_ct = pio_num_loci(&pio_file);
      sample_ct = pio_num_samples(&pio_file);
   } else if (config_flags & CONF_INPUT_PLINK) {
      snp_ct = pio_num_loci(&pio_file);
      sample_ct = pio_num_samples(&pio_file);
   } else if (config_flags & CONF_INPUT_BGEN) {
      snp_ct = bgen_getsnps(bgen_vcf_filename);
      sample_ct = bgen_getsamples(bgen_vcf_filename);
   } else if (config_flags & CONF_INPUT_VCF) {
      snp_ct = vcf_getsnps(bgen_vcf_filename);
      sample_ct = vcf_getsamples(bgen_vcf_sample_filename);
   } else if (config_flags & CONF_INPUT_VCF_GZIPPED) {
      snp_ct = vcf_gz_getsnps(bgen_vcf_filename);
      sample_ct = vcf_gz_getsamples(bgen_vcf_sample_filename);
   }

   // Sanity check
   if (snp_ct < 1 || sample_ct < 2) {
      Error("Input file-set doesn't contain required data (too few samples or SNPs)");
      return STATUS_UNKNOWN_ERROR;
   }

   // SNP locus info
   struct pio_locus_t** snp_locus_array = (struct pio_locus_t**) my_mm_malloc(sizeof(struct pio_locus_t*) * snp_ct);
   memset(snp_locus_array, 0, sizeof(struct pio_locus_t*) * snp_ct);

   // SNP genotype //BGEN_VCF:14 Changed from void** to float**
   float** snp_buffer_array = (float**) my_mm_malloc(sizeof(float*) * snp_ct);
   memset(snp_buffer_array, 0, sizeof(float*) * snp_ct);

   // SNP phenotype
   alg_float_ptr pheno_array = (alg_float_ptr) my_mm_malloc(sizeof(float) * sample_ct);

   // Sex
   alg_int_ptr sex_array = (alg_int_ptr) my_mm_malloc(sizeof(int) * sample_ct);

   // Results
   lr_process_result_t** results = (lr_process_result_t**) my_mm_malloc(sizeof(lr_process_result_t*) * snp_ct);
   memset(results, 0, sizeof(struct lr_process_result*) * snp_ct);
   ShowTimeElapsed(config_flags, &time_diff, "Data structures allocated");

   // Reading locus info
   for (int i = 0; i < snp_ct; i++) {

      //BGEN_VCF:11
      if (config_flags & (CONF_INPUT_BEAGLE | CONF_INPUT_PLINK)) {
         snp_locus_array[i] = pio_get_locus(&pio_file, i);
      } else if (config_flags & CONF_INPUT_BGEN) {
         //TODO: Change to proper file
         BGenFile *bgen = bgen_open(bgen_vcf_filename);
         VariantIndexing *index;
         Variant *variants = bgen_read_variants(bgen, &index);
         struct pio_locus_t* locus = (struct pio_locus_t*) malloc(sizeof(struct pio_locus_t));
         locus->allele1 = bgen_getallele3(bgen_vcf_filename, i, variants);
         locus->allele2 = bgen_getallele2(bgen_vcf_filename, i, variants);
         locus->bp_position = bgen_getpos(bgen_vcf_filename, i, variants);
         locus->chromosome = atoi(bgen_getid(bgen_vcf_filename, i, variants));
         locus->file_pos = 0;
         locus->name = bgen_getchromosome2(bgen_vcf_filename, i, variants);
         locus->pio_id = i;
         locus->position = 0.0;
         snp_locus_array[i] = locus;
      } else if (config_flags & CONF_INPUT_VCF) {
         //TODO: Ask Kosta for this place
      } else if (config_flags & CONF_INPUT_VCF_GZIPPED) {
         //TODO: Ask Kosta for this place
      }
   }

   ShowTimeElapsed(config_flags, &time_diff, "Genotype locus info read");

   // Adding non-genotype covariate terms (data is copied in the later step)
   for (int i = LR_COVAR_FILE_SAMPLE_ID_NUM_COLUMNS; i < cov_file.field_count; i++) {

      // In case there was a list of covariates to add, adding the covariate only if it was explicitly listed in the terms file
      if (input_file_name) {
         int j = 0;
         while (j < term_spec_list.term_spec_ct) {
            if (term_spec_list.term_spec[j]->inst == TERM_INST_ADD_VAR && !term_spec_list.term_spec[j]->op
                  && term_spec_list.term_spec[j]->term1_type == TERM_TYPE_COVAR
                  && !strcmp(term_spec_list.term_spec[j]->term1_name, cov_file.field_names[i])) {
               break;
            }
            j++;
         }

         if (j == term_spec_list.term_spec_ct) {
            continue;
         }
      }

      STATUS s = add_cov_term(&covariates, cov_file.field_names[i], NULL, sample_ct, NULL);
      if (s) {
         Error("Covariate %s couldn't be added to the model", cov_file.field_names[i]);
         return s;
      } else {
         Verbose("Covariate %s will be added to the model", cov_file.field_names[i]);
      }
   }

   //	ShowTimeElapsed(config_flags, &time_diff, "VP Covariates Read:"); //TODO: Remove that before release
   // Adding SNP covariate terms
   for (int j = 0; j < term_spec_list.term_spec_ct; j++) {
      if (term_spec_list.term_spec[j]->inst == TERM_INST_ADD_VAR && !term_spec_list.term_spec[j]->op
            && term_spec_list.term_spec[j]->term1_type == TERM_TYPE_SNP) {
         // Adding SNP covariate
         STATUS s = add_snp_term(&covariates, term_spec_list.term_spec[j]->term1_name, &pio_file, snp_locus_array, snp_ct, sample_ct);
         if (s) {
            Error("SNP %s couldn't be added to the model as a covariate", term_spec_list.term_spec[j]->term1_name);
            return s;
         } else {
            Verbose("SNP %s added to the model as a covariate", term_spec_list.term_spec[j]->term1_name);
         }
      }
   }
//	ShowTimeElapsed(config_flags, &time_diff, "VP Covariates Read - Place 1:");
   // Adding interaction and interacting terms
   for (int j = 0; j < term_spec_list.term_spec_ct; j++) {

      // Skipping for any non-interaction and non-interacting terms
      if (!((term_spec_list.term_spec[j]->inst == TERM_INST_ADD_INT)
            || (term_spec_list.term_spec[j]->inst == TERM_INST_ADD_VAR && term_spec_list.term_spec[j]->op))) {
         continue;
      }

      // Interacting term (two-way: main effect interacts with single SNP/COV)
      if (!term_spec_list.term_spec[j]->op) {
         //SNP
         if (term_spec_list.term_spec[j]->term1_type == TERM_TYPE_SNP) {
            // Adding SNP covariate
            STATUS s = add_snp_term(&interactions, term_spec_list.term_spec[j]->term1_name, &pio_file, snp_locus_array, snp_ct, sample_ct);
            if (s) {
               Error("SNP %s couldn't be added to the model as an interacting term", term_spec_list.term_spec[j]->term1_name);
               return s;
            } else {
               Verbose("SNP %s added to the model as an interacting term", term_spec_list.term_spec[j]->term1_name);
            }
         }
         // COV
         if (term_spec_list.term_spec[j]->term1_type == TERM_TYPE_COVAR) {
            STATUS s = add_cov_term(&interactions, term_spec_list.term_spec[j]->term1_name, NULL, sample_ct, term_spec_list.term_spec[j]);
            if (s) {
               Error("Cov %s couldn't be added to the model as an interacting term", term_spec_list.term_spec[j]->term1_name);
               return s;
            } else {
               Verbose("Cov %s added to the model as an interacting term", term_spec_list.term_spec[j]->term1_name);
            }
         }
         // Done
         continue;
      }

      // Adding marginal effect of interacting term (three-way: main effect interacts with two SNPs/COVs) or interaction (two-way: two SNPs/COVs)
      if (term_spec_list.term_spec[j]->op == TERM_OP_MULT_ADD_MARGINAL) {
         // First operand
         // SNP
         if (term_spec_list.term_spec[j]->term1_type == TERM_TYPE_SNP) {
            // Adding SNP covariate
            int i = 0;
            while (i < covariates.snp_ct) {
               if (!strcmp(covariates.snp_name[i++], term_spec_list.term_spec[j]->term1_name)) {
                  break;
               }
            }
            if (i == covariates.snp_ct) {
               STATUS s = add_snp_term(&covariates, term_spec_list.term_spec[j]->term1_name, &pio_file, snp_locus_array, snp_ct, sample_ct);
               if (s) {
                  Error("SNP %s couldn't be added to the model as marginal effect of interaction", term_spec_list.term_spec[j]->term1_name);
                  return s;
               } else {
                  Verbose("SNP %s added to the model as marginal effect of interaction", term_spec_list.term_spec[j]->term1_name);
               }
            }
         }
         // COV
         if (term_spec_list.term_spec[j]->term1_type == TERM_TYPE_COVAR) {
            int i = 0;
            while (i < covariates.cov_ct) {
               if (!strcmp(covariates.cov_name[i++], term_spec_list.term_spec[j]->term1_name)) {
                  break;
               }
            }
            if (i == covariates.cov_ct) {
               STATUS s = add_cov_term(&covariates, term_spec_list.term_spec[j]->term1_name, NULL, sample_ct, NULL);
               if (s) {
                  Error("Cov %s couldn't be added to the model (as marginal effect of interaction)", term_spec_list.term_spec[j]->term1_name);
                  return s;
               } else {
                  Verbose("Cov %s added to the model (as marginal effect of interaction)", term_spec_list.term_spec[j]->term1_name);
               }
            }
         }

         // Second operand
         // SNP
         if (term_spec_list.term_spec[j]->term2_type == TERM_TYPE_SNP) {
            // Adding SNP covariate
            int i = 0;
            while (i < covariates.snp_ct) {
               if (!strcmp(covariates.snp_name[i++], term_spec_list.term_spec[j]->term2_name)) {
                  break;
               }
            }
            if (i == covariates.snp_ct) {
               STATUS s = add_snp_term(&covariates, term_spec_list.term_spec[j]->term2_name, &pio_file, snp_locus_array, snp_ct, sample_ct);
               if (s) {
                  Error("SNP %s couldn't be added to the model as marginal effect of interaction", term_spec_list.term_spec[j]->term2_name);
                  return s;
               } else {
                  Verbose("SNP %s added to the model as marginal effect of interaction", term_spec_list.term_spec[j]->term2_name);
               }
            }
         }
         // COV
         if (term_spec_list.term_spec[j]->term2_type == TERM_TYPE_COVAR) {
            int i = 0;
            while (i < covariates.cov_ct) {
               if (!strcmp(covariates.cov_name[i++], term_spec_list.term_spec[j]->term2_name)) {
                  break;
               }
            }
            if (i == covariates.cov_ct) {
               STATUS s = add_cov_term(&covariates, term_spec_list.term_spec[j]->term2_name, NULL, sample_ct, NULL);
               if (s) {
                  Error("Cov %s couldn't be added to the model (as marginal effect of interaction)", term_spec_list.term_spec[j]->term2_name);
                  return s;
               } else {
                  Verbose("Cov %s added to the model (as marginal effect of interaction)", term_spec_list.term_spec[j]->term2_name);
               }
            }
         }
      }

      // Interacting term (three-way: main effect interacts with two SNPs/COVs)
      if (term_spec_list.term_spec[j]->op == TERM_OP_MULT_ADD_MARGINAL || term_spec_list.term_spec[j]->op == TERM_OP_MULT_NO_MARGINAL) {

         size_t name_len = strlen(term_spec_list.term_spec[j]->term1_name) + strlen(LT_TERM_INTERACTION_SYMBOL)
               + strlen(term_spec_list.term_spec[j]->term2_name) + 1;
         char *name = (char*) malloc(name_len);
         float *data = (float*) malloc(sizeof(float) * sample_ct);

         // Temporary covariate buffer
         for (int i = 0; i < sample_ct; i++) {
            data[i] = 1.00f;
         }

         // Interacting term name
         sprintf(name, "%s%s%s", term_spec_list.term_spec[j]->term1_name,
         LT_TERM_INTERACTION_SYMBOL, term_spec_list.term_spec[j]->term2_name);

         // First operand
         if (term_spec_list.term_spec[j]->term1_type == TERM_TYPE_SNP) {
            int i = 0;
            void *data_buf = NULL;
            switch (pio_file.format) {
            case PIO_FORMAT_PLINK_BINARY:
               data_buf = (void*) my_mm_malloc(sizeof(snp_t) * sample_ct);
               break;
            case PIO_FORMAT_BEAGLE_LIKE:
               switch (pio_file.bgl_file.format) {
               case BGL2_VALUES_PER_SAMPLE:
                  data_buf = (void*) my_mm_malloc(sizeof(float) * BGL2_VALUES_PER_SAMPLE * sample_ct);
                  break;
               case BGL3_VALUES_PER_SAMPLE:
                  data_buf = (void*) my_mm_malloc(sizeof(float) * BGL3_VALUES_PER_SAMPLE * sample_ct);
                  break;
               default:
                  break;
               }
               break;
            default:
               return STATUS_PARAM_ERROR;
               break;
            }

            pio_reset_row(&pio_file);
            // Checking if the SNP exists and seeking the genotype data
            while (i < snp_ct && strcmp(snp_locus_array[i]->name, term_spec_list.term_spec[j]->term1_name)) {
               if (pio_next_row(&pio_file, NULL) != PIO_OK) {
                  Error("Term cannot be added to the model because the specified SNP couldn't be accessed.");
                  return STATUS_UNKNOWN_ERROR;
               }
               i++;
            }
            if (i == snp_ct) {
               Error("Term cannot be added to the model because the specified SNP wasn't found.");
               return STATUS_UNKNOWN_ERROR;
            }

            // Adding the SNP genotype data from data_buf to data (multiplication)
            if (pio_next_row(&pio_file, data_buf) != PIO_OK) {
               if (data_buf)
                  my_mm_free(data_buf);
               return STATUS_UNKNOWN_ERROR;
            }

            for (i = 0; i < sample_ct; i++) {
               switch (pio_file.format) {
               case PIO_FORMAT_PLINK_BINARY:
                  data[i] *= genotype_to_float(((snp_t*) data_buf)[i], inheritance);
                  break;
               case PIO_FORMAT_BEAGLE_LIKE:
                  switch (pio_file.bgl_file.format) {
                  case BGL2_VALUES_PER_SAMPLE:
                  case BGL3_VALUES_PER_SAMPLE:
                     data[i] *= genotype_bgl_to_float((float*) data_buf, i, inheritance, pio_file.bgl_file.format);
                     break;
                  default:
                     break;
                  }
                  break;
               default:
                  return STATUS_PARAM_ERROR;
                  break;
               }
            }
         }

         // Second operand
         if (term_spec_list.term_spec[j]->term2_type == TERM_TYPE_SNP) {
            int i = 0;
            void *data_buf = NULL;
            switch (pio_file.format) {
            case PIO_FORMAT_PLINK_BINARY:
               data_buf = (void*) my_mm_malloc(sizeof(snp_t) * sample_ct);
               break;
            case PIO_FORMAT_BEAGLE_LIKE:
               switch (pio_file.bgl_file.format) {
               case BGL2_VALUES_PER_SAMPLE:
                  data_buf = (void*) my_mm_malloc(sizeof(float) * BGL2_VALUES_PER_SAMPLE * sample_ct);
                  break;
               case BGL3_VALUES_PER_SAMPLE:
                  data_buf = (void*) my_mm_malloc(sizeof(float) * BGL3_VALUES_PER_SAMPLE * sample_ct);
                  break;
               default:
                  break;
               }
               break;
            default:
               return STATUS_PARAM_ERROR;
               break;
            }
            pio_reset_row(&pio_file);
            // Checking if the SNP exists and seeking the genotype data in the BED-file
            while (i < snp_ct && strcmp(snp_locus_array[i]->name, term_spec_list.term_spec[j]->term2_name)) {
               if (pio_next_row(&pio_file, NULL) != PIO_OK) {
                  Error("Term cannot be added to the model because the specified SNP couldn't be accessed.");
                  return STATUS_UNKNOWN_ERROR;
               }
               i++;
            }
            if (i == snp_ct) {
               Error("Term cannot be added to the model because the specified SNP wasn't found.");
               return STATUS_UNKNOWN_ERROR;
            }

            // Adding the SNP genotype data from data_buf to data (multiplication)
            if (pio_next_row(&pio_file, data_buf) != PIO_OK) {
               if (data_buf)
                  my_mm_free(data_buf);
               return STATUS_UNKNOWN_ERROR;
            }

            for (i = 0; i < sample_ct; i++) {
               switch (pio_file.format) {
               case PIO_FORMAT_PLINK_BINARY:
                  data[i] *= genotype_to_float(((snp_t*) data_buf)[i], inheritance);
                  break;
               case PIO_FORMAT_BEAGLE_LIKE:
                  switch (pio_file.bgl_file.format) {
                  case BGL2_VALUES_PER_SAMPLE:
                  case BGL3_VALUES_PER_SAMPLE:
                     data[i] *= genotype_bgl_to_float((float*) data_buf, i, inheritance, pio_file.bgl_file.format);
                     break;
                  default:
                     break;
                  }
                  break;
               default:
                  return STATUS_PARAM_ERROR;
                  break;
               }
            }
         }

         // Adding the interacting term
         STATUS s = STATUS_UNKNOWN_ERROR;

         if (term_spec_list.term_spec[j]->inst == TERM_INST_ADD_INT) {
            s = add_cov_term(&interactions, name, data, sample_ct, term_spec_list.term_spec[j]);
            if (s) {
               Error("Could not add non-genotype interacting term %s", name);
               if (name)
                  free(name);
               if (data)
                  free(data);
               return s;
            } else {
               Verbose("Cov %s added to the model as an interacting term", name);
            }
         }

         if ((term_spec_list.term_spec[j]->inst == TERM_INST_ADD_VAR)
               || (term_spec_list.term_spec[j]->inst == TERM_INST_ADD_INT && term_spec_list.term_spec[j]->op == TERM_OP_MULT_ADD_MARGINAL)) {
            s = add_cov_term(&covariates, name, data, sample_ct, term_spec_list.term_spec[j]);
            if (s) {
               Error("Could not add non-genotype interaction term %s", name);
               if (name)
                  free(name);
               if (data)
                  free(data);
               return s;
            } else {
               Verbose("Cov %s added to the model as an interaction term", name);
            }
         }

         if (name)
            free(name);
         if (data)
            free(data);
      }
   }

   //Reading phenotype and matching non-genotype covariates
   unsigned long hash_position = 0;
   char *last_key;
   struct pio_cov_sample_t *cov_sample_ptr2 = NULL;
   int covar_number = 0;
   if (covar_file_name != NULL)
      covar_number = cov_num_samples(&cov_file);

   hash_record hashtable[covar_number];
   if (covar_file_name != NULL) {
      //TODO VP: Replace Table with pointer allocation to be able to free the used memory for environments with low memory
      memset(hashtable, 0, covar_number * sizeof(hash_record));

      for (int j = 0; j < cov_num_samples(&cov_file); j++) {
         cov_sample_ptr2 = cov_get_sample(&cov_file, j);
         hash_position = hash(cov_sample_ptr2->fid, covar_number);

         while (hashtable[hash_position].fid != NULL) {
            hash_position++;
            if (hash_position > covar_number) {
               hash_position = 0;
            }
         }
         hashtable[hash_position].fid = cov_sample_ptr2->fid;
         hashtable[hash_position].iid = cov_sample_ptr2->iid;
         hashtable[hash_position].position = j;
      }
   }

   int i;
   int missed = 0;

   for (i = 0; i < sample_ct; i++) {
      struct pio_cov_sample_t *cov_sample_ptr = NULL;

      //BGEN_VCF:12
      char *fid = NULL;
      char *iid = NULL;

      // Phenotype (i.e. disease affection status)
      if (config_flags & (CONF_INPUT_BEAGLE | CONF_INPUT_PLINK)) {
         struct pio_sample_t *smpl = pio_get_sample(&pio_file, i);
         pheno_array[i] = smpl->affection;
         sex_array[i] = smpl->sex;
         fid = smpl->fid;
         iid = smpl->iid;

      } else if (config_flags & CONF_INPUT_BGEN) {
         struct bgenpheno mybgen = readsamplefileall("input.sample", i);
         pheno_array[i] = mybgen.phenotype;
         if (pheno_array[i] == 0.0) {
            numberofcontrols++;
         }
         if (pheno_array[i] == 1.0) {
            numberofcases++;
         }

         sex_array[i] = mybgen.sex;
         fid = mybgen.fid;
         iid = mybgen.iid;

      } else if (config_flags & CONF_INPUT_VCF) {
         //TODO: Ask Kostas
      } else if (config_flags & CONF_INPUT_VCF_GZIPPED) {
         //TODO: Ask Kostas
      }

      // Linking covariates (from the covariate file) to the sample read from the PLINK file-set
      if (cov_file.field_count > LR_COVAR_FILE_SAMPLE_ID_NUM_COLUMNS) {
         short found = 0;
         short stop = 0;

         if (covar_file_name != NULL) {
            hash_position = hash(fid, covar_number);

            int current_position = hash_position;
            while (stop == 0) {
               //BGEN_VCF:12
               if (hashtable[hash_position].fid != NULL && !strcmp(hashtable[hash_position].fid, fid) && !strcmp(hashtable[hash_position].iid, iid)) {
                  found = 1;
                  cov_sample_ptr = cov_get_sample(&cov_file, hashtable[hash_position].position);
               }
               hash_position++;
               if (found == 1 || hash_position == current_position)
                  stop = 1;
               if (hash_position >= covar_number)
                  hash_position = 0;
            }
         }

         if (found == 0) {

            missed++;
            int j;
            for (j = 0; j < cov_num_samples(&cov_file); j++) {
               cov_sample_ptr = cov_get_sample(&cov_file, j);
               //BGEN_VCF:12
               if (cov_sample_ptr && !strcmp(cov_sample_ptr->fid, fid) && !strcmp(cov_sample_ptr->iid, iid)) {
                  break;
               }
            }

            if (j >= cov_num_samples(&cov_file)) {
               Error("Could not map the PLINK data sample with respective sample data in the covariate file");
               return STATUS_UNKNOWN_ERROR;
            }
         }

         // Copying non-genotype covariate data
         for (int k = 0; k < covariates.cov_ct; k++) {
            if (covariates.term_spec[k]
                  && (covariates.term_spec[k]->op == TERM_OP_MULT_ADD_MARGINAL || covariates.term_spec[k]->op == TERM_OP_MULT_NO_MARGINAL)) {

               //printf("	OP : %d\n", covariates.term_spec[k]->op);
               // Two operands
               if (covariates.term_spec[k]->term1_type == TERM_TYPE_COVAR) {
                  int l = LR_COVAR_FILE_SAMPLE_ID_NUM_COLUMNS;
                  while (l < cov_file.field_count && strcmp(cov_file.field_names[l], covariates.term_spec[k]->term1_name)) {
                     l++;
                     printf("%d %s | %s | %d \n", cov_file.field_count, cov_file.field_names[l], covariates.term_spec[k]->term1_name,
                           strcmp(cov_file.field_names[l], covariates.term_spec[k]->term1_name));
                  }
                  if (l == cov_file.field_count)
                     continue;
                  covariates.cov_data[k][i] *= cov_sample_ptr->covars[l];
               }
               if (covariates.term_spec[k]->term2_type == TERM_TYPE_COVAR) {
                  int l = LR_COVAR_FILE_SAMPLE_ID_NUM_COLUMNS;
                  while (l < cov_file.field_count && strcmp(cov_file.field_names[l], covariates.term_spec[k]->term2_name)) {
                     l++;
                  }
                  if (l == cov_file.field_count)
                     continue;
                  covariates.cov_data[k][i] *= cov_sample_ptr->covars[l];
               }
            } else if (!covariates.term_spec[k]) {
               int l = LR_COVAR_FILE_SAMPLE_ID_NUM_COLUMNS;
               while (l < cov_file.field_count && strcmp(cov_file.field_names[l], covariates.cov_name[k])) {
                  l++;
               }
               if (l == cov_file.field_count)
                  continue;
               covariates.cov_data[k][i] = cov_sample_ptr->covars[l];
            } else {
               Error("Unrecognized operator for interacting term");
               return STATUS_UNKNOWN_ERROR;
            }
         }

         // Copying interacting covariate data
         for (int k = 0; k < interactions.cov_ct; k++) {

            printf(".");
            if (interactions.term_spec[k]->op == TERM_OP_MULT_ADD_MARGINAL || interactions.term_spec[k]->op == TERM_OP_MULT_NO_MARGINAL) {
               // Two operands
               if (interactions.term_spec[k]->term1_type == TERM_TYPE_COVAR) {
                  int l = LR_COVAR_FILE_SAMPLE_ID_NUM_COLUMNS;
                  while (l < cov_file.field_count && strcmp(cov_file.field_names[l], interactions.term_spec[k]->term1_name)) {
                     l++;
                  }
                  if (l == cov_file.field_count)
                     continue;
                  interactions.cov_data[k][i] *= cov_sample_ptr->covars[l];
               }
               if (interactions.term_spec[k]->term2_type == TERM_TYPE_COVAR) {
                  int l = LR_COVAR_FILE_SAMPLE_ID_NUM_COLUMNS;
                  while (l < cov_file.field_count && strcmp(cov_file.field_names[l], interactions.term_spec[k]->term2_name)) {
                     l++;
                  }
                  if (l == cov_file.field_count)
                     continue;
                  interactions.cov_data[k][i] *= cov_sample_ptr->covars[l];
               }
            } else if (interactions.term_spec[k]->op == TERM_OP_NONE) {
               int l = LR_COVAR_FILE_SAMPLE_ID_NUM_COLUMNS;
               while (l < cov_file.field_count && strcmp(cov_file.field_names[l], interactions.cov_name[k])) {
                  l++;
               }
               if (l == cov_file.field_count)
                  continue;
               interactions.cov_data[k][i] = cov_sample_ptr->covars[l];
            } else {
               Error("Unrecognized operator for interacting term");
               return STATUS_UNKNOWN_ERROR;
            }
         }
      }
   }

   // Closing covariate file
   cov_close(&cov_file);

   // Writing terms specification to output file
   char *terms_output_file = (char*) malloc(sizeof(char) * (strlen(output_file_prefix) + strlen(LR_TERMS_SPEC_EXTENSION)) + 1);
   *terms_output_file = '\0';
   strcat(terms_output_file, output_file_prefix);
   strcat(terms_output_file, LR_TERMS_SPEC_EXTENSION);
   if (write_term_spec_file(terms_output_file, &term_spec_list)) {
      Error("Could not write the terms specification to %s", terms_output_file);
      return STATUS_UNKNOWN_ERROR;
   }

   // Adjusting the parameter count by the number of extra SNP covariates, if any
   if (covariates.snp_ct) {
      param_ct += covariates.snp_ct;
      Verbose("Number of SNP covariates: %lu", covariates.snp_ct);
   }

   // Adjusting the parameter count by the number of covariates, if any
   if (covariates.cov_ct) {
      param_ct += covariates.cov_ct;
      Verbose("Number of non-SNP covariates: %lu", covariates.cov_ct);
   }

   // Adjusting the parameter count by the number of interacting SNPs, if any
   if (interactions.snp_ct) {
      param_ct += interactions.snp_ct;
      Verbose("Number of interacting SNP covariates: %lu", interactions.snp_ct);
   }

   // Adjusting the parameter count by the number of interacting covariates, if any
   if (interactions.cov_ct) {
      param_ct += interactions.cov_ct;
      Verbose("Number of interacting non-SNP covariates: %lu", interactions.cov_ct);
   }

   Verbose("Total number of model parameters: %lu", param_ct);
   char *report_file_name = (char*) malloc(sizeof(char) * (strlen(output_file_prefix) + strlen(LR_REGRESS_REPORT_EXTENSION)) + 1);
   *report_file_name = '\0';
   strcat(report_file_name, output_file_prefix);
   strcat(report_file_name, LR_REGRESS_REPORT_EXTENSION);

   io_report_file_t* regression_report = open_report_file(report_file_name, snp_ct, (LR_REPORT_SUMMARY_LINES + param_ct - LR_INTERCEPT_NUM_PARAMS),
   LR_REPORT_LINE_LENGTH, false);					//VP [original was false]

   if (!regression_report) {
      Error("Could not open report file");
      return STATUS_UNKNOWN_ERROR;
   }

   reset_progress(snp_ct);

   //BGEN_VCF:13
   // Resetting PIO row pointer
   if (config_flags & (CONF_INPUT_BGEN | CONF_INPUT_BEAGLE))
      pio_reset_row(&pio_file);

   //BGEN_VCF:13
   afarray = (double *) my_mm_malloc(sizeof(double) * snp_ct);
   afarraycases = (double *) my_mm_malloc(sizeof(double) * snp_ct);
   afarraycontrols = (double *) my_mm_malloc(sizeof(double) * snp_ct);

#ifdef USE_OPEN_MP
   // OpenMP global thread pool configuration
   omp_set_num_threads(num_of_threads);
// Parallel computing using OpenMP
#pragma omp parallel firstprivate(blk_size)
#endif
#ifdef USE_OPEN_MP
#pragma omp for schedule(static,1)
#endif
   for (int i = 0; i < snp_ct; i += blk_size) {
      // Correcting size of the last block
      if (i + blk_size > snp_ct) {
         blk_size = snp_ct - i;
      }

      // Allocating structures and reading genotype data
#pragma omp critical(alloc)
      {
         while (snp_buffer_idx < i + blk_size) {
            switch (pio_file.format) {
            case PIO_FORMAT_PLINK_BINARY:
               snp_buffer_array[snp_buffer_idx] = (void*) my_mm_malloc(sizeof(snp_t) * sample_ct);
               break;
            case PIO_FORMAT_BEAGLE_LIKE:
               switch (pio_file.bgl_file.format) {
               case BGL2_VALUES_PER_SAMPLE:
                  snp_buffer_array[snp_buffer_idx] = (void*) my_mm_malloc(sizeof(float) * BGL2_VALUES_PER_SAMPLE * sample_ct);
                  break;
               case BGL3_VALUES_PER_SAMPLE:
                  snp_buffer_array[snp_buffer_idx] = (void*) my_mm_malloc(sizeof(float) * BGL3_VALUES_PER_SAMPLE * sample_ct);
                  break;
               default:
                  break;
               }
               break;
            default:
               break;

            }

            //BGEN_VCF:13
            if (config_flags & (CONF_INPUT_BEAGLE | CONF_INPUT_BGEN)) {
               if (pio_next_row(&pio_file, snp_buffer_array[snp_buffer_idx]) != PIO_OK) {
                  Error("Could not read the next row from file");
                  if (snp_buffer_array[snp_buffer_idx])
                     my_mm_free(snp_buffer_array[snp_buffer_idx]);
                  snp_buffer_array[snp_buffer_idx] = NULL;
               }
            } else if (config_flags & CONF_INPUT_BGEN) {
               VariantGenotype *vg = bgen_open_variant_genotype(index, snp_buffer_idx);
               real *probabilities = malloc(sample_ct * bgen_ncombs(vg) * sizeof(real));
               bgen_read_variant_genotype(index, vg, probabilities);
               for (int i2 = 0; i2 < sample_ct; i2++) {

                  real* probs = bgen_getgenotypicprobabilites(bgen_vcf_filename, snp_buffer_idx, i2, vg, probabilities);
                  real test = 0;
                  real maxprob = 0.0;
                  test = probs[1] + 2 * probs[2];

                  if (probs)
                     my_mm_free(probs);

                  snp_buffer_array[snp_buffer_idx][i2] = test;
               }
            }

            // find median value
           /* int *myvalues = (int *) malloc(sizeof(int) * sample_ct);
            for (int i2 = 0; i2 < sample_ct; i2++) {
               myvalues[i2] = snp_buffer_array[snp_buffer_idx][i2];
            }

            qsort(myvalues, sample_ct, sizeof(int), compare);
            int themedian = myvalues[sample_ct / 2];

            if (myvalues)
               my_mm_free(myvalues);

            int myalleles = 0;
            int myallelescases = 0;
            int myallelescontrols = 0;
            for (int i2 = 0; i2 < sample_ct; i2++) {
               // af for a1
               if (snp_buffer_array[snp_buffer_idx][i2] == 0) {
                  myalleles = myalleles + 2;
               }

               if (snp_buffer_array[snp_buffer_idx][i2] == 1) {
                  myalleles = myalleles + 1;
               }

               // af for a1 cases
               if (snp_buffer_array[snp_buffer_idx][i2] == 0) {
                  if (pheno_array[i2] == 1.0) {
                     myallelescases = myallelescases + 2;
                  }
               }

               if (snp_buffer_array[snp_buffer_idx][i2] == 1) {
                  if (pheno_array[i2] == 1.0) {
                     myallelescases = myallelescases + 1;
                  }
               }

               // af for a2 controls
               if (snp_buffer_array[snp_buffer_idx][i2] == 2) {
                  if (pheno_array[i2] == 0.0) {
                     myallelescontrols = myallelescontrols + 2;
                  }
               }

               if (snp_buffer_array[snp_buffer_idx][i2] == 1) {
                  if (pheno_array[i2] == 0.0) {
                     myallelescontrols = myallelescontrols + 1;
                  }
               }
            }

            int totalalleles = sample_ct * 2;
            int totalallelescases = numberofcases * 2;
            int totalallelescontrols = numberofcontrols * 2;
            double af = myalleles / (double) totalalleles;
            double afcases = myallelescases / (double) totalallelescases;
            double afcontrols = myallelescontrols / (double) totalallelescontrols;
            afarray[snp_buffer_idx] = af;
            afarraycases[snp_buffer_idx] = afcases;
            afarraycontrols[snp_buffer_idx] = afcontrols;*/

            snp_buffer_idx++;
         }
      }

      // Logistic Regression for a block of SNPs
      if (logistic_regression_block(blk_size, &(snp_locus_array[i]), &(snp_buffer_array[i]), &pio_file, pheno_array, sex_array, sample_ct,
            report_file_sect_ptr(regression_report, i), &interactions, &covariates, inheritance, &(results[i]), config_flags)) {
#pragma omp atomic
         errors++;
      };
#pragma omp critical(progress)
      {
         show_progress(blk_size);
//			ShowTimeElapsed(config_flags, &time_diff, "Logistic regression block completed");
      }

      // Freeing up the buffers holding genotype data
      for (int j = i; j < i + blk_size; j++) {
         if (snp_buffer_array[j])
            my_mm_free(snp_buffer_array[j]);
         snp_buffer_array[j] = NULL;
      }
   };

   //VP: Flush report at the end
   if (regression_report)
      msync(regression_report, regression_report->file_sz, MS_ASYNC);

   if (close_report_file(regression_report)) {
      Error("Could not close the output file");
   }
   regression_report = NULL;

   ShowTimeElapsed(config_flags, &time_diff, "Logistic regression report completed");

   if (config_flags & CONF_MODEL_REPORT) {

      // Sorting the results based on the c-AIC information criterion value
      qsort(results, snp_ct, sizeof(struct lr_process_result*), compare_lr_results_aicc);

      if (report_file_name)
         free(report_file_name);
      report_file_name = (char*) malloc(sizeof(char) * (strlen(output_file_prefix) + strlen(LR_MODEL_REPORT_EXTENSION)) + 1);
      *report_file_name = '\0';
      strcat(report_file_name, output_file_prefix);
      strcat(report_file_name, LR_MODEL_REPORT_EXTENSION);

      io_report_file_t* model_report = open_report_file(report_file_name, snp_ct, 1, LR_REPORT_LINE_LENGTH, false);

      if (model_report) {
         reset_progress(snp_ct);

         double min_aicc = aicc(results[0]->model->loglik, results[0]->model->k, results[0]->model->n);

         for (int i = 0; i < snp_ct; i++) {

            char* dest_buffer = report_file_sect_ptr(model_report, i);
            int bytes_written = 0;
            double aicc_i = aicc(results[i]->model->loglik, results[i]->model->k, results[i]->model->n);

            bytes_written = sprintf(dest_buffer,
            LR_REPORT_CHR_NO_FMT LR_REPORT_SNP_ID_FMT LR_REPORT_POS_BP_FMT LR_REPORT_AL1_NM_FMT LR_REPORT_COV_NM_FMT LR_REPORT_SML_CT_FMT,
                  results[i]->chromosome, results[i]->snp_name, results[i]->bp_position, results[i]->allele1,
                  LR_REPORT_MODEL_SUMMARY_LINE_TEXT, results[i]->model->n);
            bytes_written += sprintf(dest_buffer + bytes_written,
            LR_REPORT_DEV_FMT LR_REPORT_CAIC_FMT LR_REPORT_BIC_FMT, aicc_i, aicc_i - min_aicc, 0.0);

            dest_buffer[bytes_written++] = LR_REPORT_NEW_LINE_CHR;
            assert(bytes_written == LR_REPORT_LINE_LENGTH);
         }

         if (close_report_file(model_report)) {
            Error("Could not close the output file");
         }
         model_report = NULL;
      }

   }

   ShowTimeElapsed(config_flags, &time_diff, "Model report completed");

   // De-allocating structures
   if (snp_buffer_array) {
      for (int i = 0; i < snp_ct; i++) {
         if (snp_buffer_array[i])
            my_mm_free(snp_buffer_array[i]);
      }
      my_mm_free(snp_buffer_array);
   }

   // De-allocating structures
   if (results) {
      for (int i = 0; i < snp_ct; i++) {
         if (results[i] == NULL)
            continue;
         if (free_lr_model(&(results[i]->model)) != STATUS_OK) {
            errors++;
         }
         if (results[i]->snp_name)
            free(results[i]->snp_name);
         if (results[i]->allele1)
            free(results[i]->allele1);
         if (results[i]->allele2)
            free(results[i]->allele2);
         my_mm_free(results[i]);
      }
      my_mm_free(results);
   }

   if (interactions.cov_name) {
      for (int i = 0; i < interactions.cov_ct; i++) {
         if (interactions.cov_name[i])
            my_mm_free(interactions.cov_name[i]);
      }
      my_mm_free(interactions.cov_name);
   }

   if (interactions.snp_name) {
      for (int i = 0; i < interactions.snp_ct; i++) {
         if (interactions.snp_name[i])
            my_mm_free(interactions.snp_name[i]);
         if (interactions.snp_locus[i])
            my_mm_free(interactions.snp_locus[i]);
         if (interactions.snp_data[i])
            my_mm_free(interactions.snp_data[i]);
      }
      my_mm_free(interactions.snp_name);
      my_mm_free(interactions.snp_locus);
      my_mm_free(interactions.snp_data);
   }

   if (snp_locus_array)
      my_mm_free(snp_locus_array);
   if (pheno_array)
      my_mm_free(pheno_array);
   if (sex_array)
      my_mm_free(sex_array);ShowTimeElapsed(config_flags, &time_diff, "All structures de-allocated");

   // Closing files
   cov_close(&cov_file);
   pio_close(&pio_file);
   ShowTimeElapsed(config_flags, &time_zero, "[CUMMULATIVE] Done");

   if (errors) {
      Error("Fatal errors occurred!");
      return STATUS_UNKNOWN_ERROR;
   } else {
      return STATUS_OK;
   }
}
