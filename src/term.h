/*
 * Copyright (C) 2016-2017 The Brigham and Women’s Hospital, Inc.
 * Portions contributed and copyright held by others as indicated.
 * All rights reserved.
 *
 * The contents of this file are subject to the terms of the BSD 3-clause "New" or "Revised"
 * License (the "License"). You may not use this file except in compliance with the License.
 *
 * You should have received a copy of the License in a separate text file originally named
 * "LICENSE_BWH.md"; if not, refer to "BSD-3-Clause" license at https://spdx.org/licenses/.
 */

#ifndef EFM_TERM_H
#define EFM_TERM_H

typedef enum {
    TERM_TYPE_UNKNOWN = 0,
    TERM_TYPE_SNP,
    TERM_TYPE_COVAR
} term_type_t;

typedef enum {
    TERM_INST_UNKNOWN = 0,
    TERM_INST_ADD_VAR,
    TERM_INST_REM_VAR,
    TERM_INST_ADD_INT,
} term_inst_t;

typedef enum {
    TERM_OP_NONE = 0,
    TERM_OP_MULT_NO_MARGINAL,
    TERM_OP_MULT_ADD_MARGINAL
} term_op_t;

typedef struct {
    term_inst_t inst;
    term_op_t op;
    term_type_t term1_type;
    char *term1_name;
    term_type_t term2_type;
    char *term2_name;
} term_spec_t;

#endif /* EFM_TERM_H */
