/*
 * Copyright (C) 2016-2017 The Brigham and Women’s Hospital, Inc.
 * Portions contributed and copyright held by others as indicated.
 * All rights reserved.
 *
 * The contents of this file are subject to the terms of the BSD 3-clause "New" or "Revised"
 * License (the "License"). You may not use this file except in compliance with the License.
 *
 * You should have received a copy of the License in a separate text file originally named
 * "LICENSE_BWH.md"; if not, refer to "BSD-3-Clause" license at https://spdx.org/licenses/.
 */

#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <sys/mman.h>

#include "common.h"
#include "io_plink.h"

static char *chr_code2string[27] = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19",
      "20", "21", "22", "X", "Y", "XY", "MT" };

static io_plink_status_t read_bim(plink_fileset_t *fileset, char* filename) {
   FILE *fp = NULL;
   int i = 0;

   if (!fileset || !filename || fileset->snp_ct || fileset->bim.locus_sz) {
      return PLINK_IO_PARAM_ERROR;
   }

   if (!(fp = fopen(filename, O_RDONLY))) {
      Error("Could not open file (errno=%d)", errno);
      return PLINK_IO_FS_ERROR;
   }

   char* chromosome;
   char* rs;
   float distance;
   size_t position;
   char allele_1; // by default minor allele
   char allele_2; // by default major allele

   //    while(fscanf(fp, "%s %s %s %s", ) == PLINK_BIM_NO_OF_COLS){
   //        fileset->bim.snps[i];
   //        Verbose("OK");
   //    }

   if (fclose(fp)) {
      Error("Could not close file (errno=%d)", errno);
      return PLINK_IO_FS_ERROR;
   }

   return PLINK_IO_STATUS_OK;
}

static io_plink_status_t read_fam() {
   return PLINK_IO_STATUS_OK;
}

io_plink_status_t open_plink_fileset(plink_fileset_t *fileset, char* filename_prefix) {
   /*
    report_file_t* file = NULL;
    int fd = -1;
    void* mmap_ptr = NULL;

    size_t page_size = sysconf(_SC_PAGE_SIZE);

    
    
    size_t report_file_size = line_length * (num_of_sections * lines_per_section + (header ? 1 : 0));
    
    if (!filename_prefix) return NULL;
    if (report_file_size < 1 || report_file_size > MAX_REPORT_FILE_SIZE) return NULL;
    
    if ((fd = open(file_name, O_RDWR | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)) == -1 ){
    Error("Could not create file (errno=%d)", errno);
    return NULL;
    }
    
    // memory-mapped file I/O
    mmap_ptr = (char*) mmap(NULL, report_file_size, PROT_WRITE, MAP_SHARED, fd, 0);
    if (mmap_ptr == MAP_FAILED){
    Error("Filesystem error: unsuccessful mmap(); errno=%d", errno);
    close(fd);
    return NULL;
    }
    
    file = (report_file_t*) calloc(1, sizeof(report_file_t));
    file->fd = fd;
    file->file_name = strdup(file_name);
    file->header = header;
    file->sect_ct = num_of_sections;
    file->lines_per_sect = lines_per_section;
    file->line_len = line_length;
    file->file_sz = report_file_size;
    file->mmap_ptr = mmap_ptr;

    return file;
    */
   return PLINK_IO_STATUS_OK;
}
