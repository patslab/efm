/*
 * Copyright (C) 2016-2017 The Brigham and Women’s Hospital, Inc.
 * Portions contributed and copyright held by others as indicated.
 * All rights reserved.
 *
 * The contents of this file are subject to the terms of the BSD 3-clause "New" or "Revised"
 * License (the "License"). You may not use this file except in compliance with the License.
 *
 * You should have received a copy of the License in a separate text file originally named
 * "LICENSE_BWH.md"; if not, refer to "BSD-3-Clause" license at https://spdx.org/licenses/.
 */

#ifndef EFM__COMMON_H
#define EFM__COMMON_H

#include <stdio.h>
#include <libgen.h>

#include "types.h"
#include "config.h"
#include "version.h"

#define PLINK_TRANSPOSED_BED_EXTENSION ".t"
#define DEFAULT_OUT_FILESET_PREFIX      EXECUTABLE_NAME

// Global configuration shared across the project
extern unsigned long int g_conf;
extern unsigned int blocksize;
extern char *g_plink_fileset_prefix;
extern char *g_beagle_fileset_prefix;
extern char *g_bgen_fileset_prefix;
extern char *g_vcf_fileset_prefix;
extern char *g_gzipped_vcf_fileset_prefix;
extern char *g_aux_input_file_name;
extern char *g_plink_fileset_extensions[];
extern char *g_beagle_fileset_extensions[];
extern char *g_bgen_fileset_extensions[];
extern char *g_vcf_fileset_extensions[];
extern char *g_gzipped_vcf_fileset_extensions[];
extern char *g_covar_file_name;
extern char *g_output_fileset_prefix;
extern char *g_new_covar_spec;
extern char *g_interaction_spec;
extern char *g_extra_snps_as_covars;
extern size_t g_thread_number;
extern float g_aic_threshold;
extern char *g_covar_list;

// Enabling assert() for debug builds only
#ifdef DEBUG
#undef NDEBUG
#include <assert.h>
#else
#define NDEBUG
#include <assert.h>
#endif

#ifdef DEBUG
// IN MAGENTA
#define Debug(format, ...)		if(g_conf & CONF_DEBUG) fprintf(stderr, "\033[35mDEBUG: " format " (%s:%d)\033[0m\n",  ##__VA_ARGS__, basename(__FILE__), __LINE__)
#else
#define Debug(format, ...)
#endif
// IN YELLOW
#define Warning(format, ...)    fprintf(stderr, "\033[33mWARNING: " format "\033[0m\n",  ##__VA_ARGS__)
// IN RED
#define Error(format, ...)		fprintf(stderr, "\033[31mERROR: " format "\033[0m\n",  ##__VA_ARGS__)
// IN NAVY
#define Verbose(format, ...)	if(g_conf & CONF_VERBOSE) fprintf(stderr, "\033[34mVERBOSE: " format "\033[0m\n",  ##__VA_ARGS__)
#define Info(format, ...)		fprintf(stdout, format "\n", ##__VA_ARGS__)

#ifdef USE_OPEN_MP
#include <omp.h>
#define ClearTimeElapsed(config, time_stamp)		if(config & CONF_VERBOSE) do { *time_stamp=omp_get_wtime(); } while(0)
#define ShowTimeElapsed(config, time_stamp, msg)	if(config & CONF_VERBOSE) do { Verbose("%s in %.2f sec.", msg, omp_get_wtime() - *time_stamp); *time_stamp=omp_get_wtime(); } while(0)
#else
#define ClearTimeElapsed(config, time_stamp)
#define ShowTimeElapsed(config, time_stamp, msg)
#endif

#endif /* EFM__COMMON_H */
