/*
 * Copyright (C) 2016-2017 The Brigham and Women’s Hospital, Inc.
 * Portions contributed and copyright held by others as indicated.
 * All rights reserved.
 *
 * The contents of this file are subject to the terms of the BSD 3-clause "New" or "Revised"
 * License (the "License"). You may not use this file except in compliance with the License.
 *
 * You should have received a copy of the License in a separate text file originally named
 * "LICENSE_BWH.md"; if not, refer to "BSD-3-Clause" license at https://spdx.org/licenses/.
 */

#ifndef EFM__LR_PROCESS_H
#define EFM__LR_PROCESS_H

#include <plinkio/plinkio.h>
#include "common.h"
#include "lr_model.h"

#define LR_EXPOSURE_NUM_PARAMS  1
#define LR_INTERCEPT_NUM_PARAMS 1

#if (LR_EXPOSURE_NUM_PARAMS != 1)
#error "Unsupported number of exposure variables"
#endif

#if (LR_INTERCEPT_NUM_PARAMS != 1)
#error "Unsupported number of intercept variables"
#endif

#define LR_REGRESS_REPORT_EXTENSION         ".regress"
#define LR_MODEL_REPORT_EXTENSION           ".model"
#define LR_TERMS_SPEC_EXTENSION             ".terms"

#define LR_DEFAULT_BLOCK_SIZE               256//VP: Original 256

#define LR_COVAR_FILE_SAMPLE_ID_NUM_COLUMNS 2
#define LR_COV_SMPL_MATCHING_IDX_BASE       1

#define LR_REPORT_SUMMARY_LINES             1
#define LR_REPORT_SUMMARY_LINE_TEXT         "DEV/AICc/BIC"
#define LR_REPORT_MODEL_SUMMARY_LINE_TEXT   "Ac/dAc/dRefAc"
#define LR_REPORT_EMPTY_LINE_FMT            "%145s"
#define LR_REPORT_EMPTY_LINE_STR            "*"

#define LR_REPORT_HDR_CHR_FMT               "%4s"
#define LR_REPORT_HDR_VAL_FMT               "%16s"
#define LR_REPORT_HDR_SML_FMT               "%11s"
#define LR_REPORT_HDR_CHR_STR               "CHR"
#define LR_REPORT_HDR_SNP_ID                "SNP"
#define LR_REPORT_HDR_POS_FMT               "%11s"
#define LR_REPORT_HDR_POS_STR               "BP"
#define LR_REPORT_HDR_AL1_STR               "A1"
#define LR_REPORT_HDR_COV_STR               "TEST"
#define LR_REPORT_HDR_SML_STR               "NMISS"
#define LR_REPORT_HDR_OR_STR                "OR"
#define LR_REPORT_HDR_ZV_STR                "STAT"
#define LR_REPORT_HDR_PV_STR                "P"

#define LR_REPORT_CHR_NO_FMT                "%4d"
#define LR_REPORT_SNP_ID_FMT                "%24s"
#define LR_REPORT_POS_BP_FMT                "%11llu"
#define LR_REPORT_AL1_NM_FMT                "%4s"
#define LR_REPORT_COV_NM_FMT                "%43s"
#define LR_REPORT_SML_CT_FMT                " %10zu"
#define LR_REPORT_OR_FMT                    " %15.6Lg"
#define LR_REPORT_ZV_FMT                    " %15.6Lg"
#define LR_REPORT_PV_FMT                    " %15.6Lg"
#define LR_REPORT_NLL_FMT                   " %15.6g"
#define LR_REPORT_DEV_FMT                   " %15.6g"
#define LR_REPORT_AIC_FMT                   " %15.6g"
#define LR_REPORT_CAIC_FMT                  " %15.6g"
#define LR_REPORT_BIC_FMT                   " %15.6g"
#define LR_REPORT_ERROR_FMT                 "    %45s%10zu"
#define LR_REPORT_ERROR_STR                 "Error: "
#define LR_REPORT_NA_STR                    "              NA"
#define LR_REPORT_ADD_STR                   "        Additive"
#define LR_REPORT_REC_STR                   "       Recessive"
#define LR_REPORT_DOM_STR                   "        Dominant"

#define LR_REPORT_NEW_LINE_CHR              '\n'
#define LR_REPORT_LINE_LENGTH               146

#define LR_TERM_FILE_MAX_LINE_LEN           256
#define LR_TERM_FILE_FIELD_DELIM            " \t"
#define LR_TERM_FILE_NEW_LINE_DELIM         "\n"

#define LR_TERM_SPEC_INST_ADD_VAR_SYMB      "+"
#define LR_TERM_SPEC_INST_REM_VAR_SYMB      "-"
#define LR_TERM_SPEC_INST_ADD_INT_SYMB      "%"
#define LR_TERM_SPEC_OP_MULT_ADD_MARG_SYMB  "*"
#define LR_TERM_SPEC_OP_MULT_NO_MARG_SYMB   ":"
#define LT_TERM_INTERACTION_SYMBOL          "*"

typedef struct {
    unsigned char chromosome;
    long long bp_position;
    char *snp_name;
    char *allele1;
    char *allele2;
    lr_model_t *model;
    STATUS status;
} lr_process_result_t;

typedef struct {
    term_spec_t **term_spec;
    size_t term_spec_ct;
    size_t term_spec_sz;
} lr_term_spec_list_t;

//VP
typedef struct {
    char *fid;
    char *iid;
    int position;
} hash_record;


int logistic_regression_block(size_t block_size, struct pio_locus_t** snp_locus_block, float** snp_buffer_block, struct pio_file_t* pio_file, alg_float_ptr snp_phenotype, alg_int_ptr sex_array, size_t sample_ct, char* outfile_ptr, lr_terms_t *interactions, lr_terms_t *covariates, inheritance_mode_t inheritance, lr_process_result_t** block_results, unsigned long int config_flags);

//BGEN_VCF:1
int logistic_regression_process(const char* plink_file_prefix,const char* bgen_vcf_filename,const char* bgen_vcf_sample_filename, const char* covar_file_name, const char* input_file_name, const char* output_file_prefix, unsigned long int config_flags, size_t num_of_threads, const char* interaction_str, const char* snp_covars_str, const char* covar_list, const char* new_covar_str);

#endif /* EFM__LR_PROCESS_H */
