/*
 * Copyright (C) 2016-2017 The Brigham and Women’s Hospital, Inc.
 * Portions contributed and copyright held by others as indicated.
 * All rights reserved.
 *
 * The contents of this file are subject to the terms of the BSD 3-clause "New" or "Revised"
 * License (the "License"). You may not use this file except in compliance with the License.
 *
 * You should have received a copy of the License in a separate text file originally named
 * "LICENSE_BWH.md"; if not, refer to "BSD-3-Clause" license at https://spdx.org/licenses/.
 */

#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <math.h>
#include <string.h>
#include <errno.h>

#include "main.h"
#include "common.h"
#include "interface.h"
#include "lr_process.h"
#include "grs_process.h"

#include "bgen/bgen.h"
#include "bgen_functions.h"

/**
 Checking the system for compatibility
 @return On success: 0; number of errors found otherwise
 */
static int system_check() {
   int retval = 0;

   // Checking if assert() is truly disabled in the release version (assertions shall not be effective in production version)
#ifndef DEBUG
   assert(0);
#endif

   // Checking nanf()
   float f = nanf("");
   if (isnan(f) == 0) {
      retval++;
   }

   // Checking char type
   if (sizeof(char) != 1) {
      retval++;
   }

   return retval;
}

/**
 Main function

 @param argc number of arguments
 @param argv arguments

 @return On success: 0 and non-zero value otherwise
 */
int main(int argc, char** argv) {

   struct stat file_stat;
   char file_name[MAX_FILENAME_LENGTH];
   char** exts = NULL;
   int idx = 0;

   // System check
   if (system_check()) {
      Error("Fatal error: unsupported operating system and/or libraries (code %d). Quitting now.", system_check());
      return EXIT_FAILURE;
   }

   // Determine the number of threads based on the number of logical CPUs available
   g_thread_number = sysconf(_SC_NPROCESSORS_ONLN);

   // Processing command line arguments
   process_command_line(argc, argv);

   // Version info
   if (g_conf & CONF_VERSION) {
      print_version();
   }

   // Usage info
   if (g_conf & CONF_HELP) {
      print_usage();
   }

   // Exit in case of issues
   if ((g_conf & CONF_HELP) || (g_conf & CONF_VERSION) || (g_conf & CONF_FATAL_ERROR)) {
      if (g_conf & CONF_FATAL_ERROR)
         return EXIT_FAILURE;
      else
         return EXIT_SUCCESS;
   }

   if (g_thread_number > MAX_NUMBER_OF_THREADS)
      g_thread_number = MAX_NUMBER_OF_THREADS;

   Verbose("Detected %zu logical processors. Using %zu threads.", sysconf(_SC_NPROCESSORS_ONLN), g_thread_number);

   // Checking if file-set is readable, if specified
   if (g_conf & CONF_FILESET) {

      idx = 0;
      char *fileset_prefix = NULL;

      if (g_conf & CONF_INPUT_BEAGLE) {
         fileset_prefix = g_beagle_fileset_prefix;
         exts = g_beagle_fileset_extensions;
      } else if (g_conf & CONF_INPUT_PLINK) {
         fileset_prefix = g_plink_fileset_prefix;
         exts = g_plink_fileset_extensions;
      } else if (g_conf & CONF_INPUT_BGEN) {
         fileset_prefix = g_bgen_fileset_prefix;
         exts = g_bgen_fileset_extensions;
      } else if (g_conf & CONF_INPUT_VCF) {
         fileset_prefix = g_vcf_fileset_prefix;
         exts = g_vcf_fileset_extensions;
      } else if (g_conf & CONF_INPUT_VCF_GZIPPED) {
         fileset_prefix = g_gzipped_vcf_fileset_prefix;
         exts = g_gzipped_vcf_fileset_extensions;
      } else {
         Error("Unknown input file-set. Quitting now.");
         return EXIT_FAILURE;
      }

      while (exts[idx]) {
         strcpy(file_name, fileset_prefix);
         strcat(file_name, exts[idx++]);

         if (stat(file_name, &file_stat) || !S_ISREG(file_stat.st_mode)) {
            Error("Could not read file '%s'. Quitting now.", file_name);
            return EXIT_FAILURE;
         }
      }

      idx = 0;
   }

   // Checking if covariate-file is readable, if specified
   if (g_conf & CONF_COVARFILE) {
      if (stat(g_covar_file_name, &file_stat) || !S_ISREG(file_stat.st_mode)) {
         Error("Could not read covariate file '%s'. Quitting now.", g_covar_file_name);
         return EXIT_FAILURE;
      }
   }

   // Checking if output file-set is defined, doesn't exists yet and is writable
   if (g_conf & CONF_LOGISTIC) {
      strcpy(file_name, g_output_fileset_prefix);
      strcat(file_name, LR_REGRESS_REPORT_EXTENSION);

      if (strlen(file_name)) {
         if (stat(file_name, &file_stat)) {
            if (errno != ENOENT) {
               Error("Unknown filesystem error (errno=%d). Quitting now.", errno);
               return EXIT_FAILURE;
            }
         } else {
            Warning("Output file '%s' already exists. Overwriting it.", file_name);
         }
      } else {
         Error("Invalid output file specified. Quitting now.");
         return EXIT_FAILURE;
      }

      if (g_conf & CONF_MODEL_REPORT) {
         strcpy(file_name, g_output_fileset_prefix);
         strcat(file_name, LR_MODEL_REPORT_EXTENSION);
         if (strlen(file_name)) {
            if (stat(file_name, &file_stat)) {
               if (errno != ENOENT) {
                  Error("Unknown filesystem error (errno=%d). Quitting now.", errno);
                  return EXIT_FAILURE;
               }
            } else {
               Warning("Output file '%s' already exists. Overwriting it.", file_name);
            }
         } else {
            Error("Invalid output file specified. Quitting now.");
            return EXIT_FAILURE;
         }
      }
   }

   // Selecting the default model of inheritance unless specific one was requested
   if (!(g_conf & (CONF_INH_ADD | CONF_INH_DOM | CONF_INH_REC))) {
      Debug("No specific model of inheritance requested (additive selected by default)");
      g_conf |= CONF_INH_ADD;
   }

   // Running analysis
   switch (g_conf & (CONF_TRANSPOSE | CONF_LOGISTIC | CONF_GRS)) {

   case CONF_TRANSPOSE:
      // Checking if the output PLINK file-set doesn't exist
      exts = g_plink_fileset_extensions;
      idx = 0;
      while (exts[idx]) {
         strcpy(file_name, g_plink_fileset_prefix);
         strcat(file_name, PLINK_TRANSPOSED_BED_EXTENSION);
         strcat(file_name, exts[idx++]);
         if (stat(file_name, &file_stat)) {
            if (errno != ENOENT) {
               Error("Unknown filesystem error (errno=%d). Quitting now.", errno);
               return EXIT_FAILURE;
            }
         } else {
            Warning("Output file '%s' already exists. Overwriting it.", file_name);
         }
      }
      if (pio_transpose(g_plink_fileset_prefix, strcat(strcpy(file_name, g_plink_fileset_prefix),
      PLINK_TRANSPOSED_BED_EXTENSION)) != PIO_OK) {
         Debug("Could not transpose PLINK file-set %s into %s. Quitting now.", g_plink_fileset_prefix, file_name);
         return EXIT_FAILURE;
      } else {
         Debug("Transposed PLINK file-set with prefix '%s' into '%s'", g_plink_fileset_prefix, file_name);
         return EXIT_SUCCESS;
      }
      break;
   case CONF_LOGISTIC:
      // Checking if the input file-set was specified
      if (!(g_conf & CONF_FILESET)) {
         Info("Missing input file-set. Quitting now.");
         return EXIT_FAILURE;
      }

      // Checking if input file is readable, if specified
      if (g_conf & CONF_AUX_INFILE) {
         if (stat(g_aux_input_file_name, &file_stat) || !S_ISREG(file_stat.st_mode)) {
            Error("Could not read input file '%s'. Quitting now.", g_aux_input_file_name);
            return EXIT_FAILURE;
         }
      }

      //TODO (VP): This can be removed since we do have those from a previous step.
      char* fileset_prefix = NULL;
      char* bgen_vcf_file = NULL;
      char* bgen_vcf_sample = NULL;

      if (g_conf & CONF_INPUT_BEAGLE) {
         fileset_prefix = g_beagle_fileset_prefix;
      } else if (g_conf & CONF_INPUT_PLINK) {
         fileset_prefix = g_plink_fileset_prefix;
      } else if (g_conf & CONF_INPUT_BGEN) {
         fileset_prefix = g_bgen_fileset_prefix;
         bgen_vcf_file = strdup(fileset_prefix);
         strcat(bgen_vcf_file,".bgen");
      } else if (g_conf & CONF_INPUT_VCF) {
         fileset_prefix = g_vcf_fileset_prefix;
         bgen_vcf_file = strdup(fileset_prefix);
         strcat(bgen_vcf_file,".vcf");
         strcat(bgen_vcf_sample,".sample");
      } else if (g_conf & CONF_INPUT_VCF_GZIPPED) {
         fileset_prefix = g_gzipped_vcf_fileset_prefix;
         strcat(bgen_vcf_file,".vcf.gz");
         strcat(bgen_vcf_sample,".sample");
      } else {
         Error("Unknown input file-set. Quitting now.");
         return EXIT_FAILURE;
      }

      Debug("Logistic Regression [PLINK file-set prefix: %s; Output file-set prefix: %s]", fileset_prefix, g_output_fileset_prefix); //BGEN_VCF: 1
      Verbose("Output file-set prefix: \'%s\'.", g_output_fileset_prefix);

      if (logistic_regression_process(fileset_prefix,bgen_vcf_file ,bgen_vcf_sample, g_covar_file_name, g_aux_input_file_name, g_output_fileset_prefix, g_conf, g_thread_number,
            g_interaction_spec, g_extra_snps_as_covars, g_covar_list, g_new_covar_spec) != STATUS_OK) {
         Info("Logistic regression process failed.");
         return EXIT_FAILURE;
      }

      break;
   case CONF_GRS:
      // Checking if the input PLINK file-set was specified
      if (!(g_conf & CONF_FILESET)) {
         Info("Missing input PLINK file-set. Quitting now.");
         return EXIT_FAILURE;
      }

      // Checking if the input file was specified
      if (!(g_conf & CONF_AUX_INFILE)) {
         Info("Missing input file. Quitting now.");
         return EXIT_FAILURE;
      }

      // Checking if input file is readable, if specified
      if (g_conf & CONF_AUX_INFILE) {
         if (stat(g_aux_input_file_name, &file_stat) || !S_ISREG(file_stat.st_mode)) {
            Error("Could not read input file '%s'. Quitting now.", g_aux_input_file_name);
            return EXIT_FAILURE;
         }
      }

      Verbose("Output file-set prefix: \'%s\'.", g_output_fileset_prefix);
      if (grs_process(g_plink_fileset_prefix, g_aux_input_file_name, g_output_fileset_prefix, g_thread_number, g_conf) != STATUS_OK) {
         Info("Genetic Risk Score process failed.");
         return EXIT_FAILURE;
      }
      break;
   default:
      Info("No function or conflicting functions specified. Quitting now.");
      return EXIT_FAILURE;
      break;
   }

   return EXIT_SUCCESS;
}
