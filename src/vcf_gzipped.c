/*
 * Copyright (C) 2016-2017 The Brigham and Women’s Hospital, Inc.
 * Portions contributed and copyright held by others as indicated.
 * All rights reserved.
 *
 * The contents of this file are subject to the terms of the BSD 3-clause "New" or "Revised"
 * License (the "License"). You may not use this file except in compliance with the License.
 *
 * You should have received a copy of the License in a separate text file originally named
 * "LICENSE_BWH.md"; if not, refer to "BSD-3-Clause" license at https://spdx.org/licenses/.
 */

#include "vcf_gzipped.h"

void append(char* s, char c) {
        int len = strlen(s);
        s[len] = c;
        s[len+1] = '\0';
}

char* appendCharToCharArray(char* array, char a)
{
    size_t len = strlen(array);

    char* ret[len+2];

    strcpy(ret, array);
    ret[len] = a;
    ret[len+1] = '\0';

    return ret;
}

int vcf_gz_getsnps(char const* fileName)
{


    gzFile * file;

    file = gzopen (fileName, "r");
    if (! file) {
        fprintf (stderr, "gzopen of '%s' failed: %s.\n", fileName,
                 strerror (errno));
            exit (EXIT_FAILURE);
    }



    char line[10000];
    int linecnt = 0;

    while (gzgets(file, line, 10000)) {
        /* note that fgets don't strip the terminating \n, checking its
           presence would allow to handle lines longer that sizeof(line) */

		


		// find number of columns = linecnt + 1
		if (line[0] != '#')
		{




			linecnt = linecnt + 1;
			//printf("snps = %d\n", linecnt);
			






		}
    }
    /* may check feof here to make a difference between eof and io failure -- network
       timeout for instance */



	//printf("Number of SNPs = %d\n", linecnt);
	return linecnt;
	//scanf("%d", &keep1);

}


int vcf_gz_getsamples(char const* fileName)
{

    gzFile * file;

    file = gzopen (fileName, "r");
    if (! file) {
        fprintf (stderr, "gzopen of '%s' failed: %s.\n", fileName,
                 strerror (errno));
            exit (EXIT_FAILURE);
    }




    char line[10000];
    int linecnt = 0;

    while (gzgets(file, line, 10000)) {
        /* note that fgets don't strip the terminating \n, checking its
           presence would allow to handle lines longer that sizeof(line) */

		int linecnt = 0;


		// find number of columns = linecnt + 1
		if (line[0] != '#')
		{



			// calculation of columns
			for (int i = 0; i < sizeof(line); i++)
			{
				//printf("%c\n", line[i]);
				if (line[i] == '\t')
				{
					if (((line[i+1] == '0') || (line[i+1] == '1')) && (line[i+2] == '|'))
					{
						linecnt++;
						//printf("%d %c\n", linecnt, line[i+1]);
					}
				}


			}

			


			//printf("Number of samples: %d\n", linecnt);
			return linecnt;
			break;
		}




	}

    return linecnt;

}

char* vcf_gz_getallele1(char const* fileName, int index)
{


    gzFile * file;

    file = gzopen (fileName, "r");
    if (! file) {
        fprintf (stderr, "gzopen of '%s' failed: %s.\n", fileName,
                 strerror (errno));
            exit (EXIT_FAILURE);
    }




    char line[10000];
    char* output[50];
    int linecnt = 0;
    int totallines = 0;
    char* output2;



    while (gzgets(file, line, 10000)) {
        /* note that fgets don't strip the terminating \n, checking its
           presence would allow to handle lines longer that sizeof(line) */

		int linecnt = 0;

		int keeplinecnt = 0;
		int keepnumber = 0;
		int keepnumberb = 0;
		int keepnumber2 = 0;
		int keepnumberid = 0;

		// find number of columns = linecnt + 1
		if (line[0] != '#')
		{
	
			totallines = totallines + 1;

			if (totallines == index)
			{

				//printf("line: %d\n", totallines);
				//Verbose("line: %d", totallines);

				// calculation of columns
				for (int i = 0; i < sizeof(line); i++)
				{
					//printf("%c\n", line[i]);
					if (line[i] == '\t')
					{
						if (((line[i+1] == '0') || (line[i+1] == '1')) && (line[i+2] == '|'))
						{
							linecnt++;
							//printf("%d %c\n", linecnt, line[i+1]);
						}
					}


				}

			

				linecnt = linecnt + 9;
				//Verbose("Number of columns: %d", linecnt);
				//linecnt = 191;
				keeplinecnt = linecnt;



				// find allele1 column
				linecnt = 0;
				for (int i = 0; i < sizeof(line); i++)
				{
					//printf("%c\n", line[i]);
					if (line[i] == '\t')
					{
						linecnt++;
					}

					if (linecnt == 3)
					{
						keepnumber = i;
						break;
					}

				}


				// printouts


				//Verbose("Allele1: %c", line[keepnumber+1]);
				int arrayindex = 0;
				//Verbose("arrayindex: %c", line[keepnumber+1]);
				output[arrayindex] = line[keepnumber+1];
				//append(output, line[keepnumber+1]);
				//char dest = line[keepnumber+1];
				//Verbose("dest: %c", dest);
				//appendCharToCharArray(output, line[keepnumber+1]);
				//Verbose("output: %s", output);


				int cont = 1;
				while (line[keepnumber+1+cont] != '\t')
				{
					//Verbose("%c", line[keepnumber+1+cont]);
					arrayindex++;
					output[arrayindex] = line[keepnumber+1+cont];
					//append(output, line[keepnumber+1+cont]);
					//appendCharToCharArray(output, line[keepnumber+1+cont]);
					//arrayindex++;
					cont++;
				}
				//printf("\n");
				arrayindex++;
				output[arrayindex] = '\0';

				//Verbose("output all1: %s", output);




				output2 = (char *) malloc(sizeof(char) * arrayindex);//[len+1];
				arrayindex = 0;
				char ch = line[keepnumber+1];
				output2[arrayindex] = ch;
				arrayindex++;

				cont = 1;
				while (line[keepnumber+1+cont] != '\t')
				{
					//Verbose("%c", line[keepnumber+1+cont]);
					//arrayindex++;
					ch = line[keepnumber+1+cont];
					output2[arrayindex] = ch;
					arrayindex++;
					//append(output, line[keepnumber+1+cont]);
					//appendCharToCharArray(output, line[keepnumber+1+cont]);
					//arrayindex++;
					cont++;
				}

				output2[arrayindex] = '\0';





				int individuals = keeplinecnt-9;
				//printf("Number of individuals: %d\n", individuals);
			}

		}



	}


	return output2;




}





char* vcf_gz_getallele2(char const* fileName, int index)
{


    gzFile * file;

    file = gzopen (fileName, "r");
    if (! file) {
        fprintf (stderr, "gzopen of '%s' failed: %s.\n", fileName,
                 strerror (errno));
            exit (EXIT_FAILURE);
    }




    char line[10000];
    char* output[20];
    int linecnt = 0;
    int totallines = 0;
    char* output2;



    while (gzgets(file, line, 10000)) {
        /* note that fgets don't strip the terminating \n, checking its
           presence would allow to handle lines longer that sizeof(line) */

		int linecnt = 0;

		int keeplinecnt = 0;
		int keepnumber = 0;
		int keepnumberb = 0;
		int keepnumber2 = 0;
		int keepnumberid = 0;

		// find number of columns = linecnt + 1
		if (line[0] != '#')
		{
	
			totallines = totallines + 1;

			if (totallines == index)
			{

				//printf("line: %d\n", totallines);

				// calculation of columns
				for (int i = 0; i < sizeof(line); i++)
				{
					//printf("%c\n", line[i]);
					if (line[i] == '\t')
					{
						if (((line[i+1] == '0') || (line[i+1] == '1')) && (line[i+2] == '|'))
						{
							linecnt++;
							//printf("%d %c\n", linecnt, line[i+1]);
						}
					}


				}

			

				linecnt = linecnt + 9;
				//printf("Number of columns: %d\n", linecnt);
				//linecnt = 191;
				keeplinecnt = linecnt;



				// find allele2 column
				linecnt = 0;
				for (int i = 0; i < sizeof(line); i++)
				{
					//printf("%c\n", line[i]);
					if (line[i] == '\t')
					{
						linecnt++;
					}

					if (linecnt == 4)
					{
						keepnumber = i;
						break;
					}

				}


				// printouts


				//Verbose("Allele2: %c", line[keepnumber+1]);
				int arrayindex = 0;
				output[arrayindex] = line[keepnumber+1];
				//printf("output: %s\n", output);
				//arrayindex++;
				int cont = 1;
				while (line[keepnumber+1+cont] != '\t')
				{
					//Verbose("%c", line[keepnumber+1+cont]);
					//output[arrayindex] = line[keepnumber+1+cont];
					appendCharToCharArray(output, line[keepnumber+1+cont]);
					//arrayindex++;
					cont++;
				}
				//printf("\n");
				//output[arrayindex] = '\0';
				//Verbose("output all2: %s", output);



				output2 = (char *) malloc(sizeof(char) * arrayindex);//[len+1];
				arrayindex = 0;
				char ch = line[keepnumber+1];
				output2[arrayindex] = ch;
				arrayindex++;

				cont = 1;
				while (line[keepnumber+1+cont] != '\t')
				{
					//Verbose("%c", line[keepnumber+1+cont]);
					//arrayindex++;
					ch = line[keepnumber+1+cont];
					output2[arrayindex] = ch;
					arrayindex++;
					//append(output, line[keepnumber+1+cont]);
					//appendCharToCharArray(output, line[keepnumber+1+cont]);
					//arrayindex++;
					cont++;
				}

				output2[arrayindex] = '\0';



				int individuals = keeplinecnt-9;
				//printf("Number of individuals: %d\n", individuals);
			}

		}



	}


	return output2;




}




char* vcf_gz_getid(char const* fileName, int index)
{


    gzFile * file;

    file = gzopen (fileName, "r");
    if (! file) {
        fprintf (stderr, "gzopen of '%s' failed: %s.\n", fileName,
                 strerror (errno));
            exit (EXIT_FAILURE);
    }




    char line[10000];
    char* output[50];
    int linecnt = 0;
    int totallines = 0;



    while (gzgets(file, line, 10000)) {
        /* note that fgets don't strip the terminating \n, checking its
           presence would allow to handle lines longer that sizeof(line) */

		int linecnt = 0;

		int keeplinecnt = 0;
		int keepnumber = 0;
		int keepnumberb = 0;
		int keepnumber2 = 0;
		int keepnumberid = 0;

		// find number of columns = linecnt + 1
		if (line[0] != '#')
		{
	
			totallines = totallines + 1;

			if (totallines == index)
			{

				//printf("line: %d\n", totallines);

				// calculation of columns
				for (int i = 0; i < sizeof(line); i++)
				{
					//printf("%c\n", line[i]);
					if (line[i] == '\t')
					{
						if (((line[i+1] == '0') || (line[i+1] == '1')) && (line[i+2] == '|'))
						{
							linecnt++;
							//printf("%d %c\n", linecnt, line[i+1]);
						}
					}


				}

			

				linecnt = linecnt + 9;
				//printf("Number of columns: %d\n", linecnt);
				//linecnt = 191;
				keeplinecnt = linecnt;



				// find allele2 column
				linecnt = 0;
				for (int i = 0; i < sizeof(line); i++)
				{
					//printf("%c\n", line[i]);
					if (line[i] == '\t')
					{
						linecnt++;
					}

					if (linecnt == 2)
					{
						keepnumber = i;
						break;
					}

				}


				// printouts


				//Verbose("ID: %c", line[keepnumber+1]);
				int arrayindex = 0;
				output[arrayindex] = line[keepnumber+1];
				//append(output, line[keepnumber+1]);
				//Verbose("output: %s", output);
				//arrayindex++;
				int cont = 1;
				while (line[keepnumber+1+cont] != '\t')
				{
					//Verbose("IDinside: %c", line[keepnumber+1+cont]);
					//output[arrayindex] = line[keepnumber+1+cont];
					append(output, line[keepnumber+1+cont]);
					//Verbose("output: %s", output);
					//arrayindex++;
					cont++;
				}
				//printf("\n");
				//output[arrayindex] = '\0';
				//Verbose("arrayindex: %d", arrayindex);
				//Verbose("output: %s\n", output);

				//return output;


				int individuals = keeplinecnt-9;
				//printf("Number of individuals: %d\n", individuals);
			}

		}



	}


	return output;




}



char* vcf_gz_getpos(char const* fileName, int index)
{


    gzFile * file;

    file = gzopen (fileName, "r");
    if (! file) {
        fprintf (stderr, "gzopen of '%s' failed: %s.\n", fileName,
                 strerror (errno));
            exit (EXIT_FAILURE);
    }




    char line[10000];
    char* output[50];
    int linecnt = 0;
    int totallines = 0;



    while (gzgets(file, line, 10000)) {
        /* note that fgets don't strip the terminating \n, checking its
           presence would allow to handle lines longer that sizeof(line) */

		int linecnt = 0;

		int keeplinecnt = 0;
		int keepnumber = 0;
		int keepnumberb = 0;
		int keepnumber2 = 0;
		int keepnumberid = 0;

		// find number of columns = linecnt + 1
		if (line[0] != '#')
		{
	
			totallines = totallines + 1;

			if (totallines == index)
			{

				//printf("line: %d\n", totallines);

				// calculation of columns
				for (int i = 0; i < sizeof(line); i++)
				{
					//printf("%c\n", line[i]);
					if (line[i] == '\t')
					{
						if (((line[i+1] == '0') || (line[i+1] == '1')) && (line[i+2] == '|'))
						{
							linecnt++;
							//printf("%d %c\n", linecnt, line[i+1]);
						}
					}


				}

			

				linecnt = linecnt + 9;
				//printf("Number of columns: %d\n", linecnt);
				//linecnt = 191;
				keeplinecnt = linecnt;



				// find allele2 column
				linecnt = 0;
				for (int i = 0; i < sizeof(line); i++)
				{
					//printf("%c\n", line[i]);
					if (line[i] == '\t')
					{
						linecnt++;
					}

					if (linecnt == 1)
					{
						keepnumber = i;
						break;
					}

				}


				// printouts


				//printf("Allele2: %c", line[keepnumber+1]);
				int arrayindex = 0;
				output[arrayindex] = line[keepnumber+1];
				//printf("output: %s\n", output);
				//arrayindex++;
				int cont = 1;
				while (line[keepnumber+1+cont] != '\t')
				{
					//printf("%c", line[keepnumber+1+cont]);
					//output[arrayindex] = line[keepnumber+1+cont];
					append(output, line[keepnumber+1+cont]);
					//arrayindex++;
					cont++;
				}
				//printf("\n");
				//output[arrayindex] = '\0';
				//printf("output: %s\n", output);




				int individuals = keeplinecnt-9;
				//printf("Number of individuals: %d\n", individuals);
			}

		}



	}


	return output;




}



char* vcf_gz_getchromosome(char const* fileName, int index)
{


    gzFile * file;

    file = gzopen (fileName, "r");
    if (! file) {
        fprintf (stderr, "gzopen of '%s' failed: %s.\n", fileName,
                 strerror (errno));
            exit (EXIT_FAILURE);
    }




    char line[10000];
    char* output[50];
    int linecnt = 0;
    int totallines = 0;



    while (gzgets(file, line, 10000)) {
        /* note that fgets don't strip the terminating \n, checking its
           presence would allow to handle lines longer that sizeof(line) */

		int linecnt = 0;

		int keeplinecnt = 0;
		int keepnumber = 0;
		int keepnumberb = 0;
		int keepnumber2 = 0;
		int keepnumberid = 0;

		// find number of columns = linecnt + 1
		if (line[0] != '#')
		{
	
			totallines = totallines + 1;

			if (totallines == index)
			{

				//printf("line: %d\n", totallines);

				// calculation of columns
				for (int i = 0; i < sizeof(line); i++)
				{
					//printf("%c\n", line[i]);
					if (line[i] == '\t')
					{
						if (((line[i+1] == '0') || (line[i+1] == '1')) && (line[i+2] == '|'))
						{
							linecnt++;
							//printf("%d %c\n", linecnt, line[i+1]);
						}
					}


				}

			

				linecnt = linecnt + 9;
				//printf("Number of columns: %d\n", linecnt);
				//linecnt = 191;
				keeplinecnt = linecnt;



				// find allele2 column
				linecnt = 0;
				keepnumber = -1;
				/*
				for (int i = 0; i < sizeof(line); i++)
				{
					//printf("%c\n", line[i]);
					if (line[i] == '\t')
					{
						linecnt++;
					}

					if (linecnt == 1)
					{
						keepnumber = i;
						break;
					}

				}
				*/


				// printouts


				//printf("Allele2: %c", line[keepnumber+1]);
				int arrayindex = 0;
				output[arrayindex] = line[keepnumber+1];
				//printf("output: %s\n", output);
				//arrayindex++;
				int cont = 1;
				while (line[keepnumber+1+cont] != '\t')
				{
					//printf("%c", line[keepnumber+1+cont]);
					//output[arrayindex] = line[keepnumber+1+cont];
					append(output, line[keepnumber+1+cont]);
					//arrayindex++;
					cont++;
				}
				//printf("\n");
				//output[arrayindex] = '\0';
				//printf("output: %s\n", output);




				int individuals = keeplinecnt-9;
				//printf("Number of individuals: %d\n", individuals);
			}

		}



	}


	return output;




}




int vcf_gz_getgenotypiccount(char const* fileName, int index, int sample)
{


    gzFile * file;

    file = gzopen (fileName, "r");
    if (! file) {
        fprintf (stderr, "gzopen of '%s' failed: %s.\n", fileName,
                 strerror (errno));
            exit (EXIT_FAILURE);
    }




    char line[10000];
    char* output;
    int linecnt = 0;
    int totallines = 0;






    int totalsamples = 0;

    float afusinggt = 0.0;
    int outcome = 3;




    while (gzgets(file, line, 10000)) {
        /* note that fgets don't strip the terminating \n, checking its
           presence would allow to handle lines longer that sizeof(line) */

		int linecnt = 0;

		int keeplinecnt = 0;
		int keepnumber = 0;
		int keepnumberb = 0;
		int keepnumber2 = 0;
		int keepnumberid = 0;

		// find number of columns = linecnt + 1
		if (line[0] != '#')
		{
	
			totallines = totallines + 1;

			if (totallines == index)
			{

				//printf("line: %d\n", totallines);

				// calculation of columns
				for (int i = 0; i < sizeof(line); i++)
				{
					//printf("%c\n", line[i]);
					if (line[i] == '\t')
					{
						if (((line[i+1] == '0') || (line[i+1] == '1')) && (line[i+2] == '|'))
						{
							linecnt++;
							//printf("%d %c\n", linecnt, line[i+1]);
						}
					}


				}

			

				linecnt = linecnt + 9;
				//printf("Number of columns: %d\n", linecnt);
				//linecnt = 191;
				keeplinecnt = linecnt;



				// find allele2 column
				linecnt = 0;
				keepnumber = -1;
				/*
				for (int i = 0; i < sizeof(line); i++)
				{
					//printf("%c\n", line[i]);
					if (line[i] == '\t')
					{
						linecnt++;
					}

					if (linecnt == 1)
					{
						keepnumber = i;
						break;
					}

				}
				*/


				// printouts

/*
				//printf("Allele2: %c", line[keepnumber+1]);
				int arrayindex = 0;
				output[arrayindex] = line[keepnumber+1];
				//printf("output: %s\n", output);
				arrayindex++;
				int cont = 1;
				while (line[keepnumber+1+cont] != '\t')
				{
					//printf("%c", line[keepnumber+1+cont]);
					output[arrayindex] = line[keepnumber+1+cont];
					arrayindex++;
					cont++;
				}
				//printf("\n");
				output[arrayindex] = '\0';
				//printf("output: %s\n", output);

*/


				int individuals = keeplinecnt-9;
				//printf("Number of individuals: %d\n", individuals);

				//int individuals = keeplinecnt-9;
				//printf("Number of individuals: %d\n", individuals);
				int* indivlist = (int*)malloc(sizeof(int)*individuals);
				int totalalleles = individuals*2;

				// find AF using GT

				// flag table for running the list for AF calculations

				//if ((argc < 3) || ((argc > 2) && ((strcmp(argv[2],"gt") == 0) || (strcmp(argv[2],"all") == 0))))
				//{

					for (int i = 0; i < individuals; i++)
					{
						indivlist[i] = 0;
					}

					int alleles = 0;
					linecnt = 0;
					for (int i = 0; i < sizeof(line); i++)
					{
						//printf("%c\n", line[i]);
						if (line[i] == '\t')
						{
							linecnt++;
						}

						if ((linecnt > 8) && (linecnt < keeplinecnt+1) && (indivlist[linecnt - 9] == 0))
						{
							totalsamples = totalsamples + 1;
							keepnumber = i;
							int first = line[keepnumber+1] - '0';
							int second = line[keepnumber+3] - '0';
							if (totalsamples == sample)
							{
								alleles = alleles + first + second;
							}
							//printf("alleles: %d %d\n", i, alleles);
							indivlist[linecnt - 9] = 1;
						}

					}

					//printf("\n---AF using GT---\n");
					//printf("Alleles: %d\n", alleles);
					//printf("Total amount of alleles: %d\n", totalalleles);

					//afusinggt = (float)alleles/(float)totalalleles;

					if (alleles == 0)
					{
						outcome = 2;
					}

					if (alleles == 1)
					{
						outcome = 1;
					}

					if (alleles == 2)
					{
						outcome = 0;
					}
					//printf("AF using GT: %f\n", afusinggt);


				//}




			}

		}



	}


	return outcome;




}



double vcf_gz_getdosage(char const* fileName, int index, int sample)
{


    gzFile * file;

    file = gzopen (fileName, "r");
    if (! file) {
        fprintf (stderr, "gzopen of '%s' failed: %s.\n", fileName,
                 strerror (errno));
            exit (EXIT_FAILURE);
    }




    char line[10000];
    char* output;
    int linecnt = 0;
    int totallines = 0;


    int totalsamples = 0;

    float afusinggt = 0.0;
    int outcome = 3;
    double dsoutput = 0.0;




    while (gzgets(file, line, 10000)) {
        /* note that fgets don't strip the terminating \n, checking its
           presence would allow to handle lines longer that sizeof(line) */

		int linecnt = 0;

		int keeplinecnt = 0;
		int keepnumber = 0;
		int keepnumberb = 0;
		int keepnumber2 = 0;
		int keepnumberid = 0;

		// find number of columns = linecnt + 1
		if (line[0] != '#')
		{
	
			totallines = totallines + 1;

			if (totallines == index)
			{

				//printf("line: %d\n", totallines);

				// calculation of columns
				for (int i = 0; i < sizeof(line); i++)
				{
					//printf("%c\n", line[i]);
					if (line[i] == '\t')
					{
						if (((line[i+1] == '0') || (line[i+1] == '1')) && (line[i+2] == '|'))
						{
							linecnt++;
							//printf("%d %c\n", linecnt, line[i+1]);
						}
					}


				}

			

				linecnt = linecnt + 9;
				//printf("Number of columns: %d\n", linecnt);
				//linecnt = 191;
				keeplinecnt = linecnt;



				// find allele2 column
				linecnt = 0;
				keepnumber = -1;
				/*
				for (int i = 0; i < sizeof(line); i++)
				{
					//printf("%c\n", line[i]);
					if (line[i] == '\t')
					{
						linecnt++;
					}

					if (linecnt == 1)
					{
						keepnumber = i;
						break;
					}

				}
				*/


				// printouts

/*
				//printf("Allele2: %c", line[keepnumber+1]);
				int arrayindex = 0;
				output[arrayindex] = line[keepnumber+1];
				//printf("output: %s\n", output);
				arrayindex++;
				int cont = 1;
				while (line[keepnumber+1+cont] != '\t')
				{
					//printf("%c", line[keepnumber+1+cont]);
					output[arrayindex] = line[keepnumber+1+cont];
					arrayindex++;
					cont++;
				}
				//printf("\n");
				output[arrayindex] = '\0';
				//printf("output: %s\n", output);

*/


				int individuals = keeplinecnt-9;
				//printf("Number of individuals: %d\n", individuals);

				//int individuals = keeplinecnt-9;
				//printf("Number of individuals: %d\n", individuals);
				int* indivlist = (int*)malloc(sizeof(int)*individuals);
				int totalalleles = individuals*2;





				// find AF using DS



				for (int i = 0; i < individuals; i++)
				{
					indivlist[i] = 0;
				}

				
				//int cnt = 0;
				linecnt = 0;
				for (int i = 0; i < sizeof(line); i++)
				{
					//printf("%c\n", line[i]);
					if (line[i] == '\t')
					{
						linecnt++;
					}

					if ((linecnt > 8) && (linecnt < keeplinecnt+1) && (indivlist[linecnt - 9] == 0))
					{
						totalsamples = totalsamples + 1;
						keepnumber = i;
						int thousands = line[keepnumber+5] - '0';
						int first = line[keepnumber+7] - '0';
						int second = line[keepnumber+8] - '0';
						int third = line[keepnumber+9] - '0';
						int final = 1000*thousands+100*first + 10*second + third;
						double dfinal = final / 1000.0;
						if (totalsamples == sample)
						{
							dsoutput = dsoutput + dfinal;
						}

						//printf("dsoutput: %f cnt = %d\n", dsoutput, cnt); cnt++;
						indivlist[linecnt - 9] = 1;
					}

				}

				//printf("\n---AF using DS---\n");
				//printf("Sum of DS values: %f\n", dsoutput);
				//printf("Total amount of alleles: %d\n", totalalleles);
				//double afusingds = dsoutput/(double)totalalleles;
				//printf("AF using DS: %f\n", afusingds);
				//scanf("%d", &keep1);






			}

		}



	}


	return dsoutput;




}




double *vcf_gz_getgenotypicprobabilites(char const* fileName, int index, int sample)
{


    gzFile * file;

    file = gzopen (fileName, "r");
    if (! file) {
        fprintf (stderr, "gzopen of '%s' failed: %s.\n", fileName,
                 strerror (errno));
            exit (EXIT_FAILURE);
    }




    char line[10000];
    //char* output;
    int linecnt = 0;
    int totallines = 0;
    int totalsamples = 0;

    float afusinggt = 0.0;
    int outcome = 3;
    double *output;
double dfinal0 = 0.0;
double dfinal = 0.0;
double dfinal2 = 0.0;




    while (gzgets(file, line, 10000)) {
        /* note that fgets don't strip the terminating \n, checking its
           presence would allow to handle lines longer that sizeof(line) */

		int linecnt = 0;

		int keeplinecnt = 0;
		int keepnumber = 0;
		int keepnumberb = 0;
		int keepnumber2 = 0;
		int keepnumberid = 0;

		// find number of columns = linecnt + 1
		if (line[0] != '#')
		{
	
			totallines = totallines + 1;

			if (totallines == index)
			{

				//printf("line: %d\n", totallines);

				// calculation of columns
				for (int i = 0; i < sizeof(line); i++)
				{
					//printf("%c\n", line[i]);
					if (line[i] == '\t')
					{
						if (((line[i+1] == '0') || (line[i+1] == '1')) && (line[i+2] == '|'))
						{
							linecnt++;
							//printf("%d %c\n", linecnt, line[i+1]);
						}
					}


				}

			

				linecnt = linecnt + 9;
				//printf("Number of columns: %d\n", linecnt);
				//linecnt = 191;
				keeplinecnt = linecnt;



				// find allele2 column
				linecnt = 0;
				keepnumber = -1;
				/*
				for (int i = 0; i < sizeof(line); i++)
				{
					//printf("%c\n", line[i]);
					if (line[i] == '\t')
					{
						linecnt++;
					}

					if (linecnt == 1)
					{
						keepnumber = i;
						break;
					}

				}
				*/


				// printouts

/*
				//printf("Allele2: %c", line[keepnumber+1]);
				int arrayindex = 0;
				output[arrayindex] = line[keepnumber+1];
				//printf("output: %s\n", output);
				arrayindex++;
				int cont = 1;
				while (line[keepnumber+1+cont] != '\t')
				{
					//printf("%c", line[keepnumber+1+cont]);
					output[arrayindex] = line[keepnumber+1+cont];
					arrayindex++;
					cont++;
				}
				//printf("\n");
				output[arrayindex] = '\0';
				//printf("output: %s\n", output);

*/


				int individuals = keeplinecnt-9;
				//printf("Number of individuals: %d\n", individuals);

				//int individuals = keeplinecnt-9;
				//printf("Number of individuals: %d\n", individuals);
				int* indivlist = (int*)malloc(sizeof(int)*individuals);
				int totalalleles = individuals*2;





				// find AF using GP
			


				for (int i = 0; i < individuals; i++)
				{
					indivlist[i] = 0;
				}

				double gpoutput = 0.0;
				linecnt = 0;
				for (int i = 0; i < sizeof(line); i++)
				{
					//printf("%c\n", line[i]);
					if (line[i] == '\t')
					{
						linecnt++;
					}

					if ((linecnt > 8) && (linecnt < keeplinecnt+1) && (indivlist[linecnt - 9] == 0))
					{
						totalsamples = totalsamples + 1;
						keepnumber = i;

						if (totalsamples == sample)
						{


							int thousands0 = line[keepnumber+11] - '0';
							int first0 = line[keepnumber+13] - '0';
							int second0 = line[keepnumber+14] - '0';
							int third0 = line[keepnumber+15] - '0';
							int final0 = 1000*thousands0 + 100*first0 + 10*second0 + third0;
							dfinal0 = final0 / 1000.0;

							int thousands = line[keepnumber+17] - '0';
							int first = line[keepnumber+19] - '0';
							int second = line[keepnumber+20] - '0';
							int third = line[keepnumber+21] - '0';
							int final = 1000*thousands + 100*first + 10*second + third;
							dfinal = final / 1000.0;

							int thousands2 = line[keepnumber+23] - '0';
							int first2 = line[keepnumber+25] - '0';
							int second2 = line[keepnumber+26] - '0';
							int third2 = line[keepnumber+27] - '0';
							int final2 = 1000*thousands2 + 100*first2 + 10*second2 + third2;
							dfinal2 = final2 / 1000.0;

							gpoutput = gpoutput + dfinal + 2*dfinal2;
						}


						indivlist[linecnt - 9] = 1;
					}

				}

				//printf("\n---AF using GP---\n");
				//printf("Sum of probabilities: %f\n", gpoutput);
				//printf("Total amount of alleles: %d\n", totalalleles);
				//double afusinggp = gpoutput/(double)totalalleles;
				//printf("AF using GP: %f\n", afusinggp);

				//printf("Prob1: %f\n", dfinal0);
				//printf("Prob2: %f\n", dfinal);
				//printf("Prob3: %f\n", dfinal2);

				output[0] = dfinal0;
				output[1] = dfinal;
				output[2] = dfinal2;








			}

		}



	}


	return output;




}
