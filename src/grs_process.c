/*
 * Copyright (C) 2016-2017 The Brigham and Women’s Hospital, Inc.
 * Portions contributed and copyright held by others as indicated.
 * All rights reserved.
 *
 * The contents of this file are subject to the terms of the BSD 3-clause "New" or "Revised"
 * License (the "License"). You may not use this file except in compliance with the License.
 *
 * You should have received a copy of the License in a separate text file originally named
 * "LICENSE_BWH.md"; if not, refer to "BSD-3-Clause" license at https://spdx.org/licenses/.
 */

#include <plinkio/plinkio.h>
#include <math.h>
#include <stdio.h>
#include <sys/mman.h> //todo remove
#include <stdbool.h>

#include "mem_alloc.h"
#include "grs_process.h"
#include "genetic.h"
#include "io_report.h"

#ifdef USE_OPEN_MP
#include <omp.h>
#endif

static STATUS read_or_file(const char *or_file, snp_or_t** odds, size_t odds_sz) {
   int errors = 0;
   int odds_ct = 0;
   FILE *fp = NULL;
   char buf[GRS_OR_FILE_MAX_LINE_LEN];

   if (!(fp = fopen(or_file, "r"))) {
      return STATUS_IO_ERROR;
   }

   while (fgets(buf, GRS_OR_FILE_MAX_LINE_LEN, fp) && odds_ct < odds_sz) {
      char *buf_state = NULL;
      char *snp_name = NULL;
      char *chr_num = NULL;
      char *bp_pos = NULL;
      char *allele1 = NULL;
      char *allele2 = NULL;
      char *or = NULL;

      // SNP Name
      if (!(snp_name = strtok_r(buf, GRS_OR_FILE_DELIMITER, &buf_state))) {
         errors++;
         break;
      }

      // Chr
      if (!(chr_num = strtok_r(NULL, GRS_OR_FILE_DELIMITER, &buf_state))) {
         errors++;
         break;
      }

      // Position
      if (!(bp_pos = strtok_r(NULL, GRS_OR_FILE_DELIMITER, &buf_state))) {
         errors++;
         break;
      }

      // Allele1
      if (!(allele1 = strtok_r(NULL, GRS_OR_FILE_DELIMITER, &buf_state))) {
         errors++;
         break;
      }

      // Allele2
      if (!(allele2 = strtok_r(NULL, GRS_OR_FILE_DELIMITER, &buf_state))) {
         errors++;
         break;
      }

      // OR
      if (!(or = strtok_r(NULL, GRS_OR_FILE_DELIMITER, &buf_state))) {
         errors++;
         break;
      }

      odds[odds_ct] = (snp_or_t*) my_mm_malloc(sizeof(snp_or_t));
      memset(odds[odds_ct], 0, sizeof(snp_or_t));

      odds[odds_ct]->snp_name = strdup(snp_name);
      odds[odds_ct]->chromosome = atoi(chr_num);
      odds[odds_ct]->bp_position = atoll(bp_pos);
      odds[odds_ct]->allele1 = strdup(allele1);
      odds[odds_ct]->allele2 = strdup(allele2);
      odds[odds_ct]->or_val = strtod(or, NULL);

      odds_ct++;
   }

   if (fclose(fp)) {
      return STATUS_IO_ERROR;
   }

   if (errors) {
      return STATUS_UNKNOWN_ERROR;
   }

   return STATUS_OK;
}

int grs_process(const char *plink_file_prefix, const char *aux_input_file, const char *output_fileset_prefix, size_t num_of_threads,
      unsigned long int config_flags) {
   uint_fast32_t snp_buffer_idx = 0;
   int errors = 0;
   struct pio_file_t pio_file;
   inheritance_mode_t inheritance;

   // Determining mode of inheritance
   if (config_flags & CONF_INH_REC) {
      inheritance = INH_MODE_RECESSIVE;
   } else if (config_flags & CONF_INH_DOM) {
      inheritance = INH_MODE_DOMINANT;
   } else if (config_flags & CONF_INH_ADD) {
      inheritance = INH_MODE_ADDITIVE;
   } else {
      Error("Invalid mode of inheritance configured");
      return STATUS_UNKNOWN_ERROR;
   }

   // Initializing structures
   memset(&pio_file, 0, sizeof(struct pio_file_t));

   // Opening PLINK file-set
   if (pio_open(&pio_file, plink_file_prefix, PIO_FORMAT_PLINK_BINARY) != PIO_OK) {
      Error("Could not open PLINK file-set with prefix %s", plink_file_prefix);
      return STATUS_UNKNOWN_ERROR;
   }

   // Checking if PLINK file-set contains single SNP for all individuals per row, as opposed to the opposite storage fashion
   if (!pio_one_locus_per_row(&pio_file)) {
      Warning("PLINK BED-file must contain a single SNP (for all individuals) in each row.");
      //todo review this command with new I/O
      Warning("Use command \"transpose\" to convert this file-set and re-run program with the resulting file-set.");
      pio_close(&pio_file);
      return STATUS_UNKNOWN_ERROR;
   }

   // SNP count
   size_t snp_ct = pio_num_loci(&pio_file);

   // Sample count
   size_t sample_ct = pio_row_size(&pio_file);

   // Block size
   size_t blk_size = GRS_DEFAULT_BLOCK_SIZE;

   Verbose("SNP Count: %lu; Sample Count: %lu", snp_ct, sample_ct);

   // Sanity check for number of SNPs and samples
   if (snp_ct < 1 || sample_ct < 2) {
      Error("PLINK file-set doesn't contain required data (too few samples or SNPs)");
      return STATUS_UNKNOWN_ERROR;
   }

   // Odds array for the odds from the input file
   snp_or_t** odds_array = (snp_or_t**) my_mm_malloc(sizeof(snp_or_t*) * snp_ct);
   memset(odds_array, 0, sizeof(struct snp_or_t*) * snp_ct);

   // Reading OR-file
   STATUS status = read_or_file(aux_input_file, odds_array, snp_ct);
   if (status) {
      Error("Could not read the odds file %s (status: %d)", aux_input_file, status);
      return status;
   } else {
      size_t tmp = 0;
      size_t cnt = 0;
      while (tmp < snp_ct) {
         if (odds_array[tmp++])
            cnt++;
      }
      if (!cnt) {
         Error("No matching SNPs found in the odds file %s (status: %d)", aux_input_file, status);
         return STATUS_UNKNOWN_ERROR;
      }
      Verbose("Parsed %zd SNP/OR data from the OR input file", cnt);
   }

   // SNP locus info
   struct pio_locus_t** snp_locus_array = (struct pio_locus_t**) my_mm_malloc(sizeof(struct pio_locus_t*) * snp_ct);
   memset(snp_locus_array, 0, sizeof(struct pio_locus_t*) * snp_ct);

   // SNP genotype
   snp_t** snp_buffer_array = (snp_t**) my_mm_malloc(sizeof(snp_t*) * snp_ct);
   memset(snp_buffer_array, 0, sizeof(snp_t*) * snp_ct);

   // Reading locus info
   for (int i = 0; i < snp_ct; i++) {
      snp_locus_array[i] = pio_get_locus(&pio_file, i);
   }
   Verbose("Genotype locus info read for %zd SNPs", snp_ct);

   // GRS process results
   grs_process_result_t** results = (grs_process_result_t**) my_mm_malloc(sizeof(grs_process_result_t*) * snp_ct);
   memset(results, 0, sizeof(struct grs_process_result_t*) * snp_ct);

   // Resetting PIO row pointer
   pio_reset_row(&pio_file);

#ifdef USE_OPEN_MP
   // OpenMP global thread pool configuration
   omp_set_num_threads(num_of_threads);

   // Parallel computing using OpenMP
#pragma omp parallel firstprivate(blk_size)
#endif
#ifdef USE_OPEN_MP
#pragma omp for schedule(guided)
#endif
   for (int i = 0; i < snp_ct; i += blk_size) {

      // Correcting size of the last block
      if (i + blk_size > snp_ct) {
         blk_size = snp_ct - i;
      }

      // Allocating structures and reading genotype data
#pragma omp critical
      while (snp_buffer_idx < i + blk_size) {
         snp_buffer_array[snp_buffer_idx] = (snp_t*) my_mm_malloc(sizeof(snp_t) * sample_ct);
         if (pio_next_row(&pio_file, snp_buffer_array[snp_buffer_idx]) != PIO_OK) {
            Error("Could not read the next row from pio-file");
            if (snp_buffer_array[snp_buffer_idx])
               my_mm_free(snp_buffer_array[snp_buffer_idx]);
            snp_buffer_array[snp_buffer_idx] = NULL;
         }
         snp_buffer_idx++;
      }

      // for each SNP in the block
      for (int j = i; j < i + blk_size; j++) {

         // Matching current SNP from BIM-file with SNP from the OR-file based on the bp_position and chromosome numbers
         size_t idx = 0;
         bool alleles_swapped = false;

         while (idx < snp_ct) {
            if (odds_array[idx] && (odds_array[idx]->chromosome == snp_locus_array[j]->chromosome)
                  && (odds_array[idx]->bp_position == snp_locus_array[j]->bp_position) && is_str_dna_base(odds_array[idx]->allele1)
                  && is_str_dna_base(odds_array[idx]->allele2) && is_str_dna_base(snp_locus_array[j]->allele1)
                  && is_str_dna_base(snp_locus_array[j]->allele2)
                  && ((*(odds_array[idx]->allele1) == *(snp_locus_array[j]->allele1) && *(odds_array[idx]->allele2) == *(snp_locus_array[j]->allele2))
                        || (*(odds_array[idx]->allele1) == *(snp_locus_array[j]->allele2)
                              && *(odds_array[idx]->allele2) == *(snp_locus_array[j]->allele1)))) {
               break;
            }
            idx++;
         }

         // Skipping unmatched or invalid SNP
         if (idx >= snp_ct) {
            continue;
         }

         // Swapping alleles in odds_array if needed, in order to have OR for the allele1 > 1.0
         if (odds_array[idx]->or_val < 1.0) {
            char *tmp = odds_array[idx]->allele1;
            odds_array[idx]->allele1 = odds_array[idx]->allele2;
            odds_array[idx]->allele2 = tmp;
            odds_array[idx]->or_val = 1.0 / odds_array[idx]->or_val;
            alleles_swapped = true;
         }

         // Allocating results structure for the current SNP
         results[j] = (grs_process_result_t*) my_mm_malloc(sizeof(grs_process_result_t));
         memset(results[j], 0, sizeof(grs_process_result_t));

         results[j]->chromosome = odds_array[idx]->chromosome;
         results[j]->bp_position = odds_array[idx]->bp_position;
         results[j]->snp_name = strdup(odds_array[idx]->snp_name);
         results[j]->allele1 = strdup(odds_array[idx]->allele1);
         results[j]->allele2 = strdup(odds_array[idx]->allele2);
         results[j]->or_val = odds_array[idx]->or_val;
         results[j]->alleles_swapped = alleles_swapped;
         results[j]->sample_ct = sample_ct;

         if ((*(odds_array[idx]->allele1) == *(odds_array[idx]->allele2))
               || (*(odds_array[idx]->allele1) == 'A' && *(odds_array[idx]->allele2) == 'T')
               || (*(odds_array[idx]->allele1) == 'T' && *(odds_array[idx]->allele2) == 'A')
               || (*(odds_array[idx]->allele1) == 'C' && *(odds_array[idx]->allele2) == 'G')
               || (*(odds_array[idx]->allele1) == 'G' && *(odds_array[idx]->allele2) == 'C')) {
            // Ambiguous data: cannot match minor/major alleles from the BIM-file and OR-file
            results[j]->status = STATUS_UNKNOWN_ERROR;
         } else {
            // Allocating score array
            results[j]->score = (float*) my_mm_malloc(sizeof(float) * (results[j]->sample_ct));

            for (int k = 0; k < sample_ct; k++) {
               if (snp_buffer_array[j][k] == GENOTYPE_MISSING) {
                  results[j]->score[k] = nanf("");
                  continue;
               }
               if (*(results[j]->allele1) == *(snp_locus_array[j]->allele1)) {
                  results[j]->score[k] = genotype_to_float(snp_buffer_array[j][k], inheritance);
               } else if (*(results[j]->allele1) == *(snp_locus_array[j]->allele2)) {
                  genotype_t genotype;
                  if (snp_buffer_array[j][k] == GT_HOMOZYGOUS_MAJOR) {
                     genotype = GT_HOMOZYGOUS_MINOR;
                  } else if (snp_buffer_array[j][k] == GT_HOMOZYGOUS_MINOR) {
                     genotype = GT_HOMOZYGOUS_MAJOR;
                  } else {
                     genotype = GT_HETEROZYGOUS;
                  }
                  results[j]->score[k] = genotype_to_float(genotype, inheritance);
               } else {
                  results[j]->score[k] = -1.0;
                  Error("Fatal error! Could not determine GRS for SNP %s from the odds file", results[j]->snp_name);
               }
            }
         }
      }

      // Freeing up the buffers holding genotype data
      for (int j = i; j < i + blk_size; j++) {
         if (snp_buffer_array[j])
            my_mm_free(snp_buffer_array[j]);
         snp_buffer_array[j] = NULL;
      }
   };

   // GRS Process Summary
   size_t snp_processed = 0;
   size_t snp_skipped = 0;
   for (int i = 0; i < snp_ct; i++) {
      if (!(results[i]))
         continue;
      snp_processed++;
      if (results[i]->status)
         snp_skipped++;
   }
   Verbose("GRS Calculation finished for %zd out of %zd", snp_processed - snp_skipped, snp_processed);

   // Reporting GRS
   char *report_grs_filename = strcat((char*) calloc(strlen(output_fileset_prefix) + strlen(GRS_REPORT_GRS_FILE_EXT), sizeof(char)),
         output_fileset_prefix);
   strcat(report_grs_filename, GRS_REPORT_GRS_FILE_EXT);
   char *report_wgrs_filename = strcat((char*) calloc(strlen(output_fileset_prefix) + strlen(GRS_REPORT_WGRS_FILE_EXT), sizeof(char)),
         output_fileset_prefix);
   strcat(report_wgrs_filename, GRS_REPORT_WGRS_FILE_EXT);
   char *report_lwgrs_filename = strcat((char*) calloc(strlen(output_fileset_prefix) + strlen(GRS_REPORT_LWGRS_FILE_EXT), sizeof(char)),
         output_fileset_prefix);
   strcat(report_lwgrs_filename, GRS_REPORT_LWGRS_FILE_EXT);
   char *report_info_filename = strcat((char*) calloc(strlen(output_fileset_prefix) + strlen(GRS_REPORT_INFO_FILE_EXT), sizeof(char)),
         output_fileset_prefix);
   strcat(report_info_filename, GRS_REPORT_INFO_FILE_EXT);

   // Report line length
   size_t line_len = 2 * GRS_REPORT_FIELD_LEN + GRS_REPORT_NEW_LINE_CHR_LEN;
   for (int j = 0; j < snp_ct; j++) {
      if (!results[j])
         continue;
      line_len += GRS_REPORT_FIELD_LEN;
   }

   io_report_file_t* grs = open_report_file(report_grs_filename, sample_ct, GRS_REPORT_LINES_PER_SECT, line_len, true);
   io_report_file_t* wgrs = open_report_file(report_wgrs_filename, sample_ct, GRS_REPORT_LINES_PER_SECT, line_len, true);
   io_report_file_t* lwgrs = open_report_file(report_lwgrs_filename, sample_ct, GRS_REPORT_LINES_PER_SECT, line_len, true);
   io_report_file_t* info = open_report_file(report_info_filename, snp_processed, GRS_REPORT_LINES_PER_SECT, GRS_REPORT_INFO_LINE_LEN, true);

   // Info file
   char *info_ptr = report_file_hdr_ptr(info);
   if (!info_ptr) {
      return STATUS_UNKNOWN_ERROR;
   }
   int info_hdr_bytes_written = sprintf(info_ptr,
         GRS_REPORT_INFO_SNP_ID_FMT GRS_REPORT_INFO_HDR_CHR_FMT GRS_REPORT_INFO_HDR_POS_FMT GRS_REPORT_INFO_HDR_N_FMT,
         GRS_REPORT_INFO_HDR_SNP_ID, GRS_REPORT_INFO_HDR_CHR_STR, GRS_REPORT_INFO_HDR_POS_STR, GRS_REPORT_INFO_HDR_N_STR);
   info_hdr_bytes_written += sprintf(&(info_ptr[info_hdr_bytes_written]),
         GRS_REPORT_INFO_ALL_NM_FMT GRS_REPORT_INFO_ALL_NM_FMT GRS_REPORT_INFO_STATUS_FMT,
         GRS_REPORT_INFO_HDR_AL1_STR, GRS_REPORT_INFO_HDR_AL2_STR, GRS_REPORT_INFO_HDR_ST_STR);
   info_ptr[info_hdr_bytes_written++] = GRS_REPORT_NEW_LINE_CHR;
   assert(info_hdr_bytes_written == GRS_REPORT_INFO_LINE_LEN);
   msync(info, GRS_REPORT_INFO_LINE_LEN, MS_ASYNC);

   int results_idx = 0;
   for (int i = 0; i < snp_processed; i++) {
      while (!(results[results_idx])) {
         results_idx++;
      };
      info_ptr = report_file_sect_ptr(info, i);
      int info_bytes_written = sprintf(info_ptr,
            GRS_REPORT_INFO_SNP_ID_FMT GRS_REPORT_INFO_CHR_NO_FMT GRS_REPORT_INFO_POS_BP_FMT GRS_REPORT_INFO_SMPL_CT_FMT,
            results[results_idx]->snp_name, results[results_idx]->chromosome, results[results_idx]->bp_position, results[results_idx]->sample_ct);
      info_bytes_written += sprintf(&(info_ptr[info_bytes_written]), GRS_REPORT_INFO_ALL_NM_FMT GRS_REPORT_INFO_ALL_NM_FMT GRS_REPORT_INFO_STATUS_FMT,
            results[results_idx]->allele1, results[results_idx]->allele2,
            results[results_idx]->status ?
                  GRS_REPORT_INFO_SKIPPED_STR : (results[results_idx]->alleles_swapped ? GRS_REPORT_INFO_FLIPPED_STR : GRS_REPORT_INFO_OK_STR));
      info_ptr[info_bytes_written++] = GRS_REPORT_NEW_LINE_CHR;
      assert(info_bytes_written == GRS_REPORT_INFO_LINE_LEN);
      msync(info, GRS_REPORT_INFO_LINE_LEN, MS_ASYNC);
      results_idx++;
   }

   char *grs_hdr_ptr = report_file_hdr_ptr(grs);
   char *wgrs_hdr_ptr = report_file_hdr_ptr(wgrs);
   char *lwgrs_hdr_ptr = report_file_hdr_ptr(lwgrs);

   int grs_hdr_bytes_written = sprintf(grs_hdr_ptr, GRS_REPORT_FID_FMT GRS_REPORT_IID_FMT, GRS_REPORT_FID_STR, GRS_REPORT_IID_STR);
   int wgrs_hdr_bytes_written = sprintf(wgrs_hdr_ptr, GRS_REPORT_FID_FMT GRS_REPORT_IID_FMT, GRS_REPORT_FID_STR, GRS_REPORT_IID_STR);
   int lwgrs_hdr_bytes_written = sprintf(lwgrs_hdr_ptr, GRS_REPORT_FID_FMT GRS_REPORT_IID_FMT, GRS_REPORT_FID_STR, GRS_REPORT_IID_STR);

   // Headers for GRS files
   for (int j = 0; j < snp_ct; j++) {
      if (!results[j])
         continue;
      grs_hdr_bytes_written += sprintf(&(grs_hdr_ptr[grs_hdr_bytes_written]), GRS_REPORT_HDR_FMT, results[j]->snp_name, results[j]->allele1);
      wgrs_hdr_bytes_written += sprintf(&(wgrs_hdr_ptr[wgrs_hdr_bytes_written]), GRS_REPORT_HDR_FMT, results[j]->snp_name, results[j]->allele1);
      lwgrs_hdr_bytes_written += sprintf(&(lwgrs_hdr_ptr[lwgrs_hdr_bytes_written]), GRS_REPORT_HDR_FMT, results[j]->snp_name, results[j]->allele1);
   }
   grs_hdr_ptr[grs_hdr_bytes_written++] = GRS_REPORT_NEW_LINE_CHR;
   wgrs_hdr_ptr[wgrs_hdr_bytes_written++] = GRS_REPORT_NEW_LINE_CHR;
   lwgrs_hdr_ptr[lwgrs_hdr_bytes_written++] = GRS_REPORT_NEW_LINE_CHR;
   assert(grs_hdr_bytes_written == line_len);
   assert(wgrs_hdr_bytes_written == line_len);
   assert(lwgrs_hdr_bytes_written == line_len);
   msync(grs_hdr_ptr, line_len, MS_ASYNC);
   msync(wgrs_hdr_ptr, line_len, MS_ASYNC);
   msync(lwgrs_hdr_ptr, line_len, MS_ASYNC);

   // GRS results
   for (int i = 0; i < sample_ct; i++) {
      char *grs_ptr = report_file_sect_ptr(grs, i);
      char *wgrs_ptr = report_file_sect_ptr(wgrs, i);
      char *lwgrs_ptr = report_file_sect_ptr(lwgrs, i);

      if (!grs_ptr || !wgrs_ptr || !lwgrs_ptr) {
         return STATUS_UNKNOWN_ERROR;
      }

      struct pio_sample_t *sample = pio_get_sample(&pio_file, i);

      int grs_bytes_written = sprintf(grs_ptr, GRS_REPORT_FID_FMT GRS_REPORT_IID_FMT, sample->fid, sample->iid);
      int wgrs_bytes_written = sprintf(wgrs_ptr, GRS_REPORT_FID_FMT GRS_REPORT_IID_FMT, sample->fid, sample->iid);
      int lwgrs_bytes_written = sprintf(lwgrs_ptr, GRS_REPORT_FID_FMT GRS_REPORT_IID_FMT, sample->fid, sample->iid);

      for (int j = 0; j < snp_ct; j++) {
         if (!results[j])
            continue;
         grs_bytes_written += sprintf(&(grs_ptr[grs_bytes_written]), GRS_REPORT_VAL_FMT, results[j]->status ? nanf("") : results[j]->score[i]);
         wgrs_bytes_written += sprintf(&(wgrs_ptr[wgrs_bytes_written]), GRS_REPORT_VAL_FMT,
               results[j]->status ? nanf("") : results[j]->score[i] * results[j]->or_val);
         lwgrs_bytes_written += sprintf(&(lwgrs_ptr[lwgrs_bytes_written]), GRS_REPORT_VAL_FMT,
               results[j]->status ? nanf("") : log(results[j]->score[i] * results[j]->or_val));
      }
      grs_ptr[grs_bytes_written++] = GRS_REPORT_NEW_LINE_CHR;
      wgrs_ptr[wgrs_bytes_written++] = GRS_REPORT_NEW_LINE_CHR;
      lwgrs_ptr[lwgrs_bytes_written++] = GRS_REPORT_NEW_LINE_CHR;
      assert(grs_bytes_written == line_len);
      assert(wgrs_bytes_written == line_len);
      assert(lwgrs_bytes_written == line_len);
      msync(grs_ptr, line_len, MS_ASYNC);
      msync(wgrs_ptr, line_len, MS_ASYNC);
      msync(lwgrs_ptr, line_len, MS_ASYNC);
   }

   close_report_file(grs);
   close_report_file(wgrs);
   close_report_file(lwgrs);
   close_report_file(info);

   // De-allocating structures
   if (odds_array) {
      for (int i = 0; i < snp_ct; i++) {
         if (odds_array[i] == NULL)
            continue;
         if (odds_array[i]->snp_name)
            free(odds_array[i]->snp_name);
         if (odds_array[i]->allele1)
            free(odds_array[i]->allele1);
         if (odds_array[i]->allele2)
            free(odds_array[i]->allele2);
         my_mm_free(odds_array[i]);
      }
      my_mm_free(odds_array);
   }

   if (results) {
      for (int i = 0; i < snp_ct; i++) {
         if (results[i] == NULL)
            continue;
         if (results[i]->snp_name)
            free(results[i]->snp_name);
         if (results[i]->allele1)
            free(results[i]->allele1);
         if (results[i]->allele2)
            free(results[i]->allele2);
         if (results[i]->score)
            my_mm_free(results[i]->score);
         my_mm_free(results[i]);
      }
      my_mm_free(results);
   }
   if (snp_locus_array)
      my_mm_free(snp_locus_array);
   if (snp_buffer_array)
      my_mm_free(snp_buffer_array);

   // Closing the PLINK fileset
   pio_close(&pio_file);

   if (errors) {
      Error("Fatal errors occurred!");
      return STATUS_UNKNOWN_ERROR;
   } else {
      return STATUS_OK;
   }
}
